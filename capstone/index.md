---
Title: capstone
Homepage: https://www.capstone-engine.org/
Repository: https://salsa.debian.org/pkg-security-team/capstone
Architectures: any
Version: 5.0.5-1
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-forensics kali-tools-hardware kali-tools-respond kali-tools-reverse-engineering 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### capstone-tool
 
  Capstone is a lightweight multi-platform, multi-architecture disassembly
  framework.
   
  This package contains cstool, a command-line tool to disassemble
  hexadecimal strings.
 
 **Installed size:** `8.96 MB`  
 **How to install:** `sudo apt install capstone-tool`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### cstool
 
 
 ```
 root@kali:~# cstool -h
 Cstool for Capstone Disassembler Engine v5.0.5
 
 Syntax: cstool [-d|-s|-u|-v] <arch+mode> <assembly-hexstring> [start-address-in-hex-format]
 
 The following <arch+mode> options are supported:
         x16         16-bit mode (X86)
         x32         32-bit mode (X86)
         x64         64-bit mode (X86)
         x16att      16-bit mode (X86), syntax AT&T
         x32att      32-bit mode (X86), syntax AT&T
         x64att      64-bit mode (X86), syntax AT&T
         arm         arm
         armbe       arm + big endian
         thumb       thumb mode
         thumbbe     thumb + big endian
         cortexm     thumb + cortex-m extensions
         armv8       arm v8
         thumbv8     thumb v8
         armv8be     arm v8 + big endian
         thumbv8be   thumb v8 + big endian
         arm64       aarch64 mode
         arm64be     aarch64 + big endian
         mips        mips32 + little endian
         mipsbe      mips32 + big endian
         mips64      mips64 + little endian
         mips64be    mips64 + big endian
         ppc32       ppc32 + little endian
         ppc32be     ppc32 + big endian
         ppc32qpx    ppc32 + qpx + little endian
         ppc32beqpx  ppc32 + qpx + big endian
         ppc32ps     ppc32 + ps + little endian
         ppc32beps   ppc32 + ps + big endian
         ppc64       ppc64 + little endian
         ppc64be     ppc64 + big endian
         ppc64qpx    ppc64 + qpx + little endian
         ppc64beqpx  ppc64 + qpx + big endian
         sparc       sparc
         systemz     systemz (s390x)
         xcore       xcore
         m68k        m68k + big endian
         m68k40      m68k_040
         tms320c64x  TMS320C64x
         m6800       M6800/2
         m6801       M6801/3
         m6805       M6805
         m6808       M68HC08
         m6809       M6809
         m6811       M68HC11
         cpu12       M68HC12/HCS12
         hd6301      HD6301/3
         hd6309      HD6309
         hcs08       HCS08
         evm         Ethereum Virtual Machine
         6502        MOS 6502
         65c02       WDC 65c02
         w65c02      WDC w65c02
         65816       WDC 65816 (long m/x)
         wasm:       Web Assembly
         bpf         Classic BPF
         bpfbe       Classic BPF + big endian
         ebpf        Extended BPF
         ebpfbe      Extended BPF + big endian
         riscv32     riscv32
         riscv64     riscv64
         sh          superh SH1
         sh2         superh SH2
         sh2e        superh SH2E
         sh2dsp      superh SH2-DSP
         sh2a        superh SH2A
         sh2afpu     superh SH2A-FPU
         sh3         superh SH3
         sh3be       superh SH3 big endian
         sh3e        superh SH3E
         sh3ebe      superh SH3E big endian
         sh3-dsp     superh SH3-DSP
         sh3-dspbe   superh SH3-DSP big endian
         sh4         superh SH4
         sh4be       superh SH4 big endian
         sh4a        superh SH4A
         sh4abe      superh SH4A big endian
         sh4al-dsp   superh SH4AL-DSP
         sh4al-dspbe superh SH4AL-DSP big endian
         tc110       tricore V1.1
         tc120       tricore V1.2
         tc130       tricore V1.3
         tc131       tricore V1.3.1
         tc160       tricore V1.6
         tc161       tricore V1.6.1
         tc162       tricore V1.6.2
 
 Extra options:
         -d show detailed information of the instructions
         -s decode in SKIPDATA mode
         -u show immediates as unsigned
         -v show version & Capstone core build info
 
 ```
 
 - - -
 
 ### libcapstone-dev
 
  Capstone is a lightweight multi-platform, multi-architecture disassembly
  framework.
   
  These are the development headers and libraries.
 
 **Installed size:** `10.72 MB`  
 **How to install:** `sudo apt install libcapstone-dev`  
 
 {{< spoiler "Dependencies:" >}}
 * libcapstone5 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libcapstone5
 
  Capstone is a lightweight multi-platform, multi-architecture disassembly
  framework.
   
  Features:
   - Support hardware architectures: ARM, ARM64 (aka ARMv8), Mips, PowerPC &
  Intel.
   - Clean/simple/lightweight/intuitive architecture-neutral API.
   - Provide details on disassembled instructions (called "decomposer" by some
  others).
   - Provide some semantics of the disassembled instruction, such as list of
  implicit registers read & written.
   - Implemented in pure C language, with bindings for Java, OCaml and Python
  ready to use and Ruby, C#, GO & Vala available on git repos.
   - Native support for Windows & *nix (with OS X, Linux, *BSD & Solaris
  confirmed).
   - Thread-safe by design.
   - Special support for embedding into firmware or OS kernel.
   - Distributed under the open source BSD license.
 
 **Installed size:** `8.93 MB`  
 **How to install:** `sudo apt install libcapstone5`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 
 - - -
 
 ### python3-capstone
 
  Capstone is a lightweight multi-platform, multi-architecture disassembly
  framework.
   
  These are the Python 3 bindings.
 
 **Installed size:** `848 KB`  
 **How to install:** `sudo apt install python3-capstone`  
 
 {{< spoiler "Dependencies:" >}}
 * libcapstone5
 * python3
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
