---
Title: bloodyad
Homepage: https://github.com/CravateRouge/bloodyAD
Repository: https://gitlab.com/kalilinux/packages/bloodyAD
Architectures: all
Version: 2.1.7-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### bloodyad
 
  bloodyAD can perform specific LDAP calls to a domain controller in order to
  perform AD privesc. It supports authentication using cleartext passwords,
  pass-the-hash, pass-the-ticket or certificates and binds to LDAP services of a
  domain controller to perform AD privesc.
   
  Exchange of sensitive information without LDAPS is supported. It is also
  designed to be used transparently with a SOCKS proxy.
 
 **Installed size:** `870 KB`  
 **How to install:** `sudo apt install bloodyad`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-asn1crypto 
 * python3-asyauth
 * python3-cryptography 
 * python3-dnspython 
 * python3-msldap
 * python3-unicrypto
 * python3-winacl
 {{< /spoiler >}}
 
 ##### bloodyAD
 
 
 ```
 root@kali:~# bloodyAD -h
 usage: bloodyAD [-h] [-d DOMAIN] [-u USERNAME] [-p PASSWORD]
                 [-k [KERBEROS ...]] [-f {b64,hex,aes,rc4,default}]
                 [-c [CERTIFICATE]] [-s] [--host HOST] [--dc-ip DC_IP]
                 [--dns DNS] [--gc] [-v {QUIET,INFO,DEBUG}]
                 {add,get,remove,set} ...
 
 AD Privesc Swiss Army Knife
 
 options:
   -h, --help            show this help message and exit
   -d, --domain DOMAIN   Domain used for NTLM authentication
   -u, --username USERNAME
                         Username used for NTLM authentication
   -p, --password PASSWORD
                         password or LMHASH:NTHASH for NTLM authentication,
                         password or AES/RC4 key for kerberos, password for
                         certificate(Do not specify to trigger integrated
                         windows authentication)
   -k, --kerberos [KERBEROS ...]
                         Enable Kerberos authentication. If '-p' is provided it
                         will try to query a TGT with it. You can also provide
                         a list of one or more optional keywords as '-k
                         kdc=192.168.100.1 kdcc=192.168.150.1
                         realmc=foreign.realm.corp
                         <keyfile_type>=/home/silver/Admin.ccache',
                         <keyfile_type> being ccache, kirbi, keytab, pem or
                         pfx, 'kdc' being the kerberos server for the keyfile
                         provided and 'realmc' and 'kdcc' for cross realm (the
                         realm of the '--host' provided)
   -f, --format {b64,hex,aes,rc4,default}
                         Specify format for '--password' or '-k <keyfile>'
   -c, --certificate [CERTIFICATE]
                         Certificate authentication, e.g:
                         "path/to/key:path/to/cert" (Use Windows Certstore with
                         krb if left empty)
   -s, --secure          Try to use LDAP over TLS aka LDAPS (default is LDAP)
   --host HOST           Hostname or IP of the DC (ex: my.dc.local or
                         172.16.1.3)
   --dc-ip DC_IP         IP of the DC (useful if you provided a --host which
                         can't resolve)
   --dns DNS             IP of the DNS to resolve AD names (useful for inter-
                         domain functions)
   --gc                  Connect to Global Catalog (GC)
   -v, --verbose {QUIET,INFO,DEBUG}
                         Adjust output verbosity
 
 Commands:
   {add,get,remove,set}
     add                 [ADD] function category
     get                 [GET] function category
     remove              [REMOVE] function category
     set                 [SET] function category
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
