---
Title: kali-community-wallpapers
Homepage: 
Repository: https://gitlab.com/kalilinux/packages/kali-community-wallpapers
Architectures: all
Version: 2025.1.1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### kali-community-wallpapers
 
  Wallpapers which have been created and submitted by the community,
  showing off Kali Linux.
 
 **Installed size:** `69.60 MB`  
 **How to install:** `sudo apt install kali-community-wallpapers`  
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
