---
Title: gowitness
Homepage: https://github.com/sensepost/gowitness
Repository: https://gitlab.com/kalilinux/packages/gowitness
Architectures: amd64 arm64 armhf i386
Version: 3.0.5-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### gowitness
 
  gowitness is a website screenshot utility written in Golang, that uses Chrome
  Headless to generate screenshots of web interfaces using the command line, with
  a handy report viewer to process results. Both Linux and macOS is supported,
  with Windows support mostly working.
   
  Inspiration for gowitness comes from Eyewitness. If you are looking for
  something with lots of extra features, be sure to check it out along with these
  other projects.
 
 **Installed size:** `51.76 MB`  
 **How to install:** `sudo apt install gowitness`  
 
 {{< spoiler "Dependencies:" >}}
 * chromium
 * libc6 
 {{< /spoiler >}}
 
 ##### gowitness
 
 
 ```
 root@kali:~# gowitness -h
                _ _                   
  ___ ___ _ _ _|_| |_ __ ___ ___ ___ 
 | . | . | | | | |  _|  | -_|_ -|_ -|
 |_  |___|_____|_|_||_|_|___|___|___|
 |___|    v3, with <3 by @leonjza
 
 Usage:
   gowitness [command]
 
 Available Commands:
   help        Help about any command
   report      Work with gowitness reports
   scan        Perform various scans
   version     Get the gowitness version
 
 Flags:
   -D, --debug-log   Enable debug logging
   -h, --help        help for gowitness
   -q, --quiet       Silence (almost all) logging
 
 Use "gowitness [command] --help" for more information about a command.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
