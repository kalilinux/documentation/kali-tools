---
Title: dploot
Homepage: https://github.com/zblurx/dploot
Repository: https://gitlab.com/kalilinux/packages/dploot
Architectures: all
Version: 3.0.0-0kali2
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-top10 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### python3-dploot
 
  Implement all the DPAPI logic of SharpDPAPI and DPAPI, usable
  with a Python interpreter.
 
 **Installed size:** `355 KB`  
 **How to install:** `sudo apt install python3-dploot`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-cryptography 
 * python3-impacket
 * python3-lxml
 * python3-pyasn1 
 {{< /spoiler >}}
 
 ##### dploot
 
 
 ```
 root@kali:~# dploot -h
 dploot (https://github.com/zblurx/dploot) v3.0.0 by @_zblurx
 usage: dploot [-h]
               {backupkey,blob,browser,certificates,credentials,machinecertificates,machinecredentials,machinemasterkeys,machinetriage,machinevaults,masterkeys,mobaxterm,rdg,sccm,triage,vaults,wam,wifi} ...
 
 DPAPI looting locally remotely in Python
 
 positional arguments:
   {backupkey,blob,browser,certificates,credentials,machinecertificates,machinecredentials,machinemasterkeys,machinetriage,machinevaults,masterkeys,mobaxterm,rdg,sccm,triage,vaults,wam,wifi}
                         Action
     backupkey           Backup Keys from domain controller
     blob                Decrypt DPAPI blob. Can fetch masterkeys on target
     browser             Dump users credentials and cookies saved in browser
                         from local or remote target
     certificates        Dump users certificates from local or remote target
     credentials         Dump users Credential Manager blob from local or
                         remote target
     machinecertificates
                         Dump system certificates from local or remote target
     machinecredentials  Dump system credentials from local or remote target
     machinemasterkeys   Dump system masterkey from local or remote target
     machinetriage       Loot SYSTEM Masterkeys (if not set), SYSTEM
                         credentials, SYSTEM certificates and SYSTEM vaults
                         from local or remote target
     machinevaults       Dump system vaults from local or remote target
     masterkeys          Dump users masterkey from local or remote target
     mobaxterm           Dump Passwords and Credentials from MobaXterm
     rdg                 Dump users saved password information for
                         RDCMan.settings from local or remote target
     sccm                Dump SCCM secrets (NAA, Collection variables, tasks
                         sequences credentials) from local or remote target
     triage              Loot Masterkeys (if not set), credentials, rdg,
                         certificates, browser and vaults from local or remote
                         target
     vaults              Dump users Vaults blob from local or remote target
     wam                 Dump users cached azure tokens from local or remote
                         target
     wifi                Dump wifi profiles from local or remote target
 
 options:
   -h, --help            show this help message and exit
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
