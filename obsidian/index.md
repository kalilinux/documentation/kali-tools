---
Title: obsidian
Homepage: https://obsidian.md/
Repository: https://gitlab.com/kalilinux/packages/obsidian
Architectures: amd64 arm64
Version: 1.7.7-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### obsidian
 
  Obsidian stores notes on your device, so you can access them
  quickly, even offline.
   
  With hundreds of plugins and themes, you can shape Obsidian
  to fit your way of thinking.
   
  Obsidian uses open, non-proprietary files, so you're never
  locked in, and can preserve your data for the long term.
 
 **Installed size:** `287.27 MB`  
 **How to install:** `sudo apt install obsidian`  
 
 {{< spoiler "Dependencies:" >}}
 * libasound2t64 
 * libatk-bridge2.0-0t64 
 * libatk1.0-0t64 
 * libatspi2.0-0t64 
 * libc6 
 * libcairo2 
 * libcups2t64 
 * libdbus-1-3 
 * libdrm2 
 * libexpat1 
 * libgbm1 
 * libgcc-s1 
 * libglib2.0-0t64 
 * libgtk-3-0t64 
 * libnspr4 
 * libnss3 
 * libpango-1.0-0 
 * libx11-6 
 * libxcb1 
 * libxcomposite1 
 * libxdamage1 
 * libxext6
 * libxfixes3
 * libxkbcommon0 
 * libxrandr2
 {{< /spoiler >}}
 
 ##### obsidian
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
