---
Title: brutespray
Homepage: https://github.com/x90skysn3k/brutespray
Repository: https://gitlab.com/kalilinux/packages/brutespray
Architectures: any
Version: 2.2.2-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### brutespray
 
  Brutespray has been re-written in Golang, eliminating the requirement for
  additional tools. This enhanced version is more extensive and operates at a
  significantly faster pace than its Python counterpart. As of now, Brutespray
  accepts input from Nmap's GNMAP/XML output, newline-separated JSON files,
  Nexpose's XML Export feature, Nessus exports in .nessus format, and various
  lists.
 
 **Installed size:** `24.46 MB`  
 **How to install:** `sudo apt install brutespray`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### brutespray
 
 Python bruteforce tool
 
 ```
 root@kali:~# brutespray -h
 Usage of brutespray:
   -C string
     	Specify a combo wordlist deiminated by ':', example: user1:password
   -H string
     	Target in the format service://host:port, CIDR ranges supported,
     	 default port will be used if not specified
   -P	Print found hosts parsed from provided host and file arguments
   -S	List all supported services
   -T int
     	Number of hosts to bruteforce at the same time (default 5)
   -f string
     	File to parse; Supported: Nmap, Nessus, Nexpose, Lists, etc
   -o string
     	Directory containing successful attempts (default "brutespray-output")
   -p string
     	Password or password file to use for bruteforce
   -q	Suppress the banner
   -r int
     	Amount of times to retry after receiving connection failed (default 3)
   -s string
     	Service type: ssh, ftp, smtp, etc; Default all (default "all")
   -t int
     	Number of threads to use (default 10)
   -u string
     	Username or user list to bruteforce
   -w duration
     	Set timeout of bruteforce attempts (default 5s)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## brutespray Usage Examples

Attack all services in `nas.gnmap` with a specific user list (`unix_users.txt`) and password list (`password.lst`):

```console
root@kali:~# brutespray --file nas.gnmap -U /usr/share/wordlists/metasploit/unix_users.txt -P /usr/share/wordlists/metasploit/password.lst --threads 3 --hosts 1

                              #@                           @/
                           @@@                               @@@
                        %@@@                                   @@@.
                      @@@@@                                     @@@@%
                     @@@@@                                       @@@@@
                    @@@@@@@                  @                  @@@@@@@
                    @(@@@@@@@%            @@@@@@@            &@@@@@@@@@
                    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                     @@*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@
                       @@@( @@@@@#@@@@@@@@@*@@@,@@@@@@@@@@@@@@@  @@@
                           @@@@@@ .@@@/@@@@@@@@@@@@@/@@@@ @@@@@@
                                  @@@   @@@@@@@@@@@   @@@
                                 @@@@*  ,@@@@@@@@@(  ,@@@@
                                 @@@@@@@@@@@@@@@@@@@@@@@@@
                                  @@@.@@@@@@@@@@@@@@@ @@@
                                    @@@@@@ @@@@@ @@@@@@
                                       @@@@@@@@@@@@@
                                       @@   @@@   @@
                                       @@ @@@@@@@ @@
                                         @@% @  @@



        ██████╗ ██████╗ ██╗   ██╗████████╗███████╗███████╗██████╗ ██████╗  █████╗ ██╗   ██╗
        ██╔══██╗██╔══██╗██║   ██║╚══██╔══╝██╔════╝██╔════╝██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
        ██████╔╝██████╔╝██║   ██║   ██║   █████╗  ███████╗██████╔╝██████╔╝███████║ ╚████╔╝
        ██╔══██╗██╔══██╗██║   ██║   ██║   ██╔══╝  ╚════██║██╔═══╝ ██╔══██╗██╔══██║  ╚██╔╝
        ██████╔╝██║  ██║╚██████╔╝   ██║   ███████╗███████║██║     ██║  ██║██║  ██║   ██║
        ╚═════╝ ╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚══════╝╚══════╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝



 brutespray.py v1.5.2
 Created by: Shane Young/@x90skysn3k && Jacob Robles/@shellfail
 Inspired by: Leon Johnson/@sho-luv
 Credit to Medusa: JoMo-Kun / Foofus Networks <jmk@foofus.net>

Starting to brute, please make sure to use the right amount of threads(-t) and parallel hosts(-T)...  \

Brute-Forcing...
Medusa v2.2 [http://www.foofus.net] (C) JoMo-Kun / Foofus Networks <jmk@foofus.net>

ACCOUNT CHECK: [mysql] Host: 192.168.86.4 (1 of 1, 0 complete) User: 4Dgifts (1 of 111, 0 complete) Password: !@#$%^& (1 of 88396 complete)
ACCOUNT CHECK: [mysql] Host: 192.168.86.4 (1 of 1, 0 complete) User: 4Dgifts (1 of 111, 0 complete) Password: !@#$% (2 of 88396 complete)
ACCOUNT CHECK: [mysql] Host: 192.168.86.4 (1 of 1, 0 complete) User: 4Dgifts (1 of 111, 0 complete) Password: !@#$%^ (3 of 88396 complete)
[...]
```

Interactive mode, brute forcing the FTP service only.

```console
root@kali:~# brutespray -i -f nas.gnmap

                              #@                           @/
                           @@@                               @@@
                        %@@@                                   @@@.
                      @@@@@                                     @@@@%
                     @@@@@                                       @@@@@
                    @@@@@@@                  @                  @@@@@@@
                    @(@@@@@@@%            @@@@@@@            &@@@@@@@@@
                    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                     @@*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@
                       @@@( @@@@@#@@@@@@@@@*@@@,@@@@@@@@@@@@@@@  @@@
                           @@@@@@ .@@@/@@@@@@@@@@@@@/@@@@ @@@@@@
                                  @@@   @@@@@@@@@@@   @@@
                                 @@@@*  ,@@@@@@@@@(  ,@@@@
                                 @@@@@@@@@@@@@@@@@@@@@@@@@
                                  @@@.@@@@@@@@@@@@@@@ @@@
                                    @@@@@@ @@@@@ @@@@@@
                                       @@@@@@@@@@@@@
                                       @@   @@@   @@
                                       @@ @@@@@@@ @@
                                         @@% @  @@



        ██████╗ ██████╗ ██╗   ██╗████████╗███████╗███████╗██████╗ ██████╗  █████╗ ██╗   ██╗
        ██╔══██╗██╔══██╗██║   ██║╚══██╔══╝██╔════╝██╔════╝██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
        ██████╔╝██████╔╝██║   ██║   ██║   █████╗  ███████╗██████╔╝██████╔╝███████║ ╚████╔╝
        ██╔══██╗██╔══██╗██║   ██║   ██║   ██╔══╝  ╚════██║██╔═══╝ ██╔══██╗██╔══██║  ╚██╔╝
        ██████╔╝██║  ██║╚██████╔╝   ██║   ███████╗███████║██║     ██║  ██║██║  ██║   ██║
        ╚═════╝ ╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚══════╝╚══════╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝



 brutespray.py v1.5.2
 Created by: Shane Young/@x90skysn3k && Jacob Robles/@shellfail
 Inspired by: Leon Johnson/@sho-luv
 Credit to Medusa: JoMo-Kun / Foofus Networks <jmk@foofus.net>

Loading File: /

Welcome to interactive mode!


WARNING: Leaving an option blank will leave it empty and refer to default


Available services to brute-force:
Service: ftp on port 21 with 1 hosts
Service: ssh on port 22 with 1 hosts
Service: mysql on port 3306 with 1 hosts

Enter services you want to brute - default all (ssh,ftp,etc): ftp
Enter the number of parallel threads (default is 2): 4
Enter the number of parallel hosts to scan per service (default is 1): 1
Would you like to specify a wordlist? (y/n): y
Enter a userlist you would like to use: /usr/share/wordlists/metasploit/unix_users.txt
Enter a passlist you would like to use: /usr/share/wordlists/metasploit/password.lst
Would to specify a single username or password (y/n): n

Starting to brute, please make sure to use the right amount of threads(-t) and parallel hosts(-T)...  \

Brute-Forcing...
```
