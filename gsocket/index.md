---
Title: gsocket
Homepage: https://github.com/hackerschoice/gsocket
Repository: https://salsa.debian.org/debian/gsocket
Architectures: any
Version: 1.4.43-4
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### gsocket
 
  Abandon the thought of IP Addresses and Port Numbers. Instead start thinking
  that two programs should be able to communicate with each other as long as
  they know the same secret (rather than each other's IP Address and Port
  Number). The Global Socket library facilitates this: It locally derives
  temporary session keys and IDs and connects two programs through the Global
  Socket Relay Network (GSRN) regardless and independent of the local IP
  Address or geographical location.
   
  Once connected the library then negotiates a secure TLS connection(End-2-End).
  The secret never leaves your workstation. The GSRN sees only the encrypted
  traffic.
   
  The GSRN is a free cloud service and is free to use by anyone.
   
  The Global Socket Toolkit comes with a set of tools:
   
  gsocket - Makes an existing program (behind firewall or NAT) accessible from
  anywhere in the world. It does so by analyzing the program and replacing the
  IP-Layer with its own Gsocket-Layer. A client connection to a hostname ending
  in '*.gsocket' then gets automatically redirected (via the GSRN) to this
  program.
   
  gs-netcat - Netcat on steroids. Turn gs-netcat into an AES-256 encrypted
  reverse backdoor via TOR (optional) with a true PTY/interactive command shell
  (gs-netcat -s MySecret -i), integrated file-transfer, spawn a Socks4/4a/5
  proxy or forward TCP connections or give somebody temporary shell access.
   
  gs-sftp - sftp server & client between two firewalled workstations.
  gs-mount - Access and mount a remote file system.
  blitz - Copy data from workstation to workstation.
 
 **Installed size:** `278 KB`  
 **How to install:** `sudo apt install gsocket`  
 
 {{< spoiler "Dependencies:" >}}
 * dpkg-dev
 * libc6 
 * libssl3t64 
 {{< /spoiler >}}
 
 ##### blitz
 
 Securely transfer files between two workstations through NAT/Firewall.
 
 ```
 root@kali:~# blitz -h
 blitz [-k file] [-s password] [-l] [FILES ...]
    -l           Server Mode.
    -s <secret>  Secret (e.g. password).
    -k <file>    Read Secret from file.
    -f <list>	Read list of file names from FILE [or - for stdin]
    -o RSOPT=val Options passed to rsync.
  
  Example:
     $ blitz -s MySecret -l             # Server
     $ blitz -s MySecret *.dat          # Client
 
 ...limiting transfer rate to 10kB/sec
     $ blitz -s MySecret -o 'RSOPT=--bwlimit=10 -v' /etc/*.conf *.dat
 
 ...reading the file list from standard-in
     $ cd /etc
     $ find . -name '*.mp3' | blitz -s MySecret -f -
 
 ...limit the amount of path information by inserting a dot and slash
     $ blitz -s MySecret ~/tools/./upx/mp3
 
 See 'man gs-netcat' and 'man rsync' for more options.
 ```
 
 - - -
 
 ##### gs-mount
 
 Secure filesystem client through via Global Socket.
 
 ```
 root@kali:~# gs-mount -h
 gs-mount [-k file] [-s password] [-l] [mount point]
 
    -l           Server Mode.
    -R           Server in read-only mode.
    -s <secret>  Secret (e.g. password).
    -k <file>    Read Secret from file.
  
 Example:
     $ gs-mount -s MySecret -l             # Server
     $ gs-mount -s MySecret                # Client
 
 See 'gs-netcat -h' for more options.
 ```
 
 - - -
 
 ##### gs-netcat
 
 Transfer data, forward traffic and execute commands on a remote host. Securely.
 
 ```
 root@kali:~# gs-netcat -h
 gs-netcat [-skrlgvqwCTLtSDuim] [-s secret] [-e cmd] [-p port] [-d ip]
 Version 1.4.42, Jun 28 2024 02:48:58 [OpenSSL 3.2.2 4 Jun 2024]
   -s <secret>  Secret (e.g. password).
   -k <file>    Read Secret from file.
   -r           Receive-only. Terminate when no more data.
   -I           Ignore EOF on stdin.
   -l           Listening server [default: client]
   -g           Generate a Secret (random)
   -v           Verbose. -vv more verbose. -vvv insanely verbose
   -q           Quiet. No log output
   -w           Wait for server to become available [client only]
   -C           Disable encryption
   -T           Use TOR or any Socks proxy (See gs-netcat(1))
   -L <file>    Logfile
   -t           Check if peer is listening (do not connect)
   -S           Act as a SOCKS server [needs -l]
   -D           Daemon & Watchdog mode [background]
   -d <IP>      IPv4 address for port forwarding
   -p <port>    Port to listen on or forward to
   -u           Use UDP [requires -p]
   -i           Interactive login shell (TTY) [Ctrl-e q to terminate]
   -e <cmd>     Execute command [e.g. "bash -il" or "id"]
   -m           Display man page
    
 Example to forward traffic from port 2222 to 192.168.6.7:22:
     $ gs-netcat -l -d 192.168.6.7 -p 22     # Server
     $ gs-netcat -p 2222                     # Client
 Example to act as a SOCKS proxy
     $ gs-netcat -l -S                       # Server
     $ gs-netcat -p 1080                     # Client
 Example file transfer:
     $ gs-netcat -l -r >warez.tar.gz         # Server
     $ gs-netcat <warez.tar.gz               # Client
 Example for a reverse shell:
     $ gs-netcat -l -i                       # Server
     $ gs-netcat -i                          # Client
 ```
 
 - - -
 
 ##### gs-sftp
 
 Secure File Transfer Protocol via Global Socket.
 
 ```
 root@kali:~# gs-sftp -h
 gs-sftp [-k file] [-s password] [-l]
 
    -l           Server Mode.
    -R           Server in read-only mode.
    -s <secret>  Secret (e.g. password).
    -k <file>    Read Secret from file.
  
 Example:
     $ gs-sftp -s MySecret -l             # Server
     $ gs-sftp -s MySecret                # Client
 
 See 'gs-netcat -h' for more options.
 ```
 
 - - -
 
 ##### gsocket
 
 Connect like there is no firewall. Securely.
 
 ```
 root@kali:~# gsocket -h
 gsocket [-k file] [-s password] <programm> <parameters>
    -s <secret>  Secret (e.g. password).
    -k <file>    Read Secret from file.
    -p <ports>   Range of listening ports to redirect [default=all]
    -T           Use TOR.
 
 Example:
     $ gsocket -s MySecret /usr/bin/sshd -d         # Server
     $ gsocket -s MySecret ssh root@gsocket         # Client
 
 See 'gs-netcat -h' for more options.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
