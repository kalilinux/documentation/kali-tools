---
Title: cosign
Homepage: https://github.com/sigstore/cosign
Repository: https://salsa.debian.org/go-team/packages/cosign
Architectures: any all
Version: 2.4.1-2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### cosign
 
  Signing OCI containers (and other artifacts) using Sigstore
   
  Cosign supports:
   
   * "Keyless signing" with the Sigstore public good Fulcio certificate
     authority and Rekor transparency log (default)
   * Hardware and KMS signing
   * Signing with a cosign generated encrypted private/public keypair
   * Container Signing, Verification and Storage in an OCI registry.
   * Bring-your-own PKI
   
  This package contains the command-line tool cosign.
 
 **Installed size:** `66.50 MB`  
 **How to install:** `sudo apt install cosign`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### cosign
 
 Container Signing/Verification/Storage tool
 
 ```
 root@kali:~# cosign -h
 A tool for Container Signing, Verification and Storage in an OCI registry.
 
 Usage:
 cosign [command]
 
 Available Commands:
 attach                  Provides utilities for attaching artifacts to other artifacts in a registry
 attest                  Attest the supplied container image.
 attest-blob             Attest the supplied blob.
 clean                   Remove all signatures from an image.
 completion              Generate completion script
 copy                    Copy the supplied container image and signatures.
 dockerfile              Provides utilities for discovering images in and performing operations on Dockerfiles
 download                Provides utilities for downloading artifacts and attached artifacts in a registry
 env                     Prints Cosign environment variables
 generate                Generates (unsigned) signature payloads from the supplied container image.
 generate-key-pair       Generates a key-pair.
 help                    Help about any command
 import-key-pair         Imports a PEM-encoded RSA or EC private key.
 initialize              Initializes SigStore root to retrieve trusted certificate and key targets for verification.
 load                    Load a signed image on disk to a remote registry
 login                   Log in to a registry
 manifest                Provides utilities for discovering images in and performing operations on Kubernetes manifests
 public-key              Gets a public key from the key-pair.
 save                    Save the container image and associated signatures to disk at the specified directory.
 sign                    Sign the supplied container image.
 sign-blob               Sign the supplied blob, outputting the base64-encoded signature to stdout.
 tree                    Display supply chain security related artifacts for an image such as signatures, SBOMs and attestations
 triangulate             Outputs the located cosign image reference. This is the location where cosign stores the specified artifact type.
 upload                  Provides utilities for uploading artifacts to a registry
 verify                  Verify a signature on the supplied container image
 verify-attestation      Verify an attestation on the supplied container image
 verify-blob             Verify a signature on the supplied blob
 verify-blob-attestation Verify an attestation on the supplied blob
 version                 Prints the version
 
 Flags:
     -h, --help=false:
 	help for cosign
 
     --output-file='':
 	log output to a file
 
     -t, --timeout=3m0s:
 	timeout for commands
 
     -d, --verbose=false:
 	log debug output
 
 Additional help topics:
 cosign piv-tool                This cosign was not built with piv-tool support!
 cosign pkcs11-tool             This cosign was not built with pkcs11-tool support!
 
 Use "cosign [command] --help" for more information about a command.
 ```
 
 - - -
 
 ### golang-github-sigstore-cosign-dev
 
  Signing OCI containers (and other artifacts) using Sigstore
   
  Cosign supports:
   
   * "Keyless signing" with the Sigstore public good Fulcio certificate
     authority and Rekor transparency log (default)
   * Hardware and KMS signing
   * Signing with a cosign generated encrypted private/public keypair
   * Container Signing, Verification and Storage in an OCI registry.
   * Bring-your-own PKI
   
  This package contains the Go source code.
 
 **Installed size:** `2.00 MB`  
 **How to install:** `sudo apt install golang-github-sigstore-cosign-dev`  
 
 {{< spoiler "Dependencies:" >}}
 * golang-github-awslabs-amazon-ecr-credential-helper-dev
 * golang-github-go-openapi-runtime-dev
 * golang-github-go-openapi-strfmt-dev
 * golang-github-go-openapi-swag-dev
 * golang-github-google-go-cmp-dev
 * golang-github-google-go-containerregistry-dev
 * golang-github-google-go-github-dev
 * golang-github-in-toto-in-toto-golang-dev
 * golang-github-kelseyhightower-envconfig-dev
 * golang-github-mitchellh-go-wordwrap-dev
 * golang-github-moby-term-dev
 * golang-github-nozzle-throttler-dev
 * golang-github-pkg-errors-dev
 * golang-github-secure-systems-lab-go-securesystemslib-dev
 * golang-github-sigstore-fulcio-dev
 * golang-github-sigstore-rekor-dev
 * golang-github-sigstore-sigstore-dev 
 * golang-github-sigstore-sigstore-go-dev
 * golang-github-sigstore-timestamp-authority-dev
 * golang-github-smallstep-crypto-dev
 * golang-github-spf13-cobra-dev
 * golang-github-spf13-pflag-dev
 * golang-github-spf13-viper-dev
 * golang-github-spiffe-go-spiffe-dev
 * golang-github-stretchr-testify-dev
 * golang-github-transparency-dev-merkle-dev
 * golang-github-withfig-autocomplete-tools-dev
 * golang-github-xanzy-go-gitlab-dev
 * golang-golang-x-crypto-dev
 * golang-golang-x-oauth2-google-dev
 * golang-golang-x-sync-dev
 * golang-golang-x-term-dev
 * golang-google-api-dev
 * golang-gopkg-square-go-jose.v2-dev
 * golang-k8s-api-dev
 * golang-k8s-apimachinery-dev
 * golang-k8s-client-go-dev
 * golang-k8s-sigs-release-utils-dev
 * golang-k8s-utils-dev
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
