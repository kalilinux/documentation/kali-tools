---
Title: xmount
Homepage: https://www.sits.lu/xmount
Repository: https://salsa.debian.org/pkg-security-team/xmount
Architectures: linux-any
Version: 1.2.0+ds-1
Metapackages: kali-linux-everything kali-tools-forensics kali-tools-respond 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### xmount
 
  xmount allows you to convert on-the-fly between multiple input and
  output harddisk image formats. xmount creates a virtual file system
  using FUSE (Filesystem in Userspace) that contains a virtual
  representation of the input image.
   
  The virtual representation can be in raw DD, Apple's Disk Image format
  (DMG), VirtualBox's virtual disk file format (VDI), Microsoft's Virtual
  Hard Disk Image format (VHD) or in VMware's VMDK file format.
   
  Input images can be raw DD, EWF (Expert Witness Compression Format),
  AFF (Advanced Forensic Format), VDI (VirtualBox Virtual Disk Image) or
  QCOW (QEMU Copy On Write) files.
   
  In addition, xmount also supports virtual write access to the output
  files that is redirected to a cache file. This makes it for example
  possible to boot acquired harddisk images using QEMU, KVM, VirtualBox,
  VWware or alike.
 
 **Installed size:** `327 KB`  
 **How to install:** `sudo apt install xmount`  
 
 {{< spoiler "Dependencies:" >}}
 * libafflib0t64 
 * libc6 
 * libewf2 
 * libfuse3-3 
 * zlib1g 
 {{< /spoiler >}}
 
 ##### xmount
 
 Tool to crossmount between multiple input and output harddisk image files
 
 ```
 root@kali:~# xmount -h
 
 xmount v1.2.0 Copyright (c) 2024 by SITS Sarl <development@sits.lu>
 
 Usage:
   xmount [fopts] <xopts> <mntp>
 
 Options:
   fopts:
     -d : Enable FUSE's and xmount's debug mode.
     -h : Display this help message.
     -s : Run single threaded.
     -o no_allow_other : Disable automatic addition of FUSE's allow_other option.
     -o <fopts> : Specify fuse mount options. Will also disable automatic addition of FUSE's allow_other option!
 
   xopts:
     --cache <cfile> : Enable virtual write support.
       <cfile> specifies the cache file to use.
     --in <itype> <ifile> : Input image format and source file(s). May be specified multiple times.
       <itype> can be "aaff", "aff", "aff3", "vdi", "ewf", "aewf", "qcow", "qcow2", "qemu", "raw", "dd".
       <ifile> specifies the source file. If your image is split into multiple files, you have to specify them all!
     --inopts <iopts> : Specify input library specific options.
       <iopts> specifies a comma separated list of key=value options. See below for details.
     --info : Print out infos about used compiler and libraries.
     --morph <mtype> : Morphing function to apply to input image(s). If not specified, defaults to "combine".
       <mtype> can be "raid0", "unallocated", "combine".
     --morphopts <mopts> : Specify morphing library specific options.
       <mopts> specifies a comma separated list of key=value options. See below for details.
     --offset <off> : Move the output image data start <off> bytes into the input image(s).
     --out <otype> : Output image format. If not specified, defaults to "raw".
       <otype> can be "raw", "dmg", "vdi", "vhd", "vmdk", "vmdks".
     --owcache <file> : Same as --cache <file> but overwrites existing cache file.
     --rocache <file> : Same as --cache <file> but does **not** allow further writes.
     --sizelimit <size> : The data end of input image(s) is set to no more than <size> bytes after the data start.
     --version : Same as --info.
 
   mntp:
     Mount point where output image should be located.
 
 Infos:
   * One --in option and a mount point are mandatory!
   * If you specify --in multiple times, data from all images is morphed into one output image using the specified morphing function.
   * For VMDK emulation, you have to uncomment "user_allow_other" in /etc/fuse.conf or run xmount as root.
 
 Input / Morphing library specific options:
   Input / Morphing libraries might support an own set of options to configure / tune their behaviour.
   Libraries supporting this feature (if any) and their options are listed below.
 
   - libxmount_input_aaff.so
     aaffmaxmem   : Maximum amount of RAM cache, in MiB, for image seek offsets. Default: 10 MiB
     aafflog      : Log file name.
     Specify full path for aafflog. The given file name is extended by _<pid>.
     The AFF format has been declared as deprecated by its inventor!
 
   - libxmount_input_vdi.so
     vdilog       : Path for writing log file(must exist).
                    The files created in this directory will be named log_<pid>.
 
   - libxmount_input_aewf.so
     aewfmaxmem   : Maximum amount of RAM cache, in MiB, for image offset tables. Default: 10 MiB
     aewfmaxfiles : Maximum number of concurrently opened image segment files. Default: 10
     aewfstats    : Output statistics at regular intervals to this directory (must exist).
                    The files created in this directory will be named stats_<pid>.
     aewfrefresh  : The update interval, in seconds, for the statistics (aewfstats must be set). Default: 10s.
     aewflog      : Path for writing log file (must exist).
                    The files created in this directory will be named log_<pid>.
     aewfthreads  : Max. number of threads for parallelized decompression. Default: System CPUs (4)
                    A value of 1 switches back to old, single-threaded legacy functions.
 
   - libxmount_morphing_raid.so
     raid_chunksize : Specify the chunk size to use in bytes. Defaults to 524288 (512k).
 
   - libxmount_morphing_unallocated.so
     unallocated_fs : Specify the filesystem to extract unallocated blocks from. Supported filesystems are: 'hfs', 'fat'. Default: autodetect.
 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
