---
Title: havoc
Homepage: https://github.com/HavocFramework/Havoc
Repository: https://gitlab.com/kalilinux/packages/havoc
Architectures: amd64
Version: 0.6~git20240910.69ce17c-0kali1
Metapackages: kali-linux-everything 
Icon: images/havoc-logo.svg
PackagesInfo: |
 ### havoc
 
  Havoc is a modern, malleable post-exploitation command and control framework
  made for penetration testers, red teams, and blue teams.
 
 **Installed size:** `26.11 MB`  
 **How to install:** `sudo apt install havoc`  
 
 {{< spoiler "Dependencies:" >}}
 * gcc-mingw-w64-i686-win32
 * gcc-mingw-w64-x86-64-win32
 * libc6 
 * libgcc-s1 
 * libpython3.12t64 
 * libqt5core5t64 
 * libqt5gui5t64  | libqt5gui5-gles 
 * libqt5network5t64 
 * libqt5sql5t64 
 * libqt5websockets5 
 * libqt5widgets5t64 
 * libstdc++6 
 * nasm
 {{< /spoiler >}}
 
 ##### havoc
 
 
 ```
 root@kali:~# havoc -h
 Havoc Framework [Version: 0.7] [CodeName: Bites The Dust]
 
 Usage:
   havoc [flags]
   havoc [command]
 
 Available Commands:
   client      client command
   help        Help about any command
   server      teamserver command
 
 Flags:
   -h, --help   help for havoc
 
 Use "havoc [command] --help" for more information about a command.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
