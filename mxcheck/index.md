---
Title: mxcheck
Homepage: https://github.com/steffenfritz/mxcheck
Repository: https://gitlab.com/kalilinux/packages/mxcheck
Architectures: any
Version: 1.6.2-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### mxcheck
 
  mxcheck is an info scanner for e-mail servers, checking:
   
     - DNS records: A, MX, PTR, SPF, MTA-STS, DKIM, DMARC
     - AS Number and AS Country
     - The support of StartTLS and the certificate
     - Open ports: 25, 465, 587
     - If the service is listed by blacklists
     - If it leaks information by server string and VRFY command
     - If the server is an open relay
 
 **Installed size:** `6.49 MB`  
 **How to install:** `sudo apt install mxcheck`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### mxcheck
 
 
 ```
 root@kali:~# mxcheck -h
 
 Usage of mxcheck:
   -b, --blacklist              Check if the service is on blacklists
   -S, --dkim-selector string   The DKIM selector. If set a DKIM check is performed on the provided service domain
   -d, --dnsserver string       The dns server to be requested (default "8.8.8.8")
   -f, --mailfrom string        Set the mailFrom address (default "info@foo.wtf")
   -t, --mailto string          Set the mailTo address (default "info@baz.wtf")
   -n, --no-prompt              Answer yes to all questions
   -s, --service string         The service host to check
   -u, --updatecheck            Check for new version of mxcheck
   -v, --version                Version and license
   -w, --write-tsv              Write tsv formated report to file
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
