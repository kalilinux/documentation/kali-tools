---
Title: xsrfprobe
Homepage: https://github.com/0xInfection/XSRFProbe
Repository: https://gitlab.com/kalilinux/packages/xsrfprobe
Architectures: all
Version: 2.3.1-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### xsrfprobe
 
  XSRFProbe is an advanced Cross Site Request Forgery (CSRF/XSRF) Audit and
  Exploitation Toolkit. Equipped with a powerful crawling engine and numerous
  systematic checks, it is able to detect most cases of CSRF vulnerabilities,
  their related bypasses and further generate (maliciously) exploitable proof of
  concepts with each found vulnerability.
 
 **Installed size:** `190 KB`  
 **How to install:** `sudo apt install xsrfprobe`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-bs4
 * python3-requests
 * python3-stringdist
 * python3-tld
 * python3-yattag
 {{< /spoiler >}}
 
 ##### xsrfprobe
 
 
 ```
 root@kali:~# xsrfprobe -h
 
     XSRFProbe, A Cross Site Request Forgery Audit Toolkit
 
 usage: xsrfprobe -u <url> <args>
 
 Required Arguments:
   -u, --url URL         Main URL to test
 
 Optional Arguments:
   -c, --cookie COOKIE   Cookie value to be requested with each successive
                         request. If there are multiple cookies, separate them
                         with commas. For example: `-c PHPSESSID=i837c5n83u4,
                         _gid=jdhfbuysf`.
   -o, --output OUTPUT   Output directory where files to be stored. Default is
                         the output/ folder where all files generated will be
                         stored.
   -d, --delay DELAY     Time delay between requests in seconds. Default is
                         zero.
   -q, --quiet           Set the DEBUG mode to quiet. Report only when
                         vulnerabilities are found. Minimal output will be
                         printed on screen.
   -H, --headers HEADERS
                         Comma separated list of custom headers you'd want to
                         use. For example: ``--headers "Accept=text/php,
                         X-Requested-With=Dumb"``.
   -v, --verbose         Increase the verbosity of the output (e.g., -vv is
                         more than -v).
   -t, --timeout TIMEOUT
                         HTTP request timeout value in seconds. The entered
                         value may be either in floating point decimal or an
                         integer. Example: ``--timeout 10.0``
   -E, --exclude EXCLUDE
                         Comma separated list of paths or directories to be
                         excluded which are not in scope. These paths/dirs
                         won't be scanned. For example: `--exclude somepage/,
                         sensitive-dir/, pleasedontscan/`
   --user-agent USER_AGENT
                         Custom user-agent to be used. Only one user-agent can
                         be specified.
   --max-chars MAXCHARS  Maximum allowed character length for the custom token
                         value to be generated. For example: `--max-chars 5`.
                         Default value is 6.
   --crawl               Crawl the whole site and simultaneously test all
                         discovered endpoints for CSRF.
   --no-analysis         Skip the Post-Scan Analysis of Tokens which were
                         gathered during requests
   --malicious           Generate a malicious CSRF Form which can be used in
                         real-world exploits.
   --skip-poc            Skip the PoC Form Generation of POST-Based Cross Site
                         Request Forgeries.
   --no-verify           Do not verify SSL certificates with requests.
   --display             Print out response headers of requests while making
                         requests.
   --update              Update XSRFProbe to latest version on GitHub via git.
   --random-agent        Use random user-agents for making requests.
   --version             Display the version of XSRFProbe and exit.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
