---
Title: sprayhound
Homepage: https://github.com/Hackndo/sprayhound
Repository: https://gitlab.com/kalilinux/packages/sprayhound
Architectures: all
Version: 0.0~git20241231.5e7bf94-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sprayhound
 
  SprayHound is a Python library to safely password spray in Active Directory,
  which sets pwned users as owned in Bloodhound and detects paths to Domain
  Admins.
 
 **Installed size:** `68 KB`  
 **How to install:** `sudo apt install sprayhound`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-ldap3
 * python3-neo4j
 * python3-pkg-resources
 {{< /spoiler >}}
 
 ##### sprayhound
 
 
 ```
 root@kali:~# sprayhound -h
 usage: sprayhound [-h] [-u USERNAME] [-U USERFILE] [-p PASSWORD | --lower |
                   --upper] [-t THRESHOLD] [-dc DOMAIN_CONTROLLER] [-d DOMAIN]
                   [-lP LDAP_PORT] [-lu LDAP_USER] [-lp LDAP_PASS] [-lssl]
                   [-lpage LDAP_PAGE_SIZE] [-nh NEO4J_HOST] [-nP NEO4J_PORT]
                   [-nu NEO4J_USER] [-np NEO4J_PASS] [--unsafe] [--force]
                   [--nocolor] [-v]
 
 sprayhound v0.0.4 - Password spraying
 
 options:
   -h, --help            show this help message and exit
   --unsafe              Enable login tries on almost locked out accounts
   --force               Do not prompt for user confirmation
   --nocolor             Do not use color for output
   -v                    Verbosity level (-v or -vv)
 
 credentials:
   -u, --username USERNAME
                         Username
   -U, --userfile USERFILE
                         File containing username list
   -p, --password PASSWORD
                         Password
   --lower               User as pass with lowercase password
   --upper               User as pass with uppercase password
   -t, --threshold THRESHOLD
                         Number of password left allowed before locked out
 
 ldap:
   -dc, --domain-controller DOMAIN_CONTROLLER
                         Domain controller
   -d, --domain DOMAIN   Domain FQDN
   -lP, --ldap-port LDAP_PORT
                         LDAP Port
   -lu, --ldap-user LDAP_USER
                         LDAP User
   -lp, --ldap-pass LDAP_PASS
                         LDAP Password
   -lssl, --ldap-ssl     LDAP over TLS (ldaps)
   -lpage, --ldap-page-size LDAP_PAGE_SIZE
                         LDAP Paging size (Default: 200)
 
 neo4j:
   -nh, --neo4j-host NEO4J_HOST
                         Neo4J Host (Default: 127.0.0.1)
   -nP, --neo4j-port NEO4J_PORT
                         Neo4J Port (Default: 7687)
   -nu, --neo4j-user NEO4J_USER
                         Neo4J user (Default: neo4j)
   -np, --neo4j-pass NEO4J_PASS
                         Neo4J password (Default: neo4j)
 
 example:
     sprayhound -d adsec.local -p Winter202
     sprayhound -U userlist.txt -d adsec.local
     
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
