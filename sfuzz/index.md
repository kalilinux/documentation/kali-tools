---
Title: sfuzz
Homepage: http://aconole.brad-x.com/programs/sfuzz.html
Repository: https://gitlab.com/kalilinux/packages/sfuzz
Architectures: any
Version: 0.7.0-1kali4
Metapackages: kali-linux-everything kali-linux-large kali-tools-fuzzing kali-tools-vulnerability 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sfuzz
 
  In the same vein as the Generic Protocol Framework, sfuzz is
  a really simple to use black box testing suite called Simple
  Fuzzer (what else would you expect?). The goal is to provide
  a simple to use, but fairly powerful and flexible black box
  testing utility.
 
 **Installed size:** `191 KB`  
 **How to install:** `sudo apt install sfuzz`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### sfo
 
 
 ```
 root@kali:~# sfo -h
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103966].
 [103966] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103967].
 [103967] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103968].
 [103968] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103969].
 [103969] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103970].
 [103970] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103971].
 [103971] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103980].
 [103980] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103981].
 [103981] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103982].
 [103982] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103983].
 [103983] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103984].
 [103984] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103985].
 [103985] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103986].
 [103986] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103987].
 [103987] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103988].
 [103988] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103997].
 [103997] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103998].
 [103998] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [103999].
 [103999] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104000].
 [104000] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104001].
 [104001] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104002].
 [104002] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104003].
 [104003] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104004].
 [104004] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104010].
 [104010] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104014].
 [104014] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104015].
 [104015] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104016].
 [104016] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [104017].
 [104017] Exited (possibly normal), status[255]
  [sfo] === spawning!
 [] Attempting to spawn a monitored task.
 [SFUZZ-ORACLE] attached [10401
 ```
 
 - - -
 
 ##### sfuzz
 
 
 ```
 root@kali:~# sfuzz -h
 		Simple Fuzzer
 By:	 Aaron Conole
 version: 0.7.0
 url:	 http://aconole.brad-x.com/programs/sfuzz.html
 EMAIL:	 apconole@yahoo.com
 Build-prefix: /usr
 	-h	 This message.
 	-V	 Version information.
 
 networking / output:
 	-v	 Verbose output
 	-q	 Silent output mode (generally for CLI fuzzing)
 	-X	 prints the output in hex
 
 	-b	 Begin fuzzing at the test specified.
 	-e	 End testing on failure.
 	-t	 Wait time for reading the socket
 	-S	 Remote host
 	-p	 Port
 	-T|-U|-O TCP|UDP|Output mode
 	-R	 Refrain from closing connections (ie: "leak" them)
 
 	-f	 Config File
 	-L	 Log file
 	-n	 Create a new logfile after each fuzz
 	-r	 Trim the tailing newline
 	-D	 Define a symbol and value (X=y).
 	-l	 Only perform literal fuzzing
 	-s	 Only perform sequence fuzzing
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## sfuzz Usage Example

Fuzz the target server (`-S 192.168.1.1`) on port 10443 (`-p 10443`) with TCP output mode (`-T`), using the basic HTTP config (`-f /usr/share/sfuzz/sfuzz-sample/basic.http`):

```console
root@kali:~# sfuzz -S 192.168.1.1 -p 10443 -T -f /usr/share/sfuzz/sfuzz-sample/basic.http
[12:53:47] dumping options:
    filename: </usr/share/sfuzz/sfuzz-sample/basic.http>
    state:    <8>
    lineno:   <56>
    literals:  [74]
    sequences: [34]
    symbols: [0]
    req_del:  <200>
    mseq_len: <10024>
    plugin: <none>
    s_syms: <0>
    literal[1] = [AREALLYBADSTRING]
```
