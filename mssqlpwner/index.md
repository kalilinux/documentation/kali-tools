---
Title: mssqlpwner
Homepage: https://github.com/ScorpionesLabs/MSSqlPwner
Repository: https://gitlab.com/kalilinux/packages/mssqlpwner
Architectures: all
Version: 1.3.2+git20241001.a30f41f-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### mssqlpwner
 
  MSSqlPwner is an advanced and versatile pentesting tool designed to seamlessly
  interact and pwn MSSQL servers. That tool is based on impacket, which allows
  attackers to authenticate to databases using clear-text passwords NTLM Hashes,
  and kerberos tickets. With MSSqlPwner, users can execute custom commands
  through various methods, including custom assembly, `xp_cmdshell`, and
  `sp_oacreate(Ole Automation Procedures)` and much more.
 
 **Installed size:** `239 KB`  
 **How to install:** `sudo apt install mssqlpwner`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-impacket
 * python3-prompt-toolkit
 * python3-termcolor
 {{< /spoiler >}}
 
 ##### mssqlpwner
 
 
 ```
 root@kali:~# mssqlpwner -h
 usage: mssqlpwner [-h] [-port PORT] [-timeout TIMEOUT] [-db DB]
                   [-windows-auth] [-no-state] [-debug] [-hashes LMHASH:NTHASH]
                   [-no-pass] [-k] [-aesKey hex key] [-dc-ip ip address]
                   [-link-name LINK_NAME] [-max-link-depth MAX_LINK_DEPTH]
                   [-max-impersonation-depth MAX_IMPERSONATION_DEPTH]
                   [-chain-id CHAIN_ID] [-auto-yes]
                   target
                   {enumerate,set-chain,rev2self,get-rev2self-queries,get-chain-list,get-link-server-list,get-adsi-provider-list,set-link-server,exec,ntlm-relay,custom-asm,inject-custom-asm,direct-query,retrieve-password,interactive,brute} ...
 
 TDS client implementation (SSL supported).
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 Options:
   -h, --help            show this help message and exit
   -port PORT            target MSSQL port (default 1433)
   -timeout TIMEOUT      timeout in seconds (default 30)
   -db DB                MSSQL database instance (default None)
   -windows-auth         whether or not to use Windows Authentication (default
                         False)
   -no-state             whether or not to load existing state
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
 
 Choose module:
   -link-name LINK_NAME  Linked server to launch queries
   -max-link-depth MAX_LINK_DEPTH
                         Maximum links you want to depth recursively
   -max-impersonation-depth MAX_IMPERSONATION_DEPTH
                         Maximum impersonation you want to depth in each link
   -chain-id CHAIN_ID    Chain ID to use
   -auto-yes             Auto answer yes to all questions
 
 Modules:
   {enumerate,set-chain,rev2self,get-rev2self-queries,get-chain-list,get-link-server-list,get-adsi-provider-list,set-link-server,exec,ntlm-relay,custom-asm,inject-custom-asm,direct-query,retrieve-password,interactive,brute}
     enumerate           Enumerate MSSQL server
     set-chain           Set chain ID (For interactive-mode only!)
     rev2self            Revert to SELF (For interactive-mode only!)
     get-rev2self-queries
                         Retrieve queries to revert to SELF (For interactive-
                         mode only!)
     get-chain-list      Get chain list
     get-link-server-list
                         Get linked server list
     get-adsi-provider-list
                         Get ADSI provider list
     set-link-server     Set link server (For interactive-mode only!)
     exec                Command to execute
     ntlm-relay          Steal NetNTLM hash / Relay attack
     custom-asm          Execute procedures using custom assembly
     inject-custom-asm   Code injection using custom assembly
     direct-query        Execute direct query
     retrieve-password   Retrieve password from ADSI servers
     interactive         Interactive Mode
     brute               Brute force
 usage: mssqlpwner [-h] [-port PORT] [-timeout TIMEOUT] [-db DB]
                   [-windows-auth] [-no-state] [-debug] [-hashes LMHASH:NTHASH]
                   [-no-pass] [-k] [-aesKey hex key] [-dc-ip ip address]
                   [-link-name LINK_NAME] [-max-link-depth MAX_LINK_DEPTH]
                   [-max-impersonation-depth MAX_IMPERSONATION_DEPTH]
                   [-chain-id CHAIN_ID] [-auto-yes]
                   target
                   {enumerate,set-chain,rev2self,get-rev2self-queries,get-chain-list,get-link-server-list,get-adsi-provider-list,set-link-server,exec,ntlm-relay,custom-asm,inject-custom-asm,direct-query,retrieve-password,interactive,brute} ...
 
 TDS client implementation (SSL supported).
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 Options:
   -h, --help            show this help message and exit
   -port PORT            target MSSQL port (default 1433)
   -timeout TIMEOUT      timeout in seconds (default 30)
   -db DB                MSSQL database instance (default None)
   -windows-auth         whether or not to use Windows Authentication (default
                         False)
   -no-state             whether or not to load existing state
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
 
 Choose module:
   -link-name LINK_NAME  Linked server to launch queries
   -max-link-depth MAX_LINK_DEPTH
                         Maximum links you want to depth recursively
   -max-impersonation-depth MAX_IMPERSONATION_DEPTH
                         Maximum impersonation you want to depth in each link
   -chain-id CHAIN_ID    Chain ID to use
   -auto-yes             Auto answer yes to all questions
 
 Modules:
   {enumerate,set-chain,rev2self,get-rev2self-queries,get-chain-list,get-link-server-list,get-adsi-provider-list,set-link-server,exec,ntlm-relay,custom-asm,inject-custom-asm,direct-query,retrieve-password,interactive,brute}
     enumerate           Enumerate MSSQL server
     set-chain           Set chain ID (For interactive-mode only!)
     rev2self            Revert to SELF (For interactive-mode only!)
     get-rev2self-queries
                         Retrieve queries to revert to SELF (For interactive-
                         mode only!)
     get-chain-list      Get chain list
     get-link-server-list
                         Get linked server list
     get-adsi-provider-list
                         Get ADSI provider list
     set-link-server     Set link server (For interactive-mode only!)
     exec                Command to execute
     ntlm-relay          Steal NetNTLM hash / Relay attack
     custom-asm          Execute procedures using custom assembly
     inject-custom-asm   Code injection using custom assembly
     direct-query        Execute direct query
     retrieve-password   Retrieve password from ADSI servers
     interactive         Interactive Mode
     brute               Brute force
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
