---
Title: netscanner
Homepage: https://github.com/Chleba/netscanner
Repository: https://gitlab.com/kalilinux/packages/netscanner
Architectures: any
Version: 0.6.2-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### netscanner
 
  Netscanner is a network scanning tool with features like:
      - List HW Interfaces
      - Switching active Interface for scanning & packet-dumping
      - WiFi networks scanning
      - WiFi signals strength (with charts)
      - (IPv4) Pinging CIDR with hostname, oui & mac address
      - (IPv4) Packetdump (TCP, UDP, ICMP, ARP)
      - (IPv6) Packetdump (ICMP6)
      - start/pause packetdump
 
 **Installed size:** `11.93 MB`  
 **How to install:** `sudo apt install netscanner`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libgcc-s1 
 {{< /spoiler >}}
 
 ##### netscanner
 
 
 ```
 root@kali:~# netscanner -h
 Network Scanner
 
 Usage: netscanner [OPTIONS]
 
 Options:
   -t, --tick-rate <FLOAT>   Tick rate, i.e. number of ticks per second [default:
                             1]
   -f, --frame-rate <FLOAT>  Frame rate, i.e. number of frames per second
                             [default: 10]
   -h, --help                Print help
   -V, --version             Print version
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
