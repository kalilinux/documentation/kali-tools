---
Title: dfwinreg
Homepage: https://github.com/log2timeline/dfwinreg
Repository: https://salsa.debian.org/pkg-security-team/dfwinreg
Architectures: all
Version: 20240316-1
Metapackages: kali-linux-everything kali-tools-forensics kali-tools-respond 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### python3-dfwinreg
 
  dfWinReg, or Digital Forensics Windows Registry, provides read-only
  access to Windows Registry objects. The goal of dfWinReg is to
  provide a generic interface for accessing Windows Registry objects
  that resembles the Registry key hierarchy as seen on a live Windows
  system.
 
 **Installed size:** `1.05 MB`  
 **How to install:** `sudo apt install python3-dfwinreg`  
 
 {{< spoiler "Dependencies:" >}}
 * libjs-jquery 
 * libjs-sphinxdoc 
 * python3
 * python3-dfdatetime 
 * python3-dtfabric 
 * python3-libregf 
 * python3-yaml 
 * sphinx-rtd-theme-common 
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
