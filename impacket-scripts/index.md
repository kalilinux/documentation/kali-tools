---
Title: impacket-scripts
Homepage: 
Repository: https://gitlab.com/kalilinux/packages/impacket-scripts
Architectures: all
Version: 1.10
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-information-gathering kali-tools-respond kali-tools-vulnerability 
Icon: images/impacket-scripts-logo.svg
PackagesInfo: |
 ### impacket-scripts
 
  This package contains links to useful impacket scripts. It's a separate
  package to keep impacket package from Debian and have the useful scripts
  in the path for Kali.
 
 **Installed size:** `65 KB`  
 **How to install:** `sudo apt install impacket-scripts`  
 
 {{< spoiler "Dependencies:" >}}
 * python3-dnspython
 * python3-dsinternals
 * python3-impacket 
 * python3-ldap3 
 * python3-ldapdomaindump
 * python3-pcapy
 {{< /spoiler >}}
 
 ##### impacket-DumpNTLMInfo
 
 
 ```
 root@kali:~# impacket-DumpNTLMInfo -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: DumpNTLMInfo.py [-h] [-debug] [-target-ip ip address]
                        [-port destination port] [-protocol [protocol]]
                        target
 
 Do ntlm authentication and parse information.
 
 positional arguments:
   target                <targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port destination port
                         Destination port to connect to SMB/RPC Server
   -protocol [protocol]  Protocol to use (SMB or RPC). Default is SMB, port 135
                         uses RPC normally.
 ```
 
 - - -
 
 ##### impacket-Get-GPPPassword
 
 
 ```
 root@kali:~# impacket-Get-GPPPassword -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: Get-GPPPassword.py [-h] [-xmlfile XMLFILE] [-share SHARE]
                           [-base-dir BASE_DIR] [-ts] [-debug]
                           [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                           [-aesKey hex key] [-dc-ip ip address]
                           [-target-ip ip address] [-port [destination port]]
                           target
 
 Group Policy Preferences passwords finder and decryptor.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
                         or LOCAL (if you want to parse local files)
 
 options:
   -h, --help            show this help message and exit
   -xmlfile XMLFILE      Group Policy Preferences XML files to parse
   -share SHARE          SMB Share
   -base-dir BASE_DIR    Directory to search in (Default: /)
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              Don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-GetADComputers
 
 
 ```
 root@kali:~# impacket-GetADComputers -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: GetADComputers.py [-h] [-user username] [-ts] [-debug] [-resolveIP]
                          [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                          [-aesKey hex key] [-dc-ip ip address]
                          [-dc-host hostname]
                          target
 
 Queries target domain for computer data
 
 positional arguments:
   target                domain[/username[:password]]
 
 options:
   -h, --help            show this help message and exit
   -user username        Requests data for specific user
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -resolveIP            Tries to resolve the IP address of computer objects,
                         by performing the nslookup on the DC.
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-GetADUsers
 
 
 ```
 root@kali:~# impacket-GetADUsers -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: GetADUsers.py [-h] [-user username] [-all] [-ts] [-debug]
                      [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                      [-dc-ip ip address] [-dc-host hostname]
                      target
 
 Queries target domain for users data
 
 positional arguments:
   target                domain[/username[:password]]
 
 options:
   -h, --help            show this help message and exit
   -user username        Requests data for specific user
   -all                  Return all users, including those with no email
                         addresses and disabled accounts. When used with -user
                         it will return user's info even if the account is
                         disabled
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-GetLAPSPassword
 
 
 ```
 root@kali:~# impacket-GetLAPSPassword -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: GetLAPSPassword.py [-h] [-computer computername] [-ts] [-debug]
                           [-outputfile OUTPUTFILE] [-hashes LMHASH:NTHASH]
                           [-no-pass] [-k] [-aesKey hex key]
                           [-dc-ip ip address] [-dc-host hostname]
                           target
 
 Extract LAPS passwords from LDAP
 
 positional arguments:
   target                domain[/username[:password]]
 
 options:
   -h, --help            show this help message and exit
   -computer computername
                         Target a specific computer by its name
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -outputfile, -o OUTPUTFILE
                         Outputs to a file.
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CcnAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-GetNPUsers
 
 
 ```
 root@kali:~# impacket-GetNPUsers -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: GetNPUsers.py [-h] [-request] [-outputfile OUTPUTFILE]
                      [-format {hashcat,john}] [-usersfile USERSFILE] [-ts]
                      [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                      [-aesKey hex key] [-dc-ip ip address] [-dc-host hostname]
                      target
 
 Queries target domain for users with 'Do not require Kerberos
 preauthentication' set and export their TGTs for cracking
 
 positional arguments:
   target                [[domain/]username[:password]]
 
 options:
   -h, --help            show this help message and exit
   -request              Requests TGT for users and output them in JtR/hashcat
                         format (default False)
   -outputfile OUTPUTFILE
                         Output filename to write ciphers in JtR/hashcat format
   -format {hashcat,john}
                         format to save the AS_REQ of users without pre-
                         authentication. Default is hashcat
   -usersfile USERSFILE  File with user per line to test
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-GetUserSPNs
 
 
 ```
 root@kali:~# impacket-GetUserSPNs -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: GetUserSPNs.py [-h] [-target-domain TARGET_DOMAIN]
                       [-no-preauth NO_PREAUTH] [-stealth]
                       [-usersfile USERSFILE] [-request]
                       [-request-user username] [-save]
                       [-outputfile OUTPUTFILE] [-ts] [-debug]
                       [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                       [-aesKey hex key] [-dc-ip ip address]
                       [-dc-host hostname]
                       target
 
 Queries target domain for SPNs that are running under a user account
 
 positional arguments:
   target                domain[/username[:password]]
 
 options:
   -h, --help            show this help message and exit
   -target-domain TARGET_DOMAIN
                         Domain to query/request if different than the domain
                         of the user. Allows for Kerberoasting across trusts.
   -no-preauth NO_PREAUTH
                         account that does not require preauth, to obtain
                         Service Ticket through the AS
   -stealth              Removes the (servicePrincipalName=*) filter from the
                         LDAP query for added stealth. May cause huge memory
                         consumption / errors on large domains.
   -usersfile USERSFILE  File with user per line to test
   -request              Requests TGS for users and output them in JtR/hashcat
                         format (default False)
   -request-user username
                         Requests TGS for the SPN associated to the user
                         specified (just the username, no domain needed)
   -save                 Saves TGS requested to disk. Format is
                         <username>.ccache. Auto selects -request
   -outputfile OUTPUTFILE
                         Output filename to write ciphers in JtR/hashcat
                         format. Auto selects -request
   -ts                   Adds timestamp to every logging output.
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter. Ignoredif -target-domain is specified.
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-addcomputer
 
 
 ```
 root@kali:~# impacket-addcomputer -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: addcomputer.py [-h] [-domain-netbios NETBIOSNAME]
                       [-computer-name COMPUTER-NAME$]
                       [-computer-pass password] [-no-add] [-delete] [-debug]
                       [-method {SAMR,LDAPS}] [-port {139,445,636}]
                       [-baseDN DC=test,DC=local]
                       [-computer-group CN=Computers,DC=test,DC=local]
                       [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                       [-aesKey hex key] [-dc-host hostname] [-dc-ip ip]
                       [domain/]username[:password]
 
 Adds a computer account to domain
 
 positional arguments:
   [domain/]username[:password]
                         Account used to authenticate to DC.
 
 options:
   -h, --help            show this help message and exit
   -domain-netbios NETBIOSNAME
                         Domain NetBIOS name. Required if the DC has multiple
                         domains.
   -computer-name COMPUTER-NAME$
                         Name of computer to add.If omitted, a random
                         DESKTOP-[A-Z0-9]{8} will be used.
   -computer-pass password
                         Password to set to computerIf omitted, a random
                         [A-Za-z0-9]{32} will be used.
   -no-add               Don't add a computer, only set password on existing
                         one.
   -delete               Delete an existing computer.
   -debug                Turn DEBUG output ON
   -method {SAMR,LDAPS}  Method of adding the computer.SAMR works over
                         SMB.LDAPS has some certificate requirementsand isn't
                         always available.
   -port {139,445,636}   Destination port to connect to. SAMR defaults to 445,
                         LDAPS to 636.
 
 LDAP:
   -baseDN DC=test,DC=local
                         Set baseDN for LDAP.If ommited, the domain part (FQDN)
                         specified in the account parameter will be used.
   -computer-group CN=Computers,DC=test,DC=local
                         Group to which the account will be added.If omitted,
                         CN=Computers will be used,
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on account parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
   -dc-ip ip             IP of the domain controller to use. Useful if you
                         can't translate the FQDN.specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-atexec
 
 
 ```
 root@kali:~# impacket-atexec -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: atexec.py [-h] [-session-id SESSION_ID] [-ts] [-silentcommand] [-debug]
                  [-codec CODEC] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                  [-aesKey hex key] [-dc-ip ip address] [-keytab KEYTAB]
                  target [command ...]
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   command               command to execute at the target
 
 options:
   -h, --help            show this help message and exit
   -session-id SESSION_ID
                         an existed logon session to use (no output, no
                         cmd.exe)
   -ts                   adds timestamp to every logging output
   -silentcommand        does not execute cmd.exe to run given command (no
                         output)
   -debug                Turn DEBUG output ON
   -codec CODEC          Sets encoding used (codec) from the target's output
                         (default "utf-8"). If errors are detected, run
                         chcp.com at the target, map the result with https://do
                         cs.python.org/3/library/codecs.html#standard-encodings
                         and then execute wmiexec.py again with -codec and the
                         corresponding codec
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -keytab KEYTAB        Read keys for SPN from keytab file
 ```
 
 - - -
 
 ##### impacket-changepasswd
 
 
 ```
 root@kali:~# impacket-changepasswd -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: changepasswd.py [-h] [-ts] [-debug] [-newpass NEWPASS |
                        -newhashes LMHASH:NTHASH] [-hashes LMHASH:NTHASH]
                        [-no-pass] [-altuser ALTUSER] [-altpass ALTPASS |
                        -althash ALTHASH]
                        [-protocol {smb-samr,rpc-samr,kpasswd,ldap}] [-reset]
                        [-k] [-aesKey hex key] [-dc-ip ip address]
                        target
 
 Change or reset passwords over different protocols.
 
 positional arguments:
   target                [[domain/]username[:password]@]<hostname or address>
 
 options:
   -h, --help            show this help message and exit
   -ts                   adds timestamp to every logging output
   -debug                turn DEBUG output ON
 
 New credentials for target:
   -newpass NEWPASS      new password
   -newhashes LMHASH:NTHASH
                         new NTLM hashes, format is NTHASH or LMHASH:NTHASH
 
 Authentication (target user whose password is changed):
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is NTHASH or LMHASH:NTHASH
   -no-pass              Don't ask for password (useful for Kerberos, -k)
 
 Authentication (optional, privileged user performing the change):
   -altuser ALTUSER      Alternative username
   -altpass ALTPASS      Alternative password
   -althash, -althashes ALTHASH
                         Alternative NT hash, format is NTHASH or LMHASH:NTHASH
 
 Method of operations:
   -protocol, -p {smb-samr,rpc-samr,kpasswd,ldap}
                         Protocol to use for password change/reset
   -reset, -admin        Try to reset the password with privileges (may bypass
                         some password policies)
 
 Kerberos authentication:
   Applicable to the authenticating user (-altuser if defined, else target)
 
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller, for Kerberos. If
                         omitted it will use the domain part (FQDN) specified
                         in the target parameter
 ```
 
 - - -
 
 ##### impacket-dacledit
 
 
 ```
 root@kali:~# impacket-dacledit -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: dacledit.py [-h] [-use-ldaps] [-ts] [-debug] [-hashes LMHASH:NTHASH]
                    [-no-pass] [-k] [-aesKey hex key] [-dc-ip ip address]
                    [-principal NAME] [-principal-sid SID] [-principal-dn DN]
                    [-target NAME] [-target-sid SID] [-target-dn DN]
                    [-action [{read,write,remove,backup,restore}]]
                    [-file FILENAME] [-ace-type [{allowed,denied}]]
                    [-rights [{FullControl,ResetPassword,WriteMembers,DCSync}]]
                    [-rights-guid RIGHTS_GUID] [-inheritance]
                    identity
 
 Python editor for a principal's DACL.
 
 positional arguments:
   identity              domain.local/username[:password]
 
 options:
   -h, --help            show this help message and exit
   -use-ldaps            Use LDAPS instead of LDAP
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication & connection:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller or KDC (Key
                         Distribution Center) for Kerberos. If omitted it will
                         use the domain part (FQDN) specified in the identity
                         parameter
 
 principal:
   Object, controlled by the attacker, to reference in the ACE to create or
   to filter when printing a DACL
 
   -principal NAME       sAMAccountName
   -principal-sid SID    Security IDentifier
   -principal-dn DN      Distinguished Name
 
 target:
   Principal object to read/edit the DACL of
 
   -target NAME          sAMAccountName
   -target-sid SID       Security IDentifier
   -target-dn DN         Distinguished Name
 
 dacl editor:
   -action [{read,write,remove,backup,restore}]
                         Action to operate on the DACL
   -file FILENAME        Filename/path (optional for -action backup, required
                         for -restore))
   -ace-type [{allowed,denied}]
                         The ACE Type (access allowed or denied) that must be
                         added or removed (default: allowed)
   -rights [{FullControl,ResetPassword,WriteMembers,DCSync}]
                         Rights to write/remove in the target DACL (default:
                         FullControl)
   -rights-guid RIGHTS_GUID
                         Manual GUID representing the right to write/remove
   -inheritance          Enable the inheritance in the ACE flag with
                         CONTAINER_INHERIT_ACE and OBJECT_INHERIT_ACE. Useful
                         when target is a Container or an OU, ACE will be
                         inherited by objects within the container/OU (except
                         objects with adminCount=1)
 ```
 
 - - -
 
 ##### impacket-dcomexec
 
 
 ```
 root@kali:~# impacket-dcomexec -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: dcomexec.py [-h] [-share SHARE] [-nooutput] [-ts] [-debug]
                    [-codec CODEC]
                    [-object [{ShellWindows,ShellBrowserWindow,MMC20}]]
                    [-com-version MAJOR_VERSION:MINOR_VERSION]
                    [-shell-type {cmd,powershell}] [-silentcommand]
                    [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                    [-dc-ip ip address] [-A authfile] [-keytab KEYTAB]
                    target [command ...]
 
 Executes a semi-interactive shell using the ShellBrowserWindow DCOM object.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   command               command to execute at the target. If empty it will
                         launch a semi-interactive shell
 
 options:
   -h, --help            show this help message and exit
   -share SHARE          share where the output will be grabbed from (default
                         ADMIN$)
   -nooutput             whether or not to print the output (no SMB connection
                         created)
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -codec CODEC          Sets encoding used (codec) from the target's output
                         (default "utf-8"). If errors are detected, run
                         chcp.com at the target, map the result with https://do
                         cs.python.org/3/library/codecs.html#standard-encodings
                         and then execute wmiexec.py again with -codec and the
                         corresponding codec
   -object [{ShellWindows,ShellBrowserWindow,MMC20}]
                         DCOM object to be used to execute the shell command
                         (default=ShellWindows)
   -com-version MAJOR_VERSION:MINOR_VERSION
                         DCOM version, format is MAJOR_VERSION:MINOR_VERSION
                         e.g. 5.7
   -shell-type {cmd,powershell}
                         choose a command processor for the semi-interactive
                         shell
   -silentcommand        does not execute cmd.exe to run given command (no
                         output, cannot run dir/cd/etc.)
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -A authfile           smbclient/mount.cifs-style authentication file. See
                         smbclient man page's -A option.
   -keytab KEYTAB        Read keys for SPN from keytab file
 ```
 
 - - -
 
 ##### impacket-describeTicket
 
 
 ```
 root@kali:~# impacket-describeTicket -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: describeTicket.py [-h] [-debug] [-ts] [-p PASSWORD] [-hp HEXPASSWORD]
                          [-u USER] [-d DOMAIN] [-s SALT] [--rc4 RC4]
                          [--aes HEXKEY] [--asrep-key HEXKEY]
                          ticket
 
 Ticket describer. Parses ticket, decrypts the enc-part, and parses the PAC.
 
 positional arguments:
   ticket                Path to ticket.ccache
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
   -ts                   Adds timestamp to every logging output
 
 Ticket decryption credentials (optional):
   Tickets carry a set of information encrypted by one of the target service
   account's Kerberos keys.(example: if the ticket is for user:"john" for
   service:"cifs/service.domain.local", you need to supply credentials or
   keys of the service account who owns SPN "cifs/service.domain.local")
 
   -p, --password PASSWORD
                         Cleartext password of the service account
   -hp, --hex-password HEXPASSWORD
                         Hex password of the service account
   -u, --user USER       Name of the service account
   -d, --domain DOMAIN   FQDN Domain
   -s, --salt SALT       Salt for keys calculation (DOMAIN.LOCALSomeuser for
                         users, DOMAIN.LOCALhostsomemachine.domain.local for
                         machines)
   --rc4 RC4             RC4 KEY (i.e. NT hash)
   --aes HEXKEY          AES128 or AES256 key
 
 PAC Credentials decryption material:
   [MS-PAC] section 2.6 (PAC Credentials) describes an element that is used
   to send credentials for alternate security protocols to the client during
   initial logon.This PAC credentials is typically used when PKINIT is
   conducted for pre-authentication. This structure contains LM and NT
   hashes.The information is encrypted using the AS reply key. Attack
   primitive known as UnPAC-the-Hash.
   (https://www.thehacker.recipes/ad/movement/kerberos/unpac-the-hash)
 
   --asrep-key HEXKEY    AS reply key for PAC Credentials decryption
 ```
 
 - - -
 
 ##### impacket-dpapi
 
 
 ```
 root@kali:~# impacket-dpapi -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: dpapi.py [-h] [-debug]
                 {backupkeys,masterkey,credential,vault,unprotect,credhist} ...
 
 Example for using the DPAPI/Vault structures to unlock Windows Secrets.
 
 positional arguments:
   {backupkeys,masterkey,credential,vault,unprotect,credhist}
                         actions
     backupkeys          domain backup key related functions
     masterkey           masterkey related functions
     credential          credential related functions
     vault               vault credential related functions
     unprotect           Provides CryptUnprotectData functionality
     credhist            CREDHIST related functions
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
 ```
 
 - - -
 
 ##### impacket-esentutl
 
 
 ```
 root@kali:~# impacket-esentutl -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: esentutl.py [-h] [-debug] [-page PAGE]
                    databaseFile {dump,info,export} ...
 
 Extensive Storage Engine utility. Allows dumping catalog, pages and tables.
 
 positional arguments:
   databaseFile        ESE to open
   {dump,info,export}  actions
     dump              dumps an specific page
     info              dumps the catalog info for the DB
     export            dumps the catalog info for the DB
 
 options:
   -h, --help          show this help message and exit
   -debug              Turn DEBUG output ON
   -page PAGE          page to open
 ```
 
 - - -
 
 ##### impacket-exchanger
 
 
 ```
 root@kali:~# impacket-exchanger -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: exchanger.py [-h] [-debug] [-rpc-hostname RPC_HOSTNAME]
                     [-hashes LMHASH:NTHASH]
                     target {nspi} ...
 
 A tool to abuse Exchange services
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   {nspi}                A module name
     nspi                Attack NSPI interface
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG and EXTENDED output ON
   -rpc-hostname RPC_HOSTNAME
                         A name of the server in GUID (preferred) or NetBIOS
                         name format (see description in the beggining of this
                         file)
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
 ```
 
 - - -
 
 ##### impacket-findDelegation
 
 
 ```
 root@kali:~# impacket-findDelegation -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: findDelegation.py [-h] [-target-domain TARGET_DOMAIN] [-ts] [-debug]
                          [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                          [-aesKey hex key] [-dc-ip ip address]
                          [-dc-host hostname]
                          target
 
 Queries target domain for delegation relationships
 
 positional arguments:
   target                domain[/username[:password]]
 
 options:
   -h, --help            show this help message and exit
   -target-domain TARGET_DOMAIN
                         Domain to query/request if different than the domain
                         of the user. Allows for retrieving delegation info
                         across trusts.
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter. Ignoredif -target-domain is specified.
   -dc-host hostname     Hostname of the domain controller to use. If ommited,
                         the domain part (FQDN) specified in the account
                         parameter will be used
 ```
 
 - - -
 
 ##### impacket-getArch
 
 
 ```
 root@kali:~# impacket-getArch -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: getArch.py [-h] [-target TARGET] [-targets TARGETS] [-timeout TIMEOUT]
                   [-debug]
 
 Gets the target system's OS architecture version
 
 options:
   -h, --help        show this help message and exit
   -target TARGET    <targetName or address>
   -targets TARGETS  input file with targets system to query Arch from (one per
                     line).
   -timeout TIMEOUT  socket timeout out when connecting to the target (default
                     2 sec)
   -debug            Turn DEBUG output ON
 ```
 
 - - -
 
 ##### impacket-getPac
 
 
 ```
 root@kali:~# impacket-getPac -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: getPac.py [-h] -targetUser TARGETUSER [-debug] [-hashes LMHASH:NTHASH]
                  credentials
 
 positional arguments:
   credentials           domain/username[:password]. Valid domain credentials
                         to use for grabbing targetUser's PAC
 
 options:
   -h, --help            show this help message and exit
   -targetUser TARGETUSER
                         the target user to retrieve the PAC of
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
 ```
 
 - - -
 
 ##### impacket-getST
 
 
 ```
 root@kali:~# impacket-getST -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: getST.py [-h] [-spn SPN] [-altservice ALTSERVICE]
                 [-impersonate IMPERSONATE] [-additional-ticket ticket.ccache]
                 [-ts] [-debug] [-u2u] [-self] [-force-forwardable] [-renew]
                 [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                 [-dc-ip ip address]
                 identity
 
 Given a password, hash or aesKey, it will request a Service Ticket and save it
 as ccache
 
 positional arguments:
   identity              [domain/]username[:password]
 
 options:
   -h, --help            show this help message and exit
   -spn SPN              SPN (service/server) of the target service the service
                         ticket will be generated for
   -altservice ALTSERVICE
                         New sname/SPN to set in the ticket
   -impersonate IMPERSONATE
                         target username that will be impersonated (thru
                         S4U2Self) for quering the ST. Keep in mind this will
                         only work if the identity provided in this scripts is
                         allowed for delegation to the SPN specified
   -additional-ticket ticket.ccache
                         include a forwardable service ticket in a S4U2Proxy
                         request for RBCD + KCD Kerberos only
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -u2u                  Request User-to-User ticket
   -self                 Only do S4U2self, no S4U2proxy
   -force-forwardable    Force the service ticket obtained through S4U2Self to
                         be forwardable. For best results, the -hashes and
                         -aesKey values for the specified -identity should be
                         provided. This allows impresonation of protected users
                         and bypass of "Kerberos-only" constrained delegation
                         restrictions. See CVE-2020-17049
   -renew                Sets the RENEW ticket option to renew the TGT used for
                         authentication. Set -spn to 'krbtgt/DOMAINFQDN'
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If omitted it use
                         the domain part (FQDN) specified in the target
                         parameter
 ```
 
 - - -
 
 ##### impacket-getTGT
 
 
 ```
 root@kali:~# impacket-getTGT -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: getTGT.py [-h] [-ts] [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                  [-aesKey hex key] [-dc-ip ip address] [-service SPN]
                  [-principalType [PRINCIPALTYPE]]
                  identity
 
 Given a password, hash or aesKey, it will request a TGT and save it as ccache
 
 positional arguments:
   identity              [domain/]username[:password]
 
 options:
   -h, --help            show this help message and exit
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -service SPN          Request a Service Ticket directly through an AS-REQ
   -principalType [PRINCIPALTYPE]
                         PrincipalType of the token, can be one of NT_UNKNOWN,
                         NT_PRINCIPAL, NT_SRV_INST, NT_SRV_HST, NT_SRV_XHST,
                         NT_UID, NT_SMTP_NAME, NT_ENTERPRISE, NT_WELLKNOWN,
                         NT_SRV_HST_DOMAIN, NT_MS_PRINCIPAL,
                         NT_MS_PRINCIPAL_AND_ID, NT_ENT_PRINCIPAL_AND_ID;
                         default is NT_PRINCIPAL,
 ```
 
 - - -
 
 ##### impacket-goldenPac
 
 
 ```
 root@kali:~# impacket-goldenPac -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: goldenPac.py [-h] [-ts] [-debug] [-c pathname] [-w pathname]
                     [-dc-ip ip address] [-target-ip ip address]
                     [-hashes LMHASH:NTHASH]
                     target [command ...]
 
 MS14-068 Exploit. It establishes a SMBConnection and PSEXEcs the target or
 saves the TGT for later use.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName>
   command               command (or arguments if -c is used) to execute at the
                         target (w/o path). Defaults to cmd.exe. 'None' will
                         not execute PSEXEC (handy if you just want to save the
                         ticket)
 
 options:
   -h, --help            show this help message and exit
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -c pathname           uploads the filename for later execution, arguments
                         are passed in the command option
   -w pathname           writes the golden ticket in CCache format into the
                         <pathname> file
   -dc-ip ip address     IP Address of the domain controller (needed to get the
                         users SID). If omitted it will use the domain part
                         (FQDN) specified in the target parameter
   -target-ip ip address
                         IP Address of the target host you want to attack. If
                         omitted it will use the targetName parameter
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
 ```
 
 - - -
 
 ##### impacket-karmaSMB
 
 
 ```
 root@kali:~# impacket-karmaSMB --help
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: karmaSMB.py [--help] [-config pathname] [-smb2support] pathname
 
 For every file request received, this module will return the pathname contents
 
 positional arguments:
   pathname          Pathname's contents to deliver to SMB clients
 
 options:
   --help            show this help message and exit
   -config pathname  config file name to map extensions to files to deliver.
                     For those extensions not present, pathname will be
                     delivered
   -smb2support      SMB2 Support (experimental!)
 ```
 
 - - -
 
 ##### impacket-keylistattack
 
 
 ```
 root@kali:~# impacket-keylistattack -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: keylistattack.py [-h] [-rodcNo RODCNO] [-rodcKey RODCKEY] [-full]
                         [-debug] [-domain DOMAIN] [-kdc KDC] [-t T] [-tf TF]
                         [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                         [-aesKey hex key] [-dc-ip ip address]
                         [-target-ip ip address]
                         target
 
 Performs the KERB-KEY-LIST-REQ attack to dump secrets from the remote machine
 without executing any agent there.
 
 positional arguments:
   target                [[domain/]username[:password]@]<KDC HostName or IP
                         address> (Use this credential to authenticate to SMB
                         and list domain users (low-privilege account) or LIST
                         (if you want to parse a target file)
 
 options:
   -h, --help            show this help message and exit
   -rodcNo RODCNO        Number of the RODC krbtgt account
   -rodcKey RODCKEY      AES key of the Read Only Domain Controller
   -full                 Run the attack against all domain users. Noisy! It
                         could lead to more TGS requests being rejected
   -debug                Turn DEBUG output ON
 
 LIST option:
   -domain DOMAIN        The fully qualified domain name (only works with LIST)
   -kdc KDC              KDC HostName or FQDN (only works with LIST)
   -t T                  Attack only the username specified (only works with
                         LIST)
   -tf TF                File that contains a list of target usernames (only
                         works with LIST)
 
 authentication:
   -hashes LMHASH:NTHASH
                         Use NTLM hashes to authenticate to SMB and list domain
                         users.
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos to authenticate to SMB and list domain
                         users. Grabs credentials from ccache file (KRB5CCNAME)
                         based on target parameters. If valid credentials
                         cannot be found, it will use the ones specified in the
                         command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
 ```
 
 - - -
 
 ##### impacket-lookupsid
 
 
 ```
 root@kali:~# impacket-lookupsid -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: lookupsid.py [-h] [-ts] [-target-ip ip address]
                     [-port [destination port]] [-domain-sids]
                     [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                     target [maxRid]
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   maxRid                max Rid to check (default 4000)
 
 options:
   -h, --help            show this help message and exit
   -ts                   Adds timestamp to every logging output
 
 connection:
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
   -domain-sids          Enumerate Domain SIDs (will likely forward requests to
                         the DC)
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful when proxying through
                         smbrelayx)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
 ```
 
 - - -
 
 ##### impacket-machine_role
 
 
 ```
 root@kali:~# impacket-machine_role -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: machine_role.py [-h] [-ts] [-debug] [-dc-ip ip address]
                        [-target-ip ip address] [-port [destination port]]
                        [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                        [-aesKey hex key]
                        target
 
 Retrieve a host's role along with its primary domain details.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -target-ip ip address
                         IP Address of the target machine. If ommited it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 ```
 
 - - -
 
 ##### impacket-mimikatz
 
 
 ```
 root@kali:~# impacket-mimikatz -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: mimikatz.py [-h] [-file FILE] [-debug] [-hashes LMHASH:NTHASH]
                    [-no-pass] [-k] [-aesKey hex key] [-dc-ip ip address]
                    [-target-ip ip address]
                    target
 
 SMB client implementation.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -file FILE            input file with commands to execute in the mini shell
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
 ```
 
 - - -
 
 ##### impacket-mqtt_check
 
 
 ```
 root@kali:~# impacket-mqtt_check --help
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: mqtt_check.py [--help] [-client-id CLIENT_ID] [-ssl] [-port PORT]
                      [-debug]
                      target
 
 MQTT login check
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName>
 
 options:
   --help                show this help message and exit
   -client-id CLIENT_ID  Client ID used when authenticating (default random)
   -ssl                  turn SSL on
   -port PORT            port to connect to (default 1883)
   -debug                Turn DEBUG output ON
 ```
 
 - - -
 
 ##### impacket-mssqlclient
 
 
 ```
 root@kali:~# impacket-mssqlclient -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: mssqlclient.py [-h] [-db DB] [-windows-auth] [-debug] [-show]
                       [-file FILE] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                       [-aesKey hex key] [-dc-ip ip address]
                       [-target-ip ip address] [-port PORT]
                       target
 
 TDS client implementation (SSL supported).
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -db DB                MSSQL database instance (default None)
   -windows-auth         whether or not to use Windows Authentication (default
                         False)
   -debug                Turn DEBUG output ON
   -show                 show the queries
   -file FILE            input file with commands to execute in the SQL shell
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port PORT            target MSSQL port (default 1433)
 ```
 
 - - -
 
 ##### impacket-mssqlinstance
 
 
 ```
 root@kali:~# impacket-mssqlinstance -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: mssqlinstance.py [-h] [-timeout TIMEOUT] host
 
 Asks the remote host for its running MSSQL Instances.
 
 positional arguments:
   host              target host
 
 options:
   -h, --help        show this help message and exit
   -timeout TIMEOUT  timeout to wait for an answer
 ```
 
 - - -
 
 ##### impacket-net
 
 
 ```
 root@kali:~# impacket-net -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: net.py [-h] [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
               [-aesKey hex key] [-dc-ip ip address] [-target-ip ip address]
               [-port [destination port]]
               target {user,computer,localgroup,group} ...
 
 SAMR rpc client implementation.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   {user,computer,localgroup,group}
                         An account entry name
     user                Enumerate all domain/local user accounts
     computer            Enumerate all computers in domain level
     localgroup          Enumerate local groups (aliases) of local computer
     group               Enumerate domain groups registered in domain
                         controller
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-ntfs-read
 
 
 ```
 root@kali:~# impacket-ntfs-read -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: ntfs-read.py [-h] [-extract EXTRACT] [-debug] volume
 
 NTFS explorer (read-only)
 
 positional arguments:
   volume            NTFS volume to open (e.g. \\.\C: or /dev/disk1s1)
 
 options:
   -h, --help        show this help message and exit
   -extract EXTRACT  extracts pathname (e.g. \windows\system32\config\sam)
   -debug            Turn DEBUG output ON
 ```
 
 - - -
 
 ##### impacket-ntlmrelayx
 
 
 ```
 root@kali:~# impacket-ntlmrelayx -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: ntlmrelayx.py [-h] [-ts] [-debug] [-t TARGET] [-tf TARGETSFILE] [-w]
                      [-i] [-ip INTERFACE_IP] [--no-smb-server]
                      [--no-http-server] [--no-wcf-server] [--no-raw-server]
                      [--smb-port SMB_PORT] [--http-port HTTP_PORT]
                      [--wcf-port WCF_PORT] [--raw-port RAW_PORT]
                      [--no-multirelay] [--keep-relaying] [-ra] [-r SMBSERVER]
                      [-l LOOTDIR] [-of OUTPUT_FILE] [-codec CODEC]
                      [-smb2support] [-ntlmchallenge NTLMCHALLENGE] [-socks]
                      [-socks-address SOCKS_ADDRESS] [-socks-port SOCKS_PORT]
                      [-http-api-port HTTP_API_PORT] [-wh WPAD_HOST]
                      [-wa WPAD_AUTH_NUM] [-6] [--remove-mic]
                      [--serve-image SERVE_IMAGE] [-c COMMAND] [-e FILE]
                      [--enum-local-admins] [-rpc-mode {TSCH}] [-rpc-use-smb]
                      [-auth-smb [domain/]username[:password]]
                      [-hashes-smb LMHASH:NTHASH] [-rpc-smb-port {139,445}]
                      [-q QUERY] [-machine-account MACHINE_ACCOUNT]
                      [-machine-hashes LMHASH:NTHASH] [-domain DOMAIN]
                      [-remove-target] [--no-dump] [--no-da] [--no-acl]
                      [--no-validate-privs] [--escalate-user ESCALATE_USER]
                      [--delegate-access] [--sid] [--dump-laps] [--dump-gmsa]
                      [--dump-adcs] [--add-dns-record NAME IPADDR]
                      [--add-computer [COMPUTERNAME [PASSWORD ...]]]
                      [-k KEYWORD] [-m MAILBOX] [-a] [-im IMAP_MAX] [--adcs]
                      [--template TEMPLATE] [--altname ALTNAME]
                      [--shadow-credentials] [--shadow-target SHADOW_TARGET]
                      [--pfx-password PFX_PASSWORD] [--export-type {PEM,PFX}]
                      [--cert-outfile-path CERT_OUTFILE_PATH]
 
 For every connection received, this module will try to relay that connection
 to specified target(s) system or the original client
 
 Main options:
   -h, --help            show this help message and exit
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -t, --target TARGET   Target to relay the credentials to, can be an IP,
                         hostname or URL like domain\username@host:port
                         (domain\username and port are optional, and don't
                         forget to escape the '\'). If unspecified, it will
                         relay back to the client')
   -tf TARGETSFILE       File that contains targets by hostname or full URL,
                         one per line
   -w                    Watch the target file for changes and update target
                         list automatically (only valid with -tf)
   -i, --interactive     Launch an smbclient, LDAP console or SQL shell
                         insteadof executing a command after a successful
                         relay. This console will listen locally on a tcp port
                         and can be reached with for example netcat.
   -ip, --interface-ip INTERFACE_IP
                         IP address of interface to bind SMB and HTTP servers
   --smb-port SMB_PORT   Port to listen on smb server
   --http-port HTTP_PORT
                         Port(s) to listen on HTTP server. Can specify multiple
                         ports by separating them with `,`, and ranges with
                         `-`. Ex: `80,8000-8010`
   --wcf-port WCF_PORT   Port to listen on wcf server
   --raw-port RAW_PORT   Port to listen on raw server
   --no-multirelay       If set, disable multi-host relay (SMB and HTTP
                         servers)
   --keep-relaying       If set, keeps relaying to a target even after a
                         successful connection on it
   -ra, --random         Randomize target selection
   -r SMBSERVER          Redirect HTTP requests to a file:// path on SMBSERVER
   -l, --lootdir LOOTDIR
                         Loot directory in which gathered loot such as SAM
                         dumps will be stored (default: current directory).
   -of, --output-file OUTPUT_FILE
                         base output filename for encrypted hashes. Suffixes
                         will be added for ntlm and ntlmv2
   -codec CODEC          Sets encoding used (codec) from the target's output
                         (default "utf-8"). If errors are detected, run
                         chcp.com at the target, map the result with https://do
                         cs.python.org/3/library/codecs.html#standard-encodings
                         and then execute ntlmrelayx.py again with -codec and
                         the corresponding codec
   -smb2support          SMB2 Support
   -ntlmchallenge NTLMCHALLENGE
                         Specifies the NTLM server challenge used by the SMB
                         Server (16 hex bytes long. eg: 1122334455667788)
   -socks                Launch a SOCKS proxy for the connection relayed
   -socks-address SOCKS_ADDRESS
                         SOCKS5 server address (also used for HTTP API)
   -socks-port SOCKS_PORT
                         SOCKS5 server port
   -http-api-port HTTP_API_PORT
                         SOCKS5 HTTP API port
   -wh, --wpad-host WPAD_HOST
                         Enable serving a WPAD file for Proxy Authentication
                         attack, setting the proxy host to the one supplied.
   -wa, --wpad-auth-num WPAD_AUTH_NUM
                         Prompt for authentication N times for clients without
                         MS16-077 installed before serving a WPAD file.
                         (default=1)
   -6, --ipv6            Listen on both IPv6 and IPv4
   --remove-mic          Remove MIC (exploit CVE-2019-1040)
   --serve-image SERVE_IMAGE
                         local path of the image that will we returned to
                         clients
   -c COMMAND            Command to execute on target system (for SMB and RPC).
                         If not specified for SMB, hashes will be dumped
                         (secretsdump.py must be in the same directory). For
                         RPC no output will be provided.
 
   --no-smb-server       Disables the SMB server
   --no-http-server      Disables the HTTP server
   --no-wcf-server       Disables the WCF server
   --no-raw-server       Disables the RAW server
 
 SMB client options:
   -e FILE               File to execute on the target system. If not
                         specified, hashes will be dumped (secretsdump.py must
                         be in the same directory)
   --enum-local-admins   If relayed user is not admin, attempt SAMR lookup to
                         see who is (only works pre Win 10 Anniversary)
 
 RPC client options:
   -rpc-mode {TSCH}      Protocol to attack, only TSCH supported
   -rpc-use-smb          Relay DCE/RPC to SMB pipes
   -auth-smb [domain/]username[:password]
                         Use this credential to authenticate to SMB (low-
                         privilege account)
   -hashes-smb LMHASH:NTHASH
   -rpc-smb-port {139,445}
                         Destination port to connect to SMB
 
 MSSQL client options:
   -q, --query QUERY     MSSQL query to execute(can specify multiple)
 
 HTTP options:
   -machine-account MACHINE_ACCOUNT
                         Domain machine account to use when interacting with
                         the domain to grab a session key for signing, format
                         is domain/machine_name
   -machine-hashes LMHASH:NTHASH
                         Domain machine hashes, format is LMHASH:NTHASH
   -domain DOMAIN        Domain FQDN or IP to connect using NETLOGON
   -remove-target        Try to remove the target in the challenge message (in
                         case CVE-2019-1019 patch is not installed)
 
 LDAP client options:
   --no-dump             Do not attempt to dump LDAP information
   --no-da               Do not attempt to add a Domain Admin
   --no-acl              Disable ACL attacks
   --no-validate-privs   Do not attempt to enumerate privileges, assume
                         permissions are granted to escalate a user via ACL
                         attacks
   --escalate-user ESCALATE_USER
                         Escalate privileges of this user instead of creating a
                         new one
   --delegate-access     Delegate access on relayed computer account to the
                         specified account
   --sid                 Use a SID to delegate access rather than an account
                         name
   --dump-laps           Attempt to dump any LAPS passwords readable by the
                         user
   --dump-gmsa           Attempt to dump any gMSA passwords readable by the
                         user
   --dump-adcs           Attempt to dump ADCS enrollment services and
                         certificate templates info
   --add-dns-record NAME IPADDR
                         Add the <NAME> record to DNS via LDAP pointing to
                         <IPADDR>
 
 Common options for SMB and LDAP:
   --add-computer [COMPUTERNAME [PASSWORD ...]]
                         Attempt to add a new computer account via SMB or LDAP,
                         depending on the specified target. This argument can
                         be used either with the LDAP or the SMB service, as
                         long as the target is a domain controller.
 
 IMAP client options:
   -k, --keyword KEYWORD
                         IMAP keyword to search for. If not specified, will
                         search for mails containing "password"
   -m, --mailbox MAILBOX
                         Mailbox name to dump. Default: INBOX
   -a, --all             Instead of searching for keywords, dump all emails
   -im, --imap-max IMAP_MAX
                         Max number of emails to dump (0 = unlimited, default:
                         no limit)
 
 AD CS attack options:
   --adcs                Enable AD CS relay attack
   --template TEMPLATE   AD CS template. Defaults to Machine or User whether
                         relayed account name ends with `$`. Relaying a DC
                         should require specifying `DomainController`
   --altname ALTNAME     Subject Alternative Name to use when performing ESC1
                         or ESC6 attacks.
 
 Shadow Credentials attack options:
   --shadow-credentials  Enable Shadow Credentials relay attack (msDS-
                         KeyCredentialLink manipulation for PKINIT pre-
                         authentication)
   --shadow-target SHADOW_TARGET
                         target account (user or computer$) to populate msDS-
                         KeyCredentialLink from
   --pfx-password PFX_PASSWORD
                         password for the PFX stored self-signed certificate
                         (will be random if not set, not needed when exporting
                         to PEM)
   --export-type {PEM,PFX}
                         choose to export cert+private key in PEM or PFX (i.e.
                         #PKCS12) (default: PFX))
   --cert-outfile-path CERT_OUTFILE_PATH
                         filename to store the generated self-signed PEM or PFX
                         certificate and key
 ```
 
 - - -
 
 ##### impacket-owneredit
 
 
 ```
 root@kali:~# impacket-owneredit -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: owneredit.py [-h] [-use-ldaps] [-ts] [-debug] [-hashes LMHASH:NTHASH]
                     [-no-pass] [-k] [-aesKey hex key] [-dc-ip ip address]
                     [-new-owner NAME] [-new-owner-sid SID] [-new-owner-dn DN]
                     [-target NAME] [-target-sid SID] [-target-dn DN]
                     [-action [{read,write}]]
                     identity
 
 Python editor for a principal's DACL.
 
 positional arguments:
   identity              domain.local/username[:password]
 
 options:
   -h, --help            show this help message and exit
   -use-ldaps            Use LDAPS instead of LDAP
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication & connection:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller or KDC (Key
                         Distribution Center) for Kerberos. If omitted it will
                         use the domain part (FQDN) specified in the identity
                         parameter
 
 owner:
   Object, controlled by the attacker, to set as owner of the target object
 
   -new-owner NAME       sAMAccountName
   -new-owner-sid SID    Security IDentifier
   -new-owner-dn DN      Distinguished Name
 
 target:
   Target object to edit the owner of
 
   -target NAME          sAMAccountName
   -target-sid SID       Security IDentifier
   -target-dn DN         Distinguished Name
 
 dacl editor:
   -action [{read,write}]
                         Action to operate on the owner attribute
 ```
 
 - - -
 
 ##### impacket-ping
 
 
 ```
 root@kali:~# impacket-ping -h
 Use: /usr/share/doc/python3-impacket/examples/ping.py <src ip> <dst ip>
 ```
 
 - - -
 
 ##### impacket-ping6
 
 
 ```
 root@kali:~# impacket-ping6 -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 Use: /usr/share/doc/python3-impacket/examples/ping6.py <src ip> <dst ip>
 ```
 
 - - -
 
 ##### impacket-psexec
 
 
 ```
 root@kali:~# impacket-psexec -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: psexec.py [-h] [-c pathname] [-path PATH] [-file FILE] [-ts] [-debug]
                  [-codec CODEC] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                  [-aesKey hex key] [-keytab KEYTAB] [-dc-ip ip address]
                  [-target-ip ip address] [-port [destination port]]
                  [-service-name service_name]
                  [-remote-binary-name remote_binary_name]
                  target [command ...]
 
 PSEXEC like functionality example using RemComSvc.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   command               command (or arguments if -c is used) to execute at the
                         target (w/o path) - (default:cmd.exe)
 
 options:
   -h, --help            show this help message and exit
   -c pathname           copy the filename for later execution, arguments are
                         passed in the command option
   -path PATH            path of the command to execute
   -file FILE            alternative RemCom binary (be sure it doesn't require
                         CRT)
   -ts                   adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -codec CODEC          Sets encoding used (codec) from the target's output
                         (default "utf-8"). If errors are detected, run
                         chcp.com at the target, map the result with https://do
                         cs.python.org/3/library/codecs.html#standard-encodings
                         and then execute smbexec.py again with -codec and the
                         corresponding codec
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -keytab KEYTAB        Read keys for SPN from keytab file
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
   -service-name service_name
                         The name of the service used to trigger the payload
   -remote-binary-name remote_binary_name
                         This will be the name of the executable uploaded on
                         the target
 ```
 
 - - -
 
 ##### impacket-raiseChild
 
 
 ```
 root@kali:~# impacket-raiseChild -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: raiseChild.py [-h] [-ts] [-debug] [-w pathname]
                      [-target-exec target address] [-targetRID RID]
                      [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                      target
 
 Privilege Escalation from a child domain up to its forest
 
 positional arguments:
   target                domain/username[:password]
 
 options:
   -h, --help            show this help message and exit
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -w pathname           writes the golden ticket in CCache format into the
                         <pathname> file
   -target-exec target address
                         Target host you want to PSEXEC against once the main
                         attack finished
   -targetRID RID        Target user RID you want to dump credentials.
                         Administrator (500) by default.
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 ```
 
 - - -
 
 ##### impacket-rbcd
 
 
 ```
 root@kali:~# impacket-rbcd -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: rbcd.py [-h] -delegate-to DELEGATE_TO [-delegate-from DELEGATE_FROM]
                [-action [{read,write,remove,flush}]] [-use-ldaps] [-ts]
                [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                [-aesKey hex key] [-dc-ip ip address]
                identity
 
 Python (re)setter for property msDS-AllowedToActOnBehalfOfOtherIdentity for
 Kerberos RBCD attacks.
 
 positional arguments:
   identity              domain.local/username[:password]
 
 options:
   -h, --help            show this help message and exit
   -delegate-to DELEGATE_TO
                         Target account the DACL is to be read/edited/etc.
   -delegate-from DELEGATE_FROM
                         Attacker controlled account to write on the rbcd
                         property of -delegate-to (only when using `-action
                         write`)
   -action [{read,write,remove,flush}]
                         Action to operate on msDS-
                         AllowedToActOnBehalfOfOtherIdentity
   -use-ldaps            Use LDAPS instead of LDAP
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller or KDC (Key
                         Distribution Center) for Kerberos. If omitted it will
                         use the domain part (FQDN) specified in the identity
                         parameter
 ```
 
 - - -
 
 ##### impacket-rdp_check
 
 
 ```
 root@kali:~# impacket-rdp_check -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: rdp_check.py [-h] [-hashes LMHASH:NTHASH] target
 
 Test whether an account is valid on the target host using the RDP protocol.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
 ```
 
 - - -
 
 ##### impacket-reg
 
 
 ```
 root@kali:~# impacket-reg -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: reg.py [-h] [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
               [-aesKey hex key] [-dc-ip ip address] [-target-ip ip address]
               [-port [destination port]]
               target {query,add,delete,save,backup} ...
 
 Windows Register manipulation script.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   {query,add,delete,save,backup}
                         actions
     query               Returns a list of the next tier of subkeys and entries
                         that are located under a specified subkey in the
                         registry.
     add                 Adds a new subkey or entry to the registry
     delete              Deletes a subkey or entries from the registry
     save                Saves a copy of specified subkeys, entries, and values
                         of the registry in a specified file.
     backup              (special command) Backs up HKLM\SAM, HKLM\SYSTEM and
                         HKLM\SECURITY to a specified file.
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-registry-read
 
 
 ```
 root@kali:~# impacket-registry-read -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: registry-read.py [-h]
                         hive
                         {enum_key,enum_values,get_value,get_class,walk} ...
 
 Reads data from registry hives.
 
 positional arguments:
   hive                  registry hive to open
   {enum_key,enum_values,get_value,get_class,walk}
                         actions
     enum_key            enumerates the subkeys of the specified open registry
                         key
     enum_values         enumerates the values for the specified open registry
                         key
     get_value           retrieves the data for the specified registry value
     get_class           retrieves the data for the specified registry class
     walk                walks the registry from the name node down
 
 options:
   -h, --help            show this help message and exit
 ```
 
 - - -
 
 ##### impacket-rpcmap
 
 
 ```
 root@kali:~# impacket-rpcmap -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: rpcmap.py [-h] [-brute-uuids] [-brute-opnums] [-brute-versions]
                  [-opnum-max OPNUM_MAX] [-version-max VERSION_MAX]
                  [-auth-level AUTH_LEVEL] [-uuid UUID] [-debug]
                  [-target-ip ip address] [-port [destination port]]
                  [-auth-rpc AUTH_RPC] [-auth-transport AUTH_TRANSPORT]
                  [-hashes-rpc LMHASH:NTHASH] [-hashes-transport LMHASH:NTHASH]
                  [-no-pass]
                  stringbinding
 
 Lookups listening MSRPC interfaces.
 
 positional arguments:
   stringbinding         String binding to connect to MSRPC interface, for example:
                         ncacn_ip_tcp:192.168.0.1[135]
                         ncacn_np:192.168.0.1[\pipe\spoolss]
                         ncacn_http:192.168.0.1[593]
                         ncacn_http:[6001,RpcProxy=exchange.contoso.com:443]
                         ncacn_http:localhost[3388,RpcProxy=rds.contoso:443]
 
 options:
   -h, --help            show this help message and exit
   -brute-uuids          Bruteforce UUIDs even if MGMT interface is available
   -brute-opnums         Bruteforce opnums for found UUIDs
   -brute-versions       Bruteforce major versions of found UUIDs
   -opnum-max OPNUM_MAX  Bruteforce opnums from 0 to N, default 64
   -version-max VERSION_MAX
                         Bruteforce versions from 0 to N, default 64
   -auth-level AUTH_LEVEL
                         MS-RPCE auth level, from 1 to 6, default 6
                         (RPC_C_AUTHN_LEVEL_PKT_PRIVACY)
   -uuid UUID            Test only this UUID
   -debug                Turn DEBUG output ON
 
 ncacn-np-details:
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 
 authentication:
   -auth-rpc AUTH_RPC    [domain/]username[:password]
   -auth-transport AUTH_TRANSPORT
                         [domain/]username[:password]
   -hashes-rpc LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -hashes-transport LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for passwords
 ```
 
 - - -
 
 ##### impacket-sambaPipe
 
 
 ```
 root@kali:~# impacket-sambaPipe -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: sambaPipe.py [-h] -so SO [-debug] [-hashes LMHASH:NTHASH] [-no-pass]
                     [-k] [-aesKey hex key] [-dc-ip ip address]
                     [-target-ip ip address] [-port [destination port]]
                     target
 
 Samba Pipe exploit
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -so SO                so filename to upload and load
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-services
 
 
 ```
 root@kali:~# impacket-services -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: services.py [-h] [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                    [-aesKey hex key] [-dc-ip ip address]
                    [-target-ip ip address] [-port [destination port]]
                    target
                    {start,stop,delete,status,config,list,create,change} ...
 
 Windows Service manipulation script.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   {start,stop,delete,status,config,list,create,change}
                         actions
     start               starts the service
     stop                stops the service
     delete              deletes the service
     status              returns service status
     config              returns service configuration
     list                list available services
     create              create a service
     change              change a service configuration
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -target-ip ip address
                         IP Address of the target machine. If ommited it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-smbclient
 
 
 ```
 root@kali:~# impacket-smbclient -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: smbclient.py [-h] [-inputfile INPUTFILE] [-outputfile OUTPUTFILE]
                     [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                     [-aesKey hex key] [-dc-ip ip address]
                     [-target-ip ip address] [-port [destination port]]
                     target
 
 SMB client implementation.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -inputfile INPUTFILE  input file with commands to execute in the mini shell
   -outputfile OUTPUTFILE
                         Output file to log smbclient actions in
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-smbexec
 
 
 ```
 root@kali:~# impacket-smbexec -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: smbexec.py [-h] [-share SHARE] [-mode {SHARE,SERVER}] [-ts] [-debug]
                   [-codec CODEC] [-shell-type {cmd,powershell}]
                   [-dc-ip ip address] [-target-ip ip address]
                   [-port [destination port]] [-service-name service_name]
                   [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                   [-keytab KEYTAB]
                   target
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -share SHARE          share where the output will be grabbed from (default
                         C$)
   -mode {SHARE,SERVER}  mode to use (default SHARE, SERVER needs root!)
   -ts                   adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -codec CODEC          Sets encoding used (codec) from the target's output
                         (default "utf-8"). If errors are detected, run
                         chcp.com at the target, map the result with https://do
                         cs.python.org/3/library/codecs.html#standard-encodings
                         and then execute smbexec.py again with -codec and the
                         corresponding codec
   -shell-type {cmd,powershell}
                         choose a command processor for the semi-interactive
                         shell
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If ommited it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
   -service-name service_name
                         The name of theservice used to trigger the payload
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -keytab KEYTAB        Read keys for SPN from keytab file
 ```
 
 - - -
 
 ##### impacket-smbserver
 
 
 ```
 root@kali:~# impacket-smbserver -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: smbserver.py [-h] [-comment COMMENT] [-username USERNAME]
                     [-password PASSWORD] [-hashes LMHASH:NTHASH] [-ts]
                     [-debug] [-ip INTERFACE_ADDRESS] [-port PORT]
                     [-smb2support] [-outputfile OUTPUTFILE]
                     shareName sharePath
 
 This script will launch a SMB Server and add a share specified as an argument.
 You need to be root in order to bind to port 445. For optional authentication,
 it is possible to specify username and password or the NTLM hash. Example:
 smbserver.py -comment 'My share' TMP /tmp
 
 positional arguments:
   shareName             name of the share to add
   sharePath             path of the share to add
 
 options:
   -h, --help            show this help message and exit
   -comment COMMENT      share's comment to display when asked for shares
   -username USERNAME    Username to authenticate clients
   -password PASSWORD    Password for the Username
   -hashes LMHASH:NTHASH
                         NTLM hashes for the Username, format is LMHASH:NTHASH
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -ip, --interface-address INTERFACE_ADDRESS
                         ip address of listening interface
   -port PORT            TCP port for listening incoming connections (default
                         445)
   -smb2support          SMB2 Support (experimental!)
   -outputfile OUTPUTFILE
                         Output file to log smbserver output messages
 ```
 
 - - -
 
 ##### impacket-sniff
 
 
 ```
 root@kali:~# impacket-sniff -h
 0 - eth0
 1 - any
 2 - lo
 3 - docker0
 4 - bluetooth-monitor
 5 - nflog
 6 - nfqueue
 7 - dbus-system
 8 - dbus-session
 Please select an interface: 
 ```
 
 - - -
 
 ##### impacket-sniffer
 
 
 ```
 root@kali:~# impacket-sniffer -h
 Ignoring unknown protocol: -h
 There are no protocols available.
 ```
 
 - - -
 
 ##### impacket-split
 
 
 ```
 root@kali:~# impacket-split -h
 Traceback (most recent call last):
   File "/usr/share/doc/python3-impacket/examples/split.py", line 147, in <module>
     main(sys.argv[1])
     ~~~~^^^^^^^^^^^^^
   File "/usr/share/doc/python3-impacket/examples/split.py", line 130, in main
     p = open_offline(filename)
 pcapy.PcapError: -h: No such file or directory
 ```
 
 - - -
 
 ##### impacket-ticketConverter
 
 
 ```
 root@kali:~# impacket-ticketConverter -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: ticketConverter.py [-h] input_file output_file
 
 positional arguments:
   input_file   File in kirbi (KRB-CRED) or ccache format
   output_file  Output file
 
 options:
   -h, --help   show this help message and exit
 ```
 
 - - -
 
 ##### impacket-ticketer
 
 
 ```
 root@kali:~# impacket-ticketer -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: ticketer.py [-h] [-spn SPN] [-request] -domain DOMAIN
                    -domain-sid DOMAIN_SID [-aesKey hex key] [-nthash NTHASH]
                    [-keytab KEYTAB] [-groups GROUPS] [-user-id USER_ID]
                    [-extra-sid EXTRA_SID] [-extra-pac] [-old-pac]
                    [-duration DURATION] [-ts] [-debug] [-user USER]
                    [-password PASSWORD] [-hashes LMHASH:NTHASH]
                    [-dc-ip ip address] [-impersonate IMPERSONATE]
                    target
 
 Creates a Kerberos golden/silver tickets based on user options
 
 positional arguments:
   target                username for the newly created ticket
 
 options:
   -h, --help            show this help message and exit
   -spn SPN              SPN (service/server) of the target service the silver
                         ticket will be generated for. if omitted, golden
                         ticket will be created
   -request              Requests ticket to domain and clones it changing only
                         the supplied information. It requires specifying -user
   -domain DOMAIN        the fully qualified domain name (e.g. contoso.com)
   -domain-sid DOMAIN_SID
                         Domain SID of the target domain the ticker will be
                         generated for
   -aesKey hex key       AES key used for signing the ticket (128 or 256 bits)
   -nthash NTHASH        NT hash used for signing the ticket
   -keytab KEYTAB        Read keys for SPN from keytab file (silver ticket
                         only)
   -groups GROUPS        comma separated list of groups user will belong to
                         (default = 513, 512, 520, 518, 519)
   -user-id USER_ID      user id for the user the ticket will be created for
                         (default = 500)
   -extra-sid EXTRA_SID  Comma separated list of ExtraSids to be included
                         inside the ticket's PAC
   -extra-pac            Populate your ticket with extra PAC (UPN_DNS)
   -old-pac              Use the old PAC structure to create your ticket
                         (exclude PAC_ATTRIBUTES_INFO and PAC_REQUESTOR
   -duration DURATION    Amount of hours till the ticket expires (default =
                         24*365*10)
   -ts                   Adds timestamp to every logging output
   -debug                Turn DEBUG output ON
   -impersonate IMPERSONATE
                         Sapphire ticket. target username that will be
                         impersonated (through S4U2Self+U2U) for querying the
                         ST and extracting the PAC, which will be included in
                         the new ticket
 
 authentication:
   -user USER            domain/username to be used if -request is chosen (it
                         can be different from domain/username
   -password PASSWORD    password for domain/username
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
 ```
 
 - - -
 
 ##### impacket-tstool
 
 
 ```
 root@kali:~# impacket-tstool -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: tstool.py [-h] [-debug] [-hashes LMHASH:NTHASH] [-no-pass] [-k]
                  [-aesKey hex key] [-dc-ip ip address] [-target-ip ip address]
                  [-port [destination port]]
                  target
                  {qwinsta,tasklist,taskkill,tscon,tsdiscon,logoff,shutdown,msg} ...
 
 Terminal Services manipulation tool.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
   {qwinsta,tasklist,taskkill,tscon,tsdiscon,logoff,shutdown,msg}
                         actions
     qwinsta             Display information about Remote Desktop Services
                         sessions.
     tasklist            Display a list of currently running processes on the
                         system.
     taskkill            Terminate tasks by process id (PID) or image name.
     tscon               Attaches a user session to a remote desktop session.
     tsdiscon            Disconnects a Remote Desktop Services session.
     logoff              Sign out a Remote Desktop Services session.
     shutdown            Remote shutdown, affects ALL sessions and logged-in
                         users!
     msg                 Send a message to Remote Desktop Services session
                         (MSGBOX).
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
 
 connection:
   -dc-ip ip address     IP Address of the domain controller. If omitted it
                         will use the domain part (FQDN) specified in the
                         target parameter
   -target-ip ip address
                         IP Address of the target machine. If omitted it will
                         use whatever was specified as target. This is useful
                         when target is the NetBIOS name and you cannot resolve
                         it
   -port [destination port]
                         Destination port to connect to SMB Server
 ```
 
 - - -
 
 ##### impacket-wmipersist
 
 
 ```
 root@kali:~# impacket-wmipersist -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: wmipersist.py [-h] [-debug] [-com-version MAJOR_VERSION:MINOR_VERSION]
                      [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                      [-dc-ip ip address]
                      target {install,remove} ...
 
 Creates/Removes a WMI Event Consumer/Filter and link between both to execute
 Visual Basic based on the WQL filter or timer specified.
 
 positional arguments:
   target                [domain/][username[:password]@]<address>
   {install,remove}      actions
     install             installs the wmi event consumer/filter
     remove              removes the wmi event consumer/filter
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
   -com-version MAJOR_VERSION:MINOR_VERSION
                         DCOM version, format is MAJOR_VERSION:MINOR_VERSION
                         e.g. 5.7
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
 ```
 
 - - -
 
 ##### impacket-wmiquery
 
 
 ```
 root@kali:~# impacket-wmiquery -h
 Impacket v0.12.0 - Copyright Fortra, LLC and its affiliated companies 
 
 usage: wmiquery.py [-h] [-namespace NAMESPACE] [-file FILE] [-debug]
                    [-com-version MAJOR_VERSION:MINOR_VERSION]
                    [-hashes LMHASH:NTHASH] [-no-pass] [-k] [-aesKey hex key]
                    [-dc-ip ip address]
                    [-rpc-auth-level [{integrity,privacy,default}]]
                    target
 
 Executes WQL queries and gets object descriptions using Windows Management
 Instrumentation.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address>
 
 options:
   -h, --help            show this help message and exit
   -namespace NAMESPACE  namespace name (default //./root/cimv2)
   -file FILE            input file with commands to execute in the WQL shell
   -debug                Turn DEBUG output ON
   -com-version MAJOR_VERSION:MINOR_VERSION
                         DCOM version, format is MAJOR_VERSION:MINOR_VERSION
                         e.g. 5.7
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
   -no-pass              don't ask for password (useful for -k)
   -k                    Use Kerberos authentication. Grabs credentials from
                         ccache file (KRB5CCNAME) based on target parameters.
                         If valid credentials cannot be found, it will use the
                         ones specified in the command line
   -aesKey hex key       AES key to use for Kerberos Authentication (128 or 256
                         bits)
   -dc-ip ip address     IP Address of the domain controller. If ommited it use
                         the domain part (FQDN) specified in the target
                         parameter
   -rpc-auth-level [{integrity,privacy,default}]
                         default, integrity (RPC_C_AUTHN_LEVEL_PKT_INTEGRITY)
                         or privacy (RPC_C_AUTHN_LEVEL_PKT_PRIVACY). For
                         example CIM path "root/MSCluster" would require
                         privacy level by default)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
