---
Title: graudit
Homepage: https://github.com/wireghoul/graudit
Repository: 
Architectures: all
Version: 3.7-1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### graudit
 
  This is a simple script and signature sets that allows you to find potential
  security flaws in source code using the GNU utility grep. It's comparable to
  other static analysis applications like RATS, SWAAT and flaw-finder while
  keeping the technical requirements to a minimum and being very flexible.
 
 **Installed size:** `105 KB`  
 **How to install:** `sudo apt install graudit`  
 
 ##### graudit
 
 Source code auditing tool
 (unknown subject)
 
 ```
 root@kali:~# graudit -h
 ===========================================================
                                       .___ __  __   
           _________________  __ __  __| _/|__|/  |_ 
          / ___\_` __ \__  \ |  |  \/ __ | | \\_  __\
         / /_/  >  | \// __ \|  |  / /_/ | |  ||  |  
         \___  /|__|  (____  /____/\____ | |__||__|  
        /_____/            \/           \/           
               grep rough audit - static analysis tool
                   v3.7 written by @Wireghoul
 =================================[justanotherhacker.com]===
 Usage: graudit [opts] /path/to/scan
 
 OPTIONS
   -d <dbname> database to use or /path/to/file.db (uses default if not specified)
   -A scan unwanted and difficult (ALL) files
   -x exclude these files (comma separated list: -x *.js,*.sql)
   -i case in-sensitive scan
   -c <num> number of lines of context to display, default is 2
 
   -B supress banner
   -L vim friendly lines
   -b colour blind friendly template
   -z supress colors
   -Z high contrast colors
   
   -l lists databases available
   -v prints version number
   -h prints this help screen
 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
