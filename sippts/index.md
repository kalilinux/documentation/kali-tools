---
Title: sippts
Homepage: https://github.com/Pepelux/sippts
Repository: https://gitlab.com/kalilinux/packages/sippts
Architectures: all
Version: 4.1.2-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sippts
 
  Sippts is a set of tools to audit VoIP servers and devices using SIP protocol.
  Sippts is programmed in Python and it allows pentesters to check the security
  of a VoIP server using SIP protocol.
 
 **Installed size:** `762 KB`  
 **How to install:** `sudo apt install sippts`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-ipy
 * python3-netifaces
 * python3-pyshark
 * python3-rel
 * python3-requests
 * python3-scapy
 * python3-websocket
 {{< /spoiler >}}
 
 ##### sippts
 
 
 ```
 root@kali:~# sippts --help
 usage: sippts [-h]
               {video,astami,scan,exten,rcrack,send,wssend,enumerate,leak,ping,invite,dump,dcrack,flood,sniff,spoof,pcapdump,rtpbleed,rtcpbleed,rtpbleedflood,rtpbleedinject} ...
 
 
                __ _
              .: .' '.
             /: /     \_
            ;: ;  ,-'/`:\
            |: | |  |() :|
            ;: ;  '-.\_:/
             \: \     /`
              ':_'._.'
                 ||
                /__\
     .---.     |====|
   .'   _,"-,__|::  |
  /    ((O)=;--.::  |
 ;      `|: |  |::  |
 |       |: |  |::  |
 |       |: |  |::  |                                                   SIPPTS version 4.1.2 (updated)
 |       |: |  |::  |                                                        CVE version 0.1 (updated)
 |      /:'__\ |::  |                                      https://github.com/Pepelux/sippts
 |     [______]|::  |                              by Pepelux - https://twitter.com/pepeluxx
 |      `----` |::  |__
 |         _.--|::  |  ''--._
 ;       .'  __|====|__      '.
  \    .'_.-'._ `""` _.'-._    '.
   '--'/`      `'' ''`     `\    '.__
       '._     SIPPTS     _.'
         # `""--......--""`
 
  -= SIPPTS is a set of tools for auditing VoIP systems based on the SIP protocol =- 
 
 Commands:
   {video,astami,scan,exten,rcrack,send,wssend,enumerate,leak,ping,invite,dump,dcrack,flood,sniff,spoof,pcapdump,rtpbleed,rtcpbleed,rtpbleedflood,rtpbleedinject}
     video                                         Animated help
     astami                                        Asterisk AMI pentest
     scan                                          Fast SIP scanner
     exten                                         Search SIP extensions of a
                                                   PBX
     rcrack                                        Remote password cracker
     send                                          Send a customized message
     wssend                                        Send a customized message
                                                   over WS
     enumerate                                     Enumerate methods of a SIP
                                                   server
     leak                                          Exploit SIP Digest Leak
                                                   vulnerability
     ping                                          SIP ping
     invite                                        Try to make calls through a
                                                   PBX
     dump                                          Dump SIP digest
                                                   authentications from a PCAP
                                                   file
     dcrack                                        SIP digest authentication
                                                   cracking
     flood                                         Flood a SIP server
     sniff                                         SIP network sniffing
     spoof                                         ARP Spoofing tool
     pcapdump                                      Extract data from a PCAP
                                                   file
     rtpbleed                                      Detect RTPBleed
                                                   vulnerability (send RTP
                                                   streams)
     rtcpbleed                                     Detect RTPBleed
                                                   vulnerability (send RTCP
                                                   streams)
     rtpbleedflood                                 Exploit RTPBleed
                                                   vulnerability (flood RTP)
     rtpbleedinject                                Exploit RTPBleed
                                                   vulnerability (inject WAV
                                                   file)
 
 Options:
   -h, --help                                      show this help message and
                                                   exit
 
 Command help:
   sippts <command> -h
 ```
 
 - - -
 
 ##### sippts-gui
 
 
 ```
 root@kali:~# sippts-gui --help
 
 
 
                __ _
              .: .' '.
             /: /     \_
            ;: ;  ,-'/`:\
            |: | |  |() :|
            ;: ;  '-.\_:/
             \: \     /`
              ':_'._.'
                 ||
                /__\
     .---.     |====|
   .'   _,"-,__|::  |
  /    ((O)=;--.::  |
 ;      `|: |  |::  |
 |       |: |  |::  |
 |       |: |  |::  |                                                   SIPPTS version 4.1.2 (updated)
 |       |: |  |::  |                                                        CVE version 0.1 (updated)
 |      /:'__\ |::  |                                      https://github.com/Pepelux/sippts
 |     [______]|::  |                              by Pepelux - https://twitter.com/pepeluxx
 |      `----` |::  |__
 |         _.--|::  |  ''--._
 ;       .'  __|====|__      '.
  \    .'_.-'._ `""` _.'-._    '.
   '--'/`      `'' ''`     `\    '.__
       '._     SIPPTS     _.'
         # `""--......--""`
 
 Welcome to SIPPTS! Type ? or help to list commands
 
 [!] Client interface: eth0
 [!] Client IP address: 10.0.2.15
 [!] Network mask: 255.255.255.0
 [!] Gateway: 10.0.2.2
 
 Type ? or help to list commands
 
 SIPPTS> 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
