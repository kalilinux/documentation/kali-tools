---
Title: dradis
Homepage: https://dradis.com/ce/
Repository: https://gitlab.com/kalilinux/packages/dradis
Architectures: amd64
Version: 4.15.0-0kali2
Metapackages: kali-linux-everything kali-linux-large kali-tools-reporting 
Icon: images/dradis-logo.svg
PackagesInfo: |
 ### dradis
 
  Dradis is a tool to help you simplify reporting and collaboration.
   
    - Spend more time testing and less time reporting
    - Ensure consistent quality across your assessments
    - Combine the output of your favorite scanners
   
   Dradis is an open-source project released in 2007 that has been refined for
   over a decade by security professionals around the world.
 
 **Installed size:** `197.01 MB`  
 **How to install:** `sudo apt install dradis`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * bundler
 * git
 * libc6 
 * libgcc-s1 
 * libpq5 
 * libruby3.3 
 * libsqlite3-0 
 * libssl3t64 
 * libstdc++6 
 * lsof
 * nodejs
 * pwgen
 * ruby 
 * ruby-mini-racer
 * zlib1g 
 {{< /spoiler >}}
 
 ##### dradis
 
 
 ```
 root@kali:~# dradis -h
 [i] Something is already using port: 3000/tcp
 COMMAND    PID   USER FD   TYPE DEVICE SIZE/OFF NODE NAME
 ruby3.3 122953 dradis 20u  IPv4 282993      0t0  TCP localhost:3000 (LISTEN)
 ruby3.3 122953 dradis 21u  IPv6 282994      0t0  TCP localhost:3000 (LISTEN)
 
 UID          PID    PPID  C STIME TTY      STAT   TIME CMD
 dradis    122953       1 45 14:37 ?        Ssl    0:02 puma 6.6.0 (tcp://localhost:3000) [dradis]
 
 [*] Please wait for the Dradis service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:3000
 
 ```
 
 - - -
 
 ##### dradis-stop
 
 
 ```
 root@kali:~# dradis-stop -h
 * dradis.service - Dradis web application
      Loaded: loaded (/usr/lib/systemd/system/dradis.service; disabled; preset: disabled)
      Active: inactive (dead)
 
 Mar 02 14:37:32 kali bundle[122953]: *          PID: 122953
 Mar 02 14:37:32 kali bundle[122953]: * Listening on http://127.0.0.1:3000
 Mar 02 14:37:32 kali bundle[122953]: * Listening on http://[::1]:3000
 Mar 02 14:37:32 kali bundle[122953]: Use Ctrl-C to stop
 Mar 02 14:37:44 kali systemd[1]: Stopping dradis.service - Dradis web application...
 Mar 02 14:37:44 kali bundle[122953]: - Gracefully stopping, waiting for requests to finish
 Mar 02 14:37:44 kali bundle[122953]: Exiting
 Mar 02 14:37:44 kali systemd[1]: dradis.service: Deactivated successfully.
 Mar 02 14:37:44 kali systemd[1]: Stopped dradis.service - Dradis web application.
 Mar 02 14:37:44 kali systemd[1]: dradis.service: Consumed 2.344s CPU time, 148.5M memory peak.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}


```console
service dradis start
```

## Screenshots

![Dradis login screen](images/dradis-01.png)
![Dradis dashboard](images/dradis-02.png)
![Dradis issue list](images/dradis-03.png)
![Dradis methodologies](images/dradis-04.png)
