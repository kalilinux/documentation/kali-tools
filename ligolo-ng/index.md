---
Title: ligolo-ng
Homepage: https://github.com/nicocha30/ligolo-ng
Repository: https://gitlab.com/kalilinux/packages/ligolo-ng
Architectures: amd64 arm64
Version: 0.7.5-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### ligolo-ng
 
  Ligolo-ng is a simple, lightweight and fast tool that allows pentesters to
  establish tunnels from a reverse TCP/TLS connection using a tun interface
  (without the need of SOCKS).
 
 **Installed size:** `19.28 MB`  
 **How to install:** `sudo apt install ligolo-ng`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### ligolo-agent
 
 
 ```
 root@kali:~# ligolo-agent -h
   -accept-fingerprint string
     	accept certificates matching the following SHA256 fingerprint (hex format)
   -bind string
     	bind to ip:port
   -connect string
     	connect to proxy (domain:port)
   -ignore-cert
     	ignore TLS certificate validation (dangerous), only for debug purposes
   -proxy string
     	proxy URL address (http://admin:secret@127.0.0.1:8080) or socks://admin:secret@127.0.0.1:8080
   -retry
     	auto-retry on error
   -ua string
     	HTTP User-Agent (default "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36")
   -v	enable verbose mode
   -version
     	show the current version
 ```
 
 - - -
 
 ##### ligolo-proxy
 
 
 ```
 root@kali:~# ligolo-proxy -h
   -allow-domains string
     	autocert authorised domains, if empty, allow all domains, multiple domains should be comma-separated.
   -autocert
     	automatically request letsencrypt certificates, requires port 80 to be accessible
   -certfile string
     	TLS server certificate (default "certs/cert.pem")
   -keyfile string
     	TLS server key (default "certs/key.pem")
   -laddr string
     	listening address (prefix with https:// for websocket) (default "0.0.0.0:11601")
   -selfcert
     	dynamically generate self-signed certificates
   -selfcert-domain string
     	The selfcert TLS domain to use (default "ligolo")
   -v	enable verbose mode
   -version
     	show the current version
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
