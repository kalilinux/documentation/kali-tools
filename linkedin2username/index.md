---
Title: linkedin2username
Homepage: https://github.com/initstring/linkedin2username
Repository: https://gitlab.com/kalilinux/packages/linkedin2username
Architectures: all
Version: 0.29-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### linkedin2username
 
  OSINT Tool: Generate username lists from companies on LinkedIn.
   
  This is a pure web-scraper, no API key required. You use your valid LinkedIn
  username and password to login, it will create several lists of possible
  username formats for all employees of a company you point it at.
 
 **Installed size:** `37 KB`  
 **How to install:** `sudo apt install linkedin2username`  
 
 {{< spoiler "Dependencies:" >}}
 * chromium-driver
 * python3
 * python3-requests
 * python3-selenium
 {{< /spoiler >}}
 
 ##### linkedin2username
 
 
 ```
 root@kali:~# linkedin2username -h
 
 
                             .__  .__________
                             |  | |__\_____  \ __ __
                             |  | |  |/  ____/|  |  \
                             |  |_|  /       \|  |  /
                             |____/__\_______ \____/
                                linkedin2username
 
                                    Spray away.
                               github.com/initstring
 
 
 
 
 
 usage: linkedin2username [-h] -c COMPANY [-n DOMAIN] [-d DEPTH] [-s SLEEP]
                          [-x PROXY] [-k KEYWORDS] [-g] [-o OUTPUT]
 
 OSINT tool to generate lists of probable usernames from a given company's
 LinkedIn page. This tool may break when LinkedIn changes their site. Please
 open issues on GitHub to report any inconsistencies, and they will be quickly
 fixed.
 
 options:
   -h, --help            show this help message and exit
   -c, --company COMPANY
                         Company name exactly as typed in the company linkedin
                         profile page URL.
   -n, --domain DOMAIN   Append a domain name to username output. [example: "-n
                         uber.com" would output jschmoe@uber.com]
   -d, --depth DEPTH     Search depth (how many loops of 50). If unset, will
                         try to grab them all.
   -s, --sleep SLEEP     Seconds to sleep between search loops. Defaults to 0.
   -x, --proxy PROXY     Proxy server to use. WARNING: WILL DISABLE SSL
                         VERIFICATION. [example: "-p https://localhost:8080"]
   -k, --keywords KEYWORDS
                         Filter results by a a list of command separated
                         keywords. Will do a separate loop for each keyword,
                         potentially bypassing the 1,000 record limit.
                         [example: "-k 'sales,human resources,information
                         technology']
   -g, --geoblast        Attempts to bypass the 1,000 record search limit by
                         running multiple searches split across geographic
                         regions.
   -o, --output OUTPUT   Output Directory, defaults to li2u-output
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
