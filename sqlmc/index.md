---
Title: sqlmc
Homepage: https://github.com/malvads/sqlmc
Repository: https://gitlab.com/kalilinux/packages/sqlmc
Architectures: all
Version: 1.1.0-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sqlmc
 
  SQLMC (SQL Injection Massive Checker) is a tool designed to scan a domain for
  SQL injection vulnerabilities. It crawls the given URL up to a specified
  depth, checks each link for SQL injection vulnerabilities, and reports its
  findings.
 
 **Installed size:** `65 KB`  
 **How to install:** `sudo apt install sqlmc`  
 
 {{< spoiler "Dependencies:" >}}
 * figlet
 * python3
 * python3-aiohttp 
 * python3-bs4 
 * python3-pyfiglet 
 * python3-tabulate 
 {{< /spoiler >}}
 
 ##### sqlmc
 
 
 ```
 root@kali:~# sqlmc -h
  ____   ___  _     __  __  ____ 
 / ___| / _ \| |   |  \/  |/ ___|
 \___ \| | | | |   | |\/| | |    
  ___) | |_| | |___| |  | | |___ 
 |____/ \__\_\_____|_|  |_|\____|
                                 
 
 Version: 1.1.0
 Author: Miguel Álvarez
 usage: sqlmc [-h] -u URL -d DEPTH [-o OUTPUT]
 
 A simple SQLi Massive Checker & Scanner
 
 options:
   -h, --help           show this help message and exit
   -u, --url URL        The URL to scan
   -d, --depth DEPTH    The depth to scan
   -o, --output OUTPUT  The output file
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
