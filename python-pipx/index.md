---
Title: python-pipx
Homepage: https://github.com/pypa/pipx
Repository: https://salsa.debian.org/python-team/packages/python-pipx
Architectures: all
Version: 1.7.1-1
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### pipx
 
  pipx allows you to...
   
   * Run the latest version of a CLI application from a package
     in a temporary virtual environment,
     leaving your system untouched after it finishes.
   * Install packages to isolated virtual environments,
     while globally exposing their CLI applications
     so you can run them from anywhere.
   * Easily list, upgrade, and uninstall packages
     that were installed with pipx.
   
  pipx runs with regular user permissions,
  never calling "sudo pip install".
 
 **Installed size:** `3.62 MB`  
 **How to install:** `sudo apt install pipx`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-argcomplete
 * python3-packaging
 * python3-platformdirs 
 * python3-tomli | python3-supported-min 
 * python3-userpath
 * python3-venv
 {{< /spoiler >}}
 
 ##### pipx
 
 Install and execute apps from Python packages.
 
 ```
 root@kali:~# pipx -h
 usage: pipx [-h] [--quiet] [--verbose] [--global] [--version]
             {install,install-all,uninject,inject,pin,unpin,upgrade,upgrade-all,upgrade-shared,uninstall,uninstall-all,reinstall,reinstall-all,list,interpreter,run,runpip,ensurepath,environment,completions} ...
 
 Install and execute apps from Python packages.
 
 Binaries can either be installed globally into isolated Virtual Environments
 or run directly in a temporary Virtual Environment.
 
 Virtual Environment location is /root/.local/share/pipx/venvs.
 Symlinks to apps are placed in /root/.local/bin.
 Symlinks to manual pages are placed in /root/.local/share/man.
 
 optional environment variables:
   PIPX_HOME              Overrides default pipx location. Virtual Environments
                         will be installed to $PIPX_HOME/venvs.
   PIPX_GLOBAL_HOME       Used instead of PIPX_HOME when the `--global` option
                         is given.
   PIPX_BIN_DIR           Overrides location of app installations. Apps are
                         symlinked or copied here.
   PIPX_GLOBAL_BIN_DIR    Used instead of PIPX_BIN_DIR when the `--global`
                         option is given.
   PIPX_MAN_DIR           Overrides location of manual pages installations.
                         Manual pages are symlinked or copied here.
   PIPX_GLOBAL_MAN_DIR    Used instead of PIPX_MAN_DIR when the `--global`
                         option is given.
   PIPX_DEFAULT_PYTHON    Overrides default python used for commands.
   USE_EMOJI              Overrides emoji behavior. Default value varies based
                         on platform.
   PIPX_HOME_ALLOW_SPACE  Overrides default warning on spaces in the home path
 
 options:
   -h, --help            show this help message and exit
   --quiet, -q           Give less output. May be used multiple times
                         corresponding to the ERROR and CRITICAL logging
                         levels. The count maxes out at 2.
   --verbose, -v         Give more output. May be used multiple times
                         corresponding to the INFO, DEBUG and NOTSET logging
                         levels. The count maxes out at 3.
   --global              Perform action globally for all users.
   --version             Print version and exit
 
 subcommands:
   Get help for commands with pipx COMMAND --help
 
   {install,install-all,uninject,inject,pin,unpin,upgrade,upgrade-all,upgrade-shared,uninstall,uninstall-all,reinstall,reinstall-all,list,interpreter,run,runpip,ensurepath,environment,completions}
     install             Install a package
     install-all         Install all packages
     uninject            Uninstall injected packages from an existing Virtual
                         Environment
     inject              Install packages into an existing Virtual Environment
     pin                 Pin the specified package to prevent it from being
                         upgraded
     unpin               Unpin the specified package
     upgrade             Upgrade a package
     upgrade-all         Upgrade all packages. Runs `pip install -U <pkgname>`
                         for each package.
     upgrade-shared      Upgrade shared libraries.
     uninstall           Uninstall a package
     uninstall-all       Uninstall all packages
     reinstall           Reinstall a package
     reinstall-all       Reinstall all packages
     list                List installed packages
     interpreter         Interact with interpreters managed by pipx
     run                 Download the latest version of a package to a
                         temporary virtual environment, then run an app from
                         it. Also compatible with local `__pypackages__`
                         directory (experimental).
     runpip              Run pip in an existing pipx-managed Virtual
                         Environment
     ensurepath          Ensure directories necessary for pipx operation are in
                         your PATH environment variable.
     environment         Print a list of environment variables and paths used
                         by pipx.
     completions         Print instructions on enabling shell completions for
                         pipx
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
