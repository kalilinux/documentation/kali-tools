---
Title: proximoth
Homepage: https://technicaluserx.gitlab.io/proximoth
Repository: https://gitlab.com/kalilinux/packages/proximoth
Architectures: any
Version: 1.0.0-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### proximoth
 
  Proximoth is a command-line tool to detect Wi-Fi
  devices in proximity with Control Frame Attack.
 
 **Installed size:** `961 KB`  
 **How to install:** `sudo apt install proximoth`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libpcap0.8t64 
 {{< /spoiler >}}
 
 ##### proximoth
 
 Control Frame Attack Vulnerability Detection Tool
 
 ```
 root@kali:~# proximoth -h
 Usage: proximoth [options] <target>
 
  <target>                            : MAC address of the target.
 
 options:
 
  -h, --help                          : Prints this screen.
 
  -o <file>, --out-file <file>        : File to write statistics after shutdown.
 
  -b <bssid>, --bssid <bssid>         : Custom BSSID to be injected as sender MAC address.
                                        Address is fixed automatically to be global and unicast.
 
  -a, --no-mac-autofix                : Disables unicast/global auto fix for BSSID MAC addresses.
 
  -i <iface>, --interface <iface>     : Wireless interface to use packet injection and sniffing.
                                        Obligatory option.
 
  -d <file>, --dump-file <file>       : Write all CTS captures to a PCAP file.
 
  -r <us>, --rts-interval <us>        : Microseconds as threshold to wait between RTS injections.
                                        Setting it to a low value might cause malfunction.
                                        Default: 500000
 
  -t, --text-mode                     : Enables text only mode.
 
  --version                           : Prints version number and author information.
 
 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
