---
Title: certi
Homepage: https://github.com/zer1t0/certi
Repository: https://gitlab.com/kalilinux/packages/certi
Architectures: all
Version: 0.0~git20230127.6cfa656-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### certi
 
  Certi is an utility to play with ADCS, allows one to request tickets and
  collect information about related objects. Basically, it's the impacket copy
  of Certify.
 
 **Installed size:** `118 KB`  
 **How to install:** `sudo apt install certi`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-cryptography
 * python3-impacket
 {{< /spoiler >}}
 
 ##### certi
 
 
 ```
 root@kali:~# certi -h
 usage: certi [-h] {list,req} ...
 
 positional arguments:
   {list,req}
 
 options:
   -h, --help  show this help message and exit
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
