---
Title: openssh-ssh1
Homepage: https://www.openssh.com/
Repository: https://salsa.debian.org/ssh-team/openssh-ssh1
Architectures: any
Version: 1:7.5p1-17
Metapackages: kali-linux-core kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-linux-nethunter kali-linux-wsl 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### openssh-client-ssh1
 
  This is the portable version of OpenSSH, a free implementation of
  the Secure Shell protocol as specified by the IETF secsh working
  group.
   
  Ssh (Secure Shell) is a program for logging into a remote machine
  and for executing commands on a remote machine.
  It provides secure encrypted communications between two untrusted
  hosts over an insecure network. X11 connections and arbitrary TCP/IP
  ports can also be forwarded over the secure channel.
  It can be used to provide applications with a secure communication
  channel.
   
  This package provides the ssh1 and scp1 clients and the ssh-keygen1
  utility, all built with support for the legacy SSH1 protocol. This
  protocol is obsolete and should not normally be used, but in some cases
  there may be no alternative way to connect to outdated servers.
   
  In some countries it may be illegal to use any encryption at all
  without a special permit.
   
  ssh replaces the insecure rsh, rcp and rlogin programs, which are
  obsolete for most purposes.
 
 **Installed size:** `1.35 MB`  
 **How to install:** `sudo apt install openssh-client-ssh1`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libselinux1 
 * libssl3t64 
 * zlib1g 
 {{< /spoiler >}}
 
 ##### scp1
 
 Secure copy (remote file copy program)
 
 ```
 root@kali:~# scp1 -h
 unknown option -- h
 usage: scp [-12346BCpqrv] [-c cipher] [-F ssh_config] [-i identity_file]
            [-l limit] [-o ssh_option] [-P port] [-S program]
            [[user@]host1:]file1 ... [[user@]host2:]file2
 ```
 
 - - -
 
 ##### ssh-keygen1
 
 Authentication key generation, management and conversion
 
 ```
 root@kali:~# ssh-keygen1 --help
 unknown option -- -
 usage: ssh-keygen [-q] [-b bits] [-t dsa | ecdsa | ed25519 | rsa | rsa1]
                   [-N new_passphrase] [-C comment] [-f output_keyfile]
        ssh-keygen -p [-P old_passphrase] [-N new_passphrase] [-f keyfile]
        ssh-keygen -i [-m key_format] [-f input_keyfile]
        ssh-keygen -e [-m key_format] [-f input_keyfile]
        ssh-keygen -y [-f input_keyfile]
        ssh-keygen -c [-P passphrase] [-C comment] [-f keyfile]
        ssh-keygen -l [-v] [-E fingerprint_hash] [-f input_keyfile]
        ssh-keygen -B [-f input_keyfile]
        ssh-keygen -D pkcs11
        ssh-keygen -F hostname [-f known_hosts_file] [-l]
        ssh-keygen -H [-f known_hosts_file]
        ssh-keygen -R hostname [-f known_hosts_file]
        ssh-keygen -r hostname [-f input_keyfile] [-g]
        ssh-keygen -G output_file [-v] [-b bits] [-M memory] [-S start_point]
        ssh-keygen -T output_file -f input_file [-v] [-a rounds] [-J num_lines]
                   [-j start_line] [-K checkpt] [-W generator]
        ssh-keygen -s ca_key -I certificate_identity [-h] [-n principals]
                   [-O option] [-V validity_interval] [-z serial_number] file ...
        ssh-keygen -L [-f input_keyfile]
        ssh-keygen -A
        ssh-keygen -k -f krl_file [-u] [-s ca_public] [-z version_number]
                   file ...
        ssh-keygen -Q -f krl_file file ...
 ```
 
 - - -
 
 ##### ssh1
 
 OpenSSH SSH client (remote login program)
 
 ```
 root@kali:~# ssh1 -h
 unknown option -- h
 usage: ssh [-1246AaCfGgKkMNnqsTtVvXxYy] [-b bind_address] [-c cipher_spec]
            [-D [bind_address:]port] [-E log_file] [-e escape_char]
            [-F configfile] [-I pkcs11] [-i identity_file]
            [-J [user@]host[:port]] [-L address] [-l login_name] [-m mac_spec]
            [-O ctl_cmd] [-o option] [-p port] [-Q query_option] [-R address]
            [-S ctl_path] [-W host:port] [-w local_tun[:remote_tun]]
            [user@]hostname [command]
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
