---
Title: juice-shop
Homepage: https://github.com/juice-shop/juice-shop
Repository: https://gitlab.com/kalilinux/packages/juice-shop
Architectures: amd64
Version: 16.0.1-0kali6
Metapackages: kali-linux-labs 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### juice-shop
 
  This package contains a modern and sophisticated insecure web application! It
  can be used in security trainings, awareness demos, CTFs and as a guinea pig
  for security tools! Juice Shop encompasses vulnerabilities from the entire
  OWASP Top Ten along with many other security flaws found in real-world
  applications!
   
  WARNING: Do not upload it to your hosting provider's public html folder or any
  Internet facing servers, as they will be compromised.
 
 **Installed size:** `1.17 GB`  
 **How to install:** `sudo apt install juice-shop`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * libc6 
 * libgcc-s1 
 * libnode115
 * libstdc++6 
 * lsof
 * npm
 * xdg-utils
 {{< /spoiler >}}
 
 ##### juice-shop
 
 
 ```
 root@kali:~# juice-shop -h
 [*] Please wait for the Juice-shop service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:42000
 
 ```
 
 - - -
 
 ##### juice-shop-stop
 
 
 ```
 root@kali:~# juice-shop-stop -h
 x juice-shop.service - juice-shop web application
      Loaded: loaded (/usr/lib/systemd/system/juice-shop.service; disabled; preset: disabled)
      Active: failed (Result: exit-code) since Sun 2025-03-02 17:57:52 EST; 4s ago
    Duration: 347ms
  Invocation: 121c4e02ea414455bd1e3fe3e037135d
     Process: 1541611 ExecStart=npm start (code=exited, status=7)
    Main PID: 1541611 (code=exited, status=7)
    Mem peak: 19.8M
         CPU: 326ms
 
 Mar 02 17:57:52 kali npm[1541611]:     at node:dns:52:5
 Mar 02 17:57:52 kali npm[1541611]:     at BuiltinModule.compileForInternalLoader (node:internal/bootstrap/realm:398:7)
 Mar 02 17:57:52 kali npm[1541611]:     at BuiltinModule.compileForPublicLoader (node:internal/bootstrap/realm:337:10)
 Mar 02 17:57:52 kali npm[1541611]:     at loadBuiltinModule (node:internal/modules/helpers:104:7)
 Mar 02 17:57:52 kali npm[1541611]:     at Module._load (node:internal/modules/cjs/loader:1076:17)
 Mar 02 17:57:52 kali npm[1541611]:     at Module.require (node:internal/modules/cjs/loader:1311:19)
 Mar 02 17:57:52 kali npm[1541611]:     at require (node:internal/modules/helpers:179:18)
 Mar 02 17:57:52 kali npm[1541611]: Node.js v20.18.3
 Mar 02 17:57:52 kali systemd[1]: juice-shop.service: Main process exited, code=exited, status=7/NOTRUNNING
 Mar 02 17:57:52 kali systemd[1]: juice-shop.service: Failed with result 'exit-code'.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
