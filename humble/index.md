---
Title: humble
Homepage: https://github.com/rfc-st/humble
Repository: https://gitlab.com/kalilinux/packages/humble
Architectures: all
Version: 1.46-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### humble
 
  This package contains an  humble, and fast, security-oriented HTTP headers
  analyzer.
 
 **Installed size:** `324 KB`  
 **How to install:** `sudo apt install humble`  
 
 {{< spoiler "Dependencies:" >}}
 * publicsuffix
 * python3
 * python3-colorama
 * python3-fpdf
 * python3-requests
 * python3-tldextract
 {{< /spoiler >}}
 
 ##### humble
 
 
 ```
 root@kali:~# humble -h
 usage: humble.py [-h] [-a] [-b] [-c] [-df] [-e [TESTSSL_PATH]]
                  [-f [FINGERPRINT_TERM]] [-g] [-grd] [-if INPUT_FILE]
                  [-l {es}] [-lic] [-o {csv,html,json,pdf,txt,xml}]
                  [-of OUTPUT_FILE] [-op OUTPUT_PATH] [-r]
                  [-s [SKIP_HEADERS ...]] [-u URL] [-ua USER_AGENT] [-v]
 
 'humble' (HTTP Headers Analyzer) | https://github.com/rfc-st/humble | v.2025-02-01
 
 options:
   -h, --help                      show this help message and exit
   -a                              Shows statistics of the performed analysis;
                                   if the '-u' parameter is ommited they will
                                   be global
   -b                              Shows overall findings; if omitted detailed
                                   ones will be shown
   -c                              Checks URL response HTTP headers for
                                   compliance with OWASP 'Secure Headers
                                   Project' best practices
   -df                             Do not follow redirects; if omitted the last
                                   redirection will be the one analyzed
   -e [TESTSSL_PATH]               Shows TLS/SSL checks; requires the PATH of
                                   https://testssl.sh/
   -f [FINGERPRINT_TERM]           Shows fingerprint statistics; if
                                   'FINGERPRINT_TERM' (e.g., 'Google') is
                                   omitted the top 20 results will be shown
   -g                              Shows guidelines for enabling security HTTP
                                   response headers on popular frameworks,
                                   servers and services
   -grd                            Shows the checks to grade an analysis, along
                                   with advice for improvement
   -if INPUT_FILE                  Analyzes 'INPUT_FILE': must contain HTTP
                                   response headers and values separated by ':
                                   '; E.g. 'server: nginx'
   -l {es}                         Defines the language for displaying
                                   analysis, errors and messages; if omitted,
                                   will be shown in English
   -lic                            Shows the license for 'humble', along with
                                   permissions, limitations and conditions.
   -o {csv,html,json,pdf,txt,xml}  Exports analysis to 'humble_scheme_URL_port_
                                   yyyymmdd_hhmmss_language.ext' file; json
                                   will have a brief analysis
   -of OUTPUT_FILE                 Exports analysis to 'OUTPUT_FILE'; if
                                   omitted the default filename of the
                                   parameter '-o' will be used
   -op OUTPUT_PATH                 Exports analysis to 'OUTPUT_PATH'; must be
                                   absolute. If omitted the PATH of 'humble.py'
                                   will be used
   -r                              Shows HTTP response headers and a detailed
                                   analysis; '-b' parameter will take priority
   -s [SKIP_HEADERS ...]           Skips 'deprecated/insecure' and 'missing'
                                   checks for the indicated 'SKIP_HEADERS'
                                   (separated by spaces)
   -u URL                          Scheme, host and port to analyze. E.g.
                                   https://google.com
   -ua USER_AGENT                  User-Agent ID from
                                   'additional/user_agents.txt' file to use.
                                   '0' will show all and '1' is the default
   -v, --version                   Checks for updates at
                                   https://github.com/rfc-st/humble
 
 examples:
   -u URL -a                       Shows statistics of the analysis performed against the URL
   -u URL -b                       Analyzes URL and reports overall findings
   -u URL -b -o csv                Analyzes URL and exports overall findings to CSV format
   -u URL -l es                    Analyzes URL and reports (in Spanish) detailed findings
   -u URL -o pdf                   Analyzes URL and exports detailed findings to PDF format
   -u URL -o html -of test         Analyzes URL and exports detailed findings to HTML format and 'test' filename
   -u URL -o pdf -op D:/Tests      Analyzes URL and exports detailed findings to PDF format and 'D:/Tests' path
   -u URL -r                       Analyzes URL and reports detailed findings along with HTTP response headers
   -u URL -s ETag NEL              Analyzes URL and skips 'deprecated/insecure' and 'missing' checks for 'ETag' and 'NEL' headers
   -u URL -ua 4                    Analyzes URL using the fourth User-Agent of 'additional/user_agents.txt' file
   -a -l es                        Shows statistics (in Spanish) of the analysis performed against all URLs
   -f Google                       Shows HTTP fingerprint headers related to the term 'Google'
 
 want to contribute?:
   How to                          https://github.com/rfc-st/humble/#contribute
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
