---
Title: hekatomb
Homepage: https://github.com/ProcessusT/HEKATOMB
Repository: https://gitlab.com/kalilinux/packages/hekatomb
Architectures: all
Version: 1.5.14+git20240731.bdd53cf-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### hekatomb
 
  Hekatomb is a Python script that connects to an LDAP directory to retrieve all
  computers and users' information. From there, it will download all DPAPI blobs
  of all users from all computers and use Domain backup keys to decrypt them.
 
 **Installed size:** `63 KB`  
 **How to install:** `sudo apt install hekatomb`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-chardet 
 * python3-dnspython 
 * python3-impacket
 * python3-ldap3 
 * python3-pycryptodome 
 {{< /spoiler >}}
 
 ##### hekatomb
 
 
 ```
 root@kali:~# hekatomb -h
 
 ██░ ██ ▓█████  ██ ▄█▀▄▄▄     ▄▄▄█████▓ ▒█████   ███▄ ▄███▓ ▄▄▄▄   
 ▓██░ ██▒▓█   ▀  ██▄█▒▒████▄   ▓  ██▒ ▓▒▒██▒  ██▒▓██▒▀█▀ ██▒▓█████▄ 
 ▒██▀▀██░▒███   ▓███▄░▒██  ▀█▄ ▒ ▓██░ ▒░▒██░  ██▒▓██    ▓██░▒██▒ ▄██
 ░▓█ ░██ ▒▓█  ▄ ▓██ █▄░██▄▄▄▄██░ ▓██▓ ░ ▒██   ██░▒██    ▒██ ▒██░█▀  
 ░▓█▒░██▓░▒████▒▒██▒ █▄▓█   ▓██▒ ▒██▒ ░ ░ ████▓▒░▒██▒   ░██▒░▓█  ▀█▓
  ▒ ░░▒░▒░░ ▒░ ░▒ ▒▒ ▓▒▒▒   ▓▒█░ ▒ ░░   ░ ▒░▒░▒░ ░ ▒░   ░  ░░▒▓███▀▒
  ▒ ░▒░ ░ ░ ░  ░░ ░▒ ▒░ ▒   ▒▒ ░   ░      ░ ▒ ▒░ ░  ░      ░▒░▒   ░ 
  ░  ░░ ░   ░   ░ ░░ ░  ░   ▒    ░      ░ ░ ░ ▒  ░      ░    ░    ░ 
  ░  ░  ░   ░  ░░  ░        ░  ░            ░ ░         ░    ░      
    Because Domain Admin rights are not enough.
 		Hack them all.
 
 	         @Processus
 	            v1.5
 **************************************************
 
 
 usage: hekatomb [-h] [-hashes LMHASH:NTHASH] [-pvk PVK] [-dns DNS]
                 [-port [port]] [-smb2] [-just-user JUST_USER]
                 [-just-computer JUST_COMPUTER] [-md5] [-csv] [-debug]
                 [-debugmax]
                 target
 
 Script used to automate domain computers and users extraction from LDAP and
 extraction of domain controller private key through RPC to collect and decrypt
 all users' DPAPI secrets saved in Windows credential manager.
 
 positional arguments:
   target                [[domain/]username[:password]@]<targetName or address
                         of DC>
 
 options:
   -h, --help            show this help message and exit
 
 authentication:
   -hashes LMHASH:NTHASH
                         NTLM hashes, format is LMHASH:NTHASH
 
 authentication:
   -pvk PVK              Domain backup keys file
   -dns DNS              DNS server IP address to resolve computers hostname
   -port [port]          Port to connect to SMB Server
   -smb2                 Force the use of SMBv2 protocol
   -just-user JUST_USER  Test only specified username
   -just-computer JUST_COMPUTER
                         Test only specified computer
   -md5                  Print md5 hash instead of clear passwords
 
 verbosity:
   -csv                  Export results to CSV file
   -debug                Turn DEBUG output ON
   -debugmax             Turn DEBUG output TO MAAAAXXXX
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
