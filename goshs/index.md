---
Title: goshs
Homepage: https://github.com/patrickhener/goshs
Repository: https://gitlab.com/kalilinux/packages/goshs
Architectures: amd64 arm64 armhf i386
Version: 0.4.1-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### goshs
 
  This tool provides a SimpleHTTPServer written in Go, enhanced with features
  and security. This package contains a simple http server like the Python
  SimpleHTTPServer but enhanced with a lot of helpful features and security in
  mind.
 
 **Installed size:** `11.95 MB`  
 **How to install:** `sudo apt install goshs`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### goshs
 
 
 ```
 root@kali:~# goshs -h
 
 goshs v0.4.1
 Usage: goshs [options]
 
 Web server options:
   -i,  --ip           The ip/if-name to listen on             (default: 0.0.0.0)
   -p,  --port         The port to listen on                   (default: 8000)
   -d,  --dir          The web root directory                  (default: current working path)
   -w,  --webdav       Also serve using webdav protocol        (default: false)
   -wp, --webdav-port  The port to listen on for webdav        (default: 8001)
   -ro, --read-only    Read only mode, no upload possible      (default: false)
   -uo, --upload-only  Upload only mode, no download possible  (default: false)
   -si, --silent       Running without dir listing             (default: false)
   -c,  --cli          Enable cli (only with auth and tls)     (default: false)
   -e,  --embedded     Show embedded files in UI               (default: false)
 
 TLS options:
   -s,   --ssl          Use TLS
   -ss,  --self-signed  Use a self-signed certificate
   -sk,  --server-key   Path to server key
   -sc,  --server-cert  Path to server certificate
   -p12, --pkcs12       Path to server p12
   -sl,  --lets-encrypt Use Let's Encrypt as certification service
   -sld, --le-domains   Domain(s) to request from Let's Encrypt		(comma separated list)
   -sle, --le-email     Email to use with Let's Encrypt
   -slh, --le-http      Port to use for Let's Encrypt HTTP Challenge	(default: 80)
   -slt, --le-tls       Port to use for Let's Encrypt TLS ALPN Challenge (default: 443)
 
 Authentication options:
   -b,  --basic-auth    Use basic authentication (user:pass - user can be empty)
   -ca, --cert-auth     Use certificate based authentication - provide ca certificate
   -H,  --hash          Hash a password for file based ACLs
 
 Misc options:
   -u  --user          Drop privs to user (unix only)          (default: current user)
   -V  --verbose       Activate verbose log output             (default: false)
   -v                  Print the current goshs version
 
 Usage examples:
   Start with default values:    	./goshs
   Start with wevdav support:    	./goshs -w
   Start with different port:    	./goshs -p 8080
   Start with self-signed cert:  	./goshs -s -ss
   Start with let's encrypt:		./goshs -s -sl -sle your@mail.com -sld your.domain.com,your.seconddomain.com
   Start with custom cert:       	./goshs -s -sk <path to key> -sc <path to cert>
   Start with basic auth:        	./goshs -b secret-user:$up3r$3cur3
   Start with basic auth empty user:	./goshs -b :$up3r$3cur3
   Start with cli enabled:           	./goshs -b secret-user:$up3r$3cur3 -s -ss -c
 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
