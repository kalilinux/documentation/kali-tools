---
Title: freerdp3
Homepage: https://www.freerdp.com/
Repository: https://salsa.debian.org/debian-remote-team/freerdp3
Architectures: any
Version: 3.12.0+dfsg-1
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-information-gathering kali-tools-passwords kali-tools-top10 kali-tools-vulnerability kali-tools-web 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### freerdp3-dev
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the FreeRDP development files.
 
 **Installed size:** `1.21 MB`  
 **How to install:** `sudo apt install freerdp3-dev`  
 
 {{< spoiler "Dependencies:" >}}
 * libcjson-dev
 * libfreerdp-client3-3 
 * libfreerdp-server-proxy3-3 
 * libfreerdp-server3-3 
 * libfreerdp-shadow-subsystem3-3 
 * libfreerdp-shadow3-3 
 * libfreerdp3-3 
 * libfuse3-dev
 * libwinpr3-dev 
 * winpr3-utils 
 {{< /spoiler >}}
 
 
 - - -
 
 ### freerdp3-proxy-modules
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the modules for the proxy.
 
 **Installed size:** `352 KB`  
 **How to install:** `sudo apt install freerdp3-proxy-modules`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-server-proxy3-3 
 * libfreerdp3-3 
 * libgcc-s1 
 * libstdc++6 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### freerdp3-sdl
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the SDL based client.
 
 **Installed size:** `1.35 MB`  
 **How to install:** `sudo apt install freerdp3-sdl`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-client3-3 
 * libfreerdp3-3 
 * libgcc-s1 
 * libsdl2-2.0-0 
 * libsdl2-image-2.0-0 
 * libsdl2-ttf-2.0-0 
 * libstdc++6 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 ##### sdl-freerdp3
 
 FreeRDP SDL client
 
 ```
 root@kali:~# sdl-freerdp3 --help
 
 FreeRDP - A Free Remote Desktop Protocol Implementation
 See www.freerdp.com for more information
 
 Usage: sdl-freerdp3 [file] [options] [/v:<server>[:port]]
 
 Syntax:
     /flag (enables flag)
     /option:<value> (specifies option with value)
     +toggle -toggle (enables or disables toggle, where '/' is a synonym of '+')
 
     /a: <addin>[,<options>]           Addin
     /action-script: <file-name>       Action script
     +admin                            Admin (or console) session
     +aero                             Enable desktop composition
     /app: program:[<path>|<||alias>],cmd:<command>,file:<filename>,guid:<guid>,
           icon:<filename>,name:<name>,workdir:<directory>,hidef:[on|off]
                                       Remote application program
     /args-from: <file>|stdin|fd:<number>|env:<name>
                                       Read command line from a file, stdin or 
                                       file descriptor. This argument can not be 
                                       combined with any other. Provide one 
                                       argument per line.
     /assistance: <password>           Remote assistance password
     +async-channels                   Enable Asynchronous channels 
                                       (experimental)
     +async-update                     Enable Asynchronous update
     /audio-mode: <mode>               Audio output mode
     +auth-only                        Enable Authenticate only
     /auth-pkg-list: <!ntlm,kerberos>  Authentication package filter 
                                       (comma-separated list, use '!' to 
                                       exclude)
     -authentication                   Disable Authentication (experimental)
     +auto-reconnect                   Enable Automatic reconnection
     /auto-reconnect-max-retries: <retries>
                                       Automatic reconnection maximum retries, 0 
                                       for unlimited [0,1000]
     +auto-request-control             Automatically request remote assistance 
                                       input control
     /azure: [tenantid:<id>],[use-tenantid[:[on|off]],[ad:<url>]
                                       AzureAD options
     /bpp: <depth>                     Session bpp (color depth)
     +buildconfig                      Print the build configuration
     /cache: [bitmap[:on|off],codec[:rfx|nsc],glyph[:on|off],offscreen[:on|off],
             persist,persist-file:<filename>]
                                       
     /cert: [deny,ignore,name:<name>,tofu,fingerprint:<hash>:<hash as hex>[,
            fingerprint:<hash>:<another hash>]]
                                       Certificate accept options. Use with 
                                       care!
                                       
                                        * deny         ... Automatically abort 
                                       connection if the certificate does not 
                                       match, no user interaction.
                                       
                                        * ignore       ... Ignore the 
                                       certificate checks altogether (overrules 
                                       all other options)
                                       
                                        * name         ... Use the alternate 
                                       <name> instead of the certificate subject 
                                       to match locally stored certificates
                                       
                                        * tofu         ... Accept certificate 
                                       unconditionally on first connect and deny 
                                       on subsequent connections if the 
                                       certificate does not match
                                       
                                        * fingerprints ... A list of certificate 
                                       hashes that are accepted unconditionally 
                                       for a connection
     /client-build-number: <number>    Client Build Number sent to server 
                                       (influences smartcard behaviour, see 
                                       [MS-RDPESC])
     /client-hostname: <name>          Client Hostname to send to server
     [-|/]clipboard[: [[use-selection:<atom>],[direction-to:[
                      all|local|remote|off]],[files-to[:all|local|remote|off]]]]
                                       Disable Redirect clipboard:
                                       
                                        * use-selection:<atom>  ... (X11) 
                                       Specify which X selection to access. 
                                       Default is CLIPBOARD. PRIMARY is the 
                                       X-style middle-click selection.
                                       
                                        * direction-to:[all|local|remote|off] 
                                       control enabled clipboard direction
                                       
                                        * files-to:[all|local|remote|off] 
                                       control enabled file clipboard direction
     -compression                      Disable compression
     /compression-level: <level>       Compression level (0,1,2)
     +credentials-delegation           Enable credentials delegation
     /d: <domain>                      Domain
     -decorations                      Disable Window decorations
     +disable-output                   Deactivate all graphics decoding in the 
                                       client session. Useful for load tests 
                                       with many simultaneous connections
     +disp                             Display control
     /drive: <name>,<path>             Redirect directory <path> as named share 
                                       <name>. Hotplug support is enabled with 
                                       /drive:hotplug,*. This argument provides 
                                       the same function as "Drives that I plug 
                                       in later" option in MSTSC.
     +drives                           Enable Redirect all mount points as 
                                       shares
     /dump: <record|replay>,file:<file>[,nodelay]
                                       record or replay dump
     /dvc: <channel>[,<options>]       Dynamic virtual channel
     +dynamic-resolution               Enable Send resolution updates when the 
                                       window is resized
     +echo                             Echo channel
     -encryption                       Disable Encryption (experimental)
     /encryption-methods: [40,][56,][128,][FIPS]
                                       RDP standard security encryption methods
     +f                                Fullscreen mode (<Ctrl>+<Alt>+<Enter> 
                                       toggles fullscreen)
     +fipsmode                         Enable FIPS mode
     /floatbar[: sticky:[on|off],default:[visible|hidden],show:[
                 always|fullscreen|window]]
                                       floatbar is disabled by default (when 
                                       enabled defaults to sticky in fullscreen 
                                       mode)
     -fonts                            Disable smooth fonts (ClearType)
     +force-console-callbacks          Enable Use default callbacks (console) 
                                       for certificate/credential/...
     /frame-ack: <number>              Number of frame acknowledgement
     /from-stdin[: force]              Read credentials from stdin. With <force> 
                                       the prompt is done before connection, 
                                       otherwise on server request.
     /gateway: g:<gateway>[:<port>],u:<user>,d:<domain>,p:<password>,
               usage-method:[direct|detect],access-token:<token>,type:[rpc|http[,
               no-websockets][,extauth-sspi-ntlm]|auto[,no-websockets][,
               extauth-sspi-ntlm]]|arm,url:<wss://url>,
               bearer:<oauth2-bearer-token>
                                       Gateway Hostname
     /gdi: sw|hw                       GDI rendering
     +geometry                         Geometry tracking channel
     +gestures                         Enable Consume multitouch input locally
     /gfx[: [[progressive[:on|off]|RFX[:on|off]|AVC420[:on|off]AVC444[:on|off]],
            mask:<value>,small-cache[:on|off],thin-client[:on|off],progressive[
            :on|off],frame-ack[:on|off]]]
                                       RDP8 graphics pipeline
     -grab-keyboard                    Disable Grab keyboard focus, forward all 
                                       keys to remote
     -grab-mouse                       Disable Grab mouse focus, forward all 
                                       events to remote
     /h: <height>                      Height
     -heartbeat                        Disable Support heartbeat PDUs
     +help                             Print help
     +home-drive                       Enable Redirect user home as share
     /ipv4[: [:force]]                 Prefer IPv4 A record over IPv6 AAAA 
                                       record
     /ipv6[: [:force]]                 Prefer IPv6 AAAA record over IPv4 A 
                                       record
     +jpeg                             JPEG codec support
     /jpeg-quality: <percentage>       JPEG quality
     /kbd: [layout:[0x<id>|<name>],lang:<0x<id>>,fn-key:<value>,type:<value>,
           subtype:<value>,unicode[:on|off],remap:<key1>=<value1>,
           remap:<key2>=<value2>,pipe:<filename>]
                                       Keyboard related options:
                                       
                                        * layout: set the keybouard layout 
                                       announced to the server
                                       
                                        * lang: set the keyboard language 
                                       identifier sent to the server
                                       
                                        * fn-key: Function key value
                                       
                                        * pipe: Name of a named pipe that can be 
                                       used to type text into the RDP session
                                       
                                       
     /kerberos: [kdc-url:<url>,lifetime:<time>,start-time:<time>,
                renewable-lifetime:<time>,cache:<path>,armor:<path>,
                pkinit-anchors:<path>,pkcs11-module:<name>]
                                       Kerberos options
     /list: [kbd|kbd-scancode|kbd-lang[:<value>]|smartcard[:[
            pkinit-anchors:<path>][,pkcs11-module:<name>]]
            |monitor|tune|timezones]   List available options for subcommand
     /load-balance-info: <info-string> Load balance info
     /log-filters: <tag>:<level>[,<tag>:<level>[,...]]
                                       Set logger filters, see wLog(7) for 
                                       details
     /log-level: [OFF|FATAL|ERROR|WARN|INFO|DEBUG|TRACE]
                                       Set the default log level, see wLog(7) 
                                       for details
     /max-fast-path-size: <size>       Specify maximum fast-path update size
     /max-loop-time: <time>            Specify maximum time in milliseconds 
                                       spend treating packets
     +menu-anims                       Enable menu animations
     /microphone[: [sys:<sys>,][dev:<dev>,][format:<format>,][rate:<rate>,][
                   channel:<channel>]] Audio input (microphone)
     /monitors: <id>[,<id>[,...]]      Select monitors to use (only effective in 
                                       fullscreen or multimonitor mode)
     /mouse: [relative:[on|off],grab:[on|off]]
                                       Mouse related options:
                                       
                                        * relative:   send relative mouse 
                                       movements if supported by server
                                       
                                        * grab:       grab the mouse if within 
                                       the window
     -mouse-motion                     Disable Send mouse motion
     +mouse-relative                   Enable Send mouse motion with relative 
                                       addressing
     /multimon[: force]                Use multiple monitors
     +multitouch                       Enable Redirect multitouch input
     -multitransport                   Disable Support multitransport protocol
     -nego                             Disable protocol security negotiation
     /network: [invalid|modem|broadband|broadband-low|broadband-high|wan|lan|auto]
                                       Network connection type
     +nsc                              NSCodec support
     +old-license                      Enable Use the old license workflow (no 
                                       CAL and hwId set to 0)
     /orientation: [0|90|180|270]      Orientation of display in degrees
     /p: <password>                    Password
     /parallel[: <name>[,<path>]]      Redirect parallel device
     /parent-window: <window-id>       Parent window id
     /pcb: <blob>                      Preconnection Blob
     /pcid: <id>                       Preconnection Id
     /pheight: <height>                Physical height of display (in 
                                       millimeters)
     /play-rfx: <pcap-file>            Replay rfx pcap file
     /port: <number>                   Server port
     /prevent-session-lock[: <time in sec>]
                                       Prevent session locking by injecting fake 
                                       mouse motion events to the server when 
                                       the connection is idle (default interval: 
                                       180 seconds)
     +print-reconnect-cookie           Enable Print base64 reconnect cookie 
                                       after connecting
     /printer[: <name>[,<driver>[,default]]]
                                       Redirect printer device
     /proxy: [<proto>://][<user>:<password>@]<host>[:<port>]
                                       Proxy settings: override env. var (see 
                                       also environment variable below). 
                                       Protocol "socks5" should be given 
                                       explicitly where "http" is default.
     /pth: <password-hash>             Pass the hash (restricted admin mode)
     /pwidth: <width>                  Physical width of display (in 
                                       millimeters)
     /rdp2tcp: <executable path[:arg...]>
                                       TCP redirection
     /reconnect-cookie: <base64-cookie> Pass base64 reconnect cookie to the 
                                       connection
     /redirect-prefer: <FQDN|IP|NETBIOS>,[...]
                                       Override the preferred redirection order
     +relax-order-checks               Do not check if a RDP order was announced 
                                       during capability exchange, only use when 
                                       connecting to a buggy server
     +remoteGuard                      Remote guard credentials
     +restricted-admin                 Restricted admin mode
     +rfx                              RemoteFX
     /rfx-mode: [image|video]          RemoteFX mode
     /scale: [100|140|180]             Scaling factor of the display
     /scale-desktop: <percentage>      Scaling factor for desktop applications 
                                       (value between 100 and 500)
     /scale-device: 100|140|180        Scaling factor for app store applications
     /sec: [rdp[:[on|off]]|tls[:[on|off]]|nla[:[on|off]]|ext[:[on|off]]|aad[:[
           on|off]]]                   Force specific protocol security. e.g. 
                                       /sec:nla enables NLA and disables all 
                                       others, while /sec:nla:[on|off] just 
                                       toggles NLA
     /serial[: <name>[,<path>[,<driver>[,permissive]]]]
                                       Redirect serial device
     /server-name: <name>              User-specified server name to use for 
                                       validation (TLS, Kerberos)
     /shell: <shell>                   Alternate shell
     /shell-dir: <dir>                 Shell working directory
     /size: <width>x<height> or <percent>%[wh]
                                       Screen size
     /smart-sizing[: <width>x<height>] Scale remote desktop to window size
     /smartcard[: <str>[,<str>...]]    Redirect the smartcard devices containing 
                                       any of the <str> in their names.
     /smartcard-logon[: [cert:<path>,key:<key>,pin:<pin>,csp:<csp name>,
                        reader:<reader>,card:<card>]]
                                       Activates Smartcard (optional 
                                       certificate) Logon authentication.
     /sound[: [sys:<sys>,][dev:<dev>,][format:<format>,][rate:<rate>,][
              channel:<channel>,][latency:<latency>,][quality:<quality>]]
                                       Audio output (sound)
     +span                             Span screen over multiple monitors
     /spn-class: <service-class>       SPN authentication service class
     +ssh-agent                        SSH Agent forwarding channel
     /sspi-module: <SSPI module path>  SSPI shared library module file path
     -suppress-output                  Disable suppress output when minimized
     /t: <title>                       Window title
     -themes                           Disable themes
     /timeout: <time in ms>            Advanced setting for high latency links: 
                                       Adjust connection timeout, use if you 
                                       encounter timeout failures with your 
                                       connection
     /timezone: <windows timezone>     Use supplied windows timezone for 
                                       connection (requires server support), see 
                                       /list:timezones for allowed values
     /tls: [ciphers|seclevel|secrets-file|enforce]
                                       TLS configuration options: * 
                                       ciphers:[netmon|ma|<cipher names>]
                                       
                                        * seclevel:<level>, default: 1, range: 
                                       [0-5] Override the default TLS security 
                                       level, might be required for older target 
                                       servers
                                       
                                        * secrets-file:<filename>
                                       
                                        * enforce[:[ssl3|1.0|1.1|1.2|1.3]] Force 
                                       use of SSL/TLS version for a connection. 
                                       Some servers have a buggy TLS version 
                                       negotiation and might fail without this. 
                                       Defaults to TLS 1.2 if no argument is 
                                       supplied. Use 1.0 for windows 7
     -toggle-fullscreen                Disable Alt+Ctrl+Enter to toggle 
                                       fullscreen
     /tune: <setting:value>,<setting:value>
                                       [experimental] directly manipulate 
                                       freerdp settings, use with extreme 
                                       caution!
     /u: [[<domain>\]<user>|<user>[@<domain>]]
                                       Username
     +unmap-buttons                    Enable Let server see real physical 
                                       pointer button
     /usb: [dbg,][id:<vid>:<pid>#...,][addr:<bus>:<addr>#...,][auto]
                                       Redirect USB device
     /v: <server>[:port]               Server hostname
     /vc: <channel>[,<options>]        Static virtual channel
     +version                          Print version
     +video                            Video optimized remoting channel
     /vmconnect[: <vmid>]              Hyper-V console (use port 2179, disable 
                                       negotiation)
     /w: <width>                       Width
     -wallpaper                        Disable wallpaper
     +window-drag                      Enable full window drag
     /window-position: <xpos>x<ypos>   window position
     /winscard-module: <WinSCard module path>
                                       WinSCard shared library module file path
     /wm-class: <class-name>           Set the WM_CLASS hint for the window 
                                       instance
     +workarea                         Use available work area
 
 Examples:
     sdl-freerdp3 connection.rdp /p:Pwd123! /f
     sdl-freerdp3 /u:CONTOSO\JohnDoe /p:Pwd123! /v:rdp.contoso.com
     sdl-freerdp3 /u:JohnDoe /p:Pwd123! /w:1366 /h:768 /v:192.168.1.100:4489
     sdl-freerdp3 /u:JohnDoe /p:Pwd123! /vmconnect:C824F53E-95D2-46C6-9A18-23A5BB403532 /v:192.168.1.100
     sdl-freerdp3 /u:\AzureAD\user@corp.example /p:pwd /v:host
 
 Clipboard Redirection: +clipboard
 
 Drive Redirection: /drive:home,/home/user
 Smartcard Redirection: /smartcard:<device>
 Smartcard logon with Kerberos authentication: /smartcard-logon /sec:nla
 Serial Port Redirection: /serial:<name>,<device>,[SerCx2|SerCx|Serial],[permissive]
 Serial Port Redirection: /serial:COM1,/dev/ttyS0
 Parallel Port Redirection: /parallel:<name>,<device>
 Printer Redirection: /printer:<device>,<driver>,[default]
 TCP redirection: /rdp2tcp:/usr/bin/rdp2tcp
 
 Audio Output Redirection: /sound:sys:oss,dev:1,format:1
 Audio Output Redirection: /sound:sys:alsa
 Audio Input Redirection: /microphone:sys:oss,dev:1,format:1
 Audio Input Redirection: /microphone:sys:alsa
 
 Multimedia Redirection: /video
 USB Device Redirection: /usb:id:054c:0268#4669:6e6b,addr:04:0c
 
 For Gateways, the https_proxy environment variable is respected:
     export https_proxy=http://proxy.contoso.com:3128/
     sdl-freerdp3 /g:rdp.contoso.com ...
 
 More documentation is coming, in the meantime consult source files
 
 CONFIGURATION FILE
 
   The SDL client supports some user defined configuration options.
   Settings are stored in JSON format
   The location is a per user file. Location for current user is /root/.config/freerdp/sdl-freerdp.json
   The XDG_CONFIG_HOME environment variable can be used to override the base directory.
 
   The following configuration options are supported:
 
     SDL_KeyModMask
       Defines the key combination required for SDL client shortcuts.
       Default KMOD_RSHIFT
       An array of SDL_Keymod strings as defined at https://wiki.libsdl.org/SDL2/SDL_Keymod
 
     SDL_Fullscreen
       Toggles client fullscreen state.
       Default SDL_SCANCODE_RETURN.
       A string as defined at https://wiki.libsdl.org/SDL2/SDLScancodeLookup
 
     SDL_Minimize
       Minimizes client windows.
       Default SDL_SCANCODE_M.
       A string as defined at https://wiki.libsdl.org/SDL2/SDLScancodeLookup
 
     SDL_Resizeable
       Toggles local window resizeable state.
       Default SDL_SCANCODE_R.
       A string as defined at https://wiki.libsdl.org/SDL2/SDLScancodeLookup
 
     SDL_Grab
       Toggles keyboard and mouse grab state.
       Default SDL_SCANCODE_G.
       A string as defined at https://wiki.libsdl.org/SDL2/SDLScancodeLookup
 
     SDL_Disconnect
       Disconnects from the RDP session.
       Default SDL_SCANCODE_D.
       A string as defined at https://wiki.libsdl.org/SDL2/SDLScancodeLookup
 ```
 
 - - -
 
 ### freerdp3-shadow-x11
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains a "shadowing" server that can be used to
  share an already started X11 DISPLAY.
 
 **Installed size:** `172 KB`  
 **How to install:** `sudo apt install freerdp3-shadow-x11`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-shadow-subsystem3-3 
 * libfreerdp-shadow3-3 
 * libfreerdp3-3 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 ##### freerdp-shadow-cli3
 
 (unknown subject)
 
 ```
 root@kali:~# freerdp-shadow-cli3 -h
 Usage: freerdp-shadow-cli3 [options]
 
 Notes: By default NLA security is active.
 	In this mode a SAM database is required.
 	Provide one with /sam-file:<file with path>
 	else the default path /etc/winpr/SAM is used.
 	If there is no existing SAM file authentication for all users will fail.
 
 	If authentication against PAM is desired, start with -sec-nla (requires compiled in support for PAM)
 
 Syntax:
     /flag (enables flag)
     /option:<value> (specifies option with value)
     +toggle -toggle (enables or disables toggle, where '/' is a synonym of '+')
 
     /log-filters:<tag>:<level>[,<tag>:<level>[,...]]
 	Set logger filters, see wLog(7) for details
     /log-level:[OFF|FATAL|ERROR|WARN|INFO|DEBUG|TRACE]
 	Set the default log level, see wLog(7) for details
     /port:<number>       
 	Server port
     /ipc-socket:<ipc-socket>
 	Server IPC socket
     /bind-address:<bind-address>[,<another address>, ...]
 	An address to bind to. Use '[<ipv6>]' for IPv6 addresses, e.g. '[::1]' for localhost
     /monitors:<0,1,2...> 
 	Select or list monitors
     /max-connections:<number>
 	maximum connections allowed to server, 0 to deactivate
     /rect:<x,y,w,h>      
 	Select rectangle within monitor to share
     -auth (default:on)   
 	Clients must authenticate
     +remote-guard (default:off)
 	Remote credential guard
     -may-view (default:on)
 	Clients may view without prompt
     -may-interact (default:on)
 	Clients may interact without prompt
     /sec:<rdp|tls|nla|ext>
 	force specific protocol security
     -sec-rdp (default:on)
 	rdp protocol security
     -sec-tls (default:on)
 	tls protocol security
     -sec-nla (default:on)
 	nla protocol security
     +sec-ext (default:off)
 	nla extended protocol security
     /sam-file:<file>     
 	NTLM SAM file for NLA authentication
     /keytab:<file>       
 	Kerberos keytab file for NLA authentication
     /ccache:<file>       
 	Kerberos host ccache file for NLA authentication
     /tls-secrets-file:<file>
 	file where tls secrets shall be stored
     -nsc (default:on)    
 	Allow NSC codec
     -rfx (default:on)    
 	Allow RFX surface bits
     -gfx (default:on)    
 	Allow GFX pipeline
     -gfx-progressive (default:on)
 	Allow GFX progressive codec
     -gfx-rfx (default:on)
 	Allow GFX RFX codec
     -gfx-planar (default:on)
 	Allow GFX planar codec
     -gfx-avc420 (default:on)
 	Allow GFX AVC420 codec
     -gfx-avc444 (default:on)
 	Allow GFX AVC444 codec
     /version             
 	Print version
     /buildconfig         
 	Print the build configuration
     /help                
 	Print help
 ```
 
 - - -
 
 ### freerdp3-wayland
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the wayland based client.
 
 **Installed size:** `245 KB`  
 **How to install:** `sudo apt install freerdp3-wayland`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-client3-3 
 * libfreerdp3-3 
 * libwayland-client0 
 * libwayland-cursor0 
 * libwinpr3-3 
 * libxkbcommon0 
 {{< /spoiler >}}
 
 ##### wlfreerdp3
 
 (unknown subject)
 
 ```
 root@kali:~# man wlfreerdp3
 1(2017-01-12)                                                     1(2017-01-12)
 
 NAME
         - FreeRDP wayland client
 
 SYNOPSIS
        [file] [default_client_options] [/v:<server>[:port]] [/version] [/help]
 
 DESCRIPTION
        is  a  wayland Remote Desktop Protocol (RDP) client which is part of the
        FreeRDP project. A RDP server is built-in to many editions of  Windows..
        Alternative  servers  included ogon, gnome-remote-desktop, xrdp and VRDP
        (VirtualBox).
 
 OPTIONS
        The wayland client also supports a lot of  the  default  client  options
        which  are  not described here. For details on those see the xfreerdp(1)
        man page.
 
        /v:<server>[:port]
               The server hostname or IP, and optionally the  port,  to  connect
               to.
 
        /version
               Print the version and exit.
 
        /help  Print the help and exit.
 
 EXIT STATUS
        0      Successful program execution.
 
        not 0  On failure.
 
 SEE ALSO
        xfreerdp(1) wlog(7)
 
 AUTHOR
        FreeRDP <team@freerdp.com>
 
 FreeRDP                              3.12.0                       1(2017-01-12)
 ```
 
 - - -
 
 ### freerdp3-x11
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the X11 based client.
 
 **Installed size:** `844 KB`  
 **How to install:** `sudo apt install freerdp3-x11`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-client3-3 
 * libfreerdp3-3 
 * libwinpr3-3 
 * libx11-6 
 * libxcursor1 
 * libxext6
 * libxfixes3
 * libxi6 
 * libxinerama1 
 * libxrandr2 
 * libxrender1
 {{< /spoiler >}}
 
 ##### xfreerdp3
 
 FreeRDP X11 client
 
 ```
 root@kali:~# xfreerdp3 --help
 
 FreeRDP - A Free Remote Desktop Protocol Implementation
 See www.freerdp.com for more information
 
 Usage: xfreerdp3 [file] [options] [/v:<server>[:port]]
 
 Syntax:
     /flag (enables flag)
     /option:<value> (specifies option with value)
     +toggle -toggle (enables or disables toggle, where '/' is a synonym of '+')
 
     /a: <addin>[,<options>]           Addin
     /action-script: <file-name>       Action script
     +admin                            Admin (or console) session
     +aero                             Enable desktop composition
     /app: program:[<path>|<||alias>],cmd:<command>,file:<filename>,guid:<guid>,
           icon:<filename>,name:<name>,workdir:<directory>,hidef:[on|off]
                                       Remote application program
     /args-from: <file>|stdin|fd:<number>|env:<name>
                                       Read command line from a file, stdin or 
                                       file descriptor. This argument can not be 
                                       combined with any other. Provide one 
                                       argument per line.
     /assistance: <password>           Remote assistance password
     +async-channels                   Enable Asynchronous channels 
                                       (experimental)
     +async-update                     Enable Asynchronous update
     /audio-mode: <mode>               Audio output mode
     +auth-only                        Enable Authenticate only
     /auth-pkg-list: <!ntlm,kerberos>  Authentication package filter 
                                       (comma-separated list, use '!' to 
                                       exclude)
     -authentication                   Disable Authentication (experimental)
     +auto-reconnect                   Enable Automatic reconnection
     /auto-reconnect-max-retries: <retries>
                                       Automatic reconnection maximum retries, 0 
                                       for unlimited [0,1000]
     +auto-request-control             Automatically request remote assistance 
                                       input control
     /azure: [tenantid:<id>],[use-tenantid[:[on|off]],[ad:<url>]
                                       AzureAD options
     /bpp: <depth>                     Session bpp (color depth)
     +buildconfig                      Print the build configuration
     /cache: [bitmap[:on|off],codec[:rfx|nsc],glyph[:on|off],offscreen[:on|off],
             persist,persist-file:<filename>]
                                       
     /cert: [deny,ignore,name:<name>,tofu,fingerprint:<hash>:<hash as hex>[,
            fingerprint:<hash>:<another hash>]]
                                       Certificate accept options. Use with 
                                       care!
                                       
                                        * deny         ... Automatically abort 
                                       connection if the certificate does not 
                                       match, no user interaction.
                                       
                                        * ignore       ... Ignore the 
                                       certificate checks altogether (overrules 
                                       all other options)
                                       
                                        * name         ... Use the alternate 
                                       <name> instead of the certificate subject 
                                       to match locally stored certificates
                                       
                                        * tofu         ... Accept certificate 
                                       unconditionally on first connect and deny 
                                       on subsequent connections if the 
                                       certificate does not match
                                       
                                        * fingerprints ... A list of certificate 
                                       hashes that are accepted unconditionally 
                                       for a connection
     /client-build-number: <number>    Client Build Number sent to server 
                                       (influences smartcard behaviour, see 
                                       [MS-RDPESC])
     /client-hostname: <name>          Client Hostname to send to server
     [-|/]clipboard[: [[use-selection:<atom>],[direction-to:[
                      all|local|remote|off]],[files-to[:all|local|remote|off]]]]
                                       Disable Redirect clipboard:
                                       
                                        * use-selection:<atom>  ... (X11) 
                                       Specify which X selection to access. 
                                       Default is CLIPBOARD. PRIMARY is the 
                                       X-style middle-click selection.
                                       
                                        * direction-to:[all|local|remote|off] 
                                       control enabled clipboard direction
                                       
                                        * files-to:[all|local|remote|off] 
                                       control enabled file clipboard direction
     -compression                      Disable compression
     /compression-level: <level>       Compression level (0,1,2)
     +credentials-delegation           Enable credentials delegation
     /d: <domain>                      Domain
     -decorations                      Disable Window decorations
     +disable-output                   Deactivate all graphics decoding in the 
                                       client session. Useful for load tests 
                                       with many simultaneous connections
     +disp                             Display control
     /drive: <name>,<path>             Redirect directory <path> as named share 
                                       <name>. Hotplug support is enabled with 
                                       /drive:hotplug,*. This argument provides 
                                       the same function as "Drives that I plug 
                                       in later" option in MSTSC.
     +drives                           Enable Redirect all mount points as 
                                       shares
     /dump: <record|replay>,file:<file>[,nodelay]
                                       record or replay dump
     /dvc: <channel>[,<options>]       Dynamic virtual channel
     +dynamic-resolution               Enable Send resolution updates when the 
                                       window is resized
     +echo                             Echo channel
     -encryption                       Disable Encryption (experimental)
     /encryption-methods: [40,][56,][128,][FIPS]
                                       RDP standard security encryption methods
     +f                                Fullscreen mode (<Ctrl>+<Alt>+<Enter> 
                                       toggles fullscreen)
     +fipsmode                         Enable FIPS mode
     /floatbar[: sticky:[on|off],default:[visible|hidden],show:[
                 always|fullscreen|window]]
                                       floatbar is disabled by default (when 
                                       enabled defaults to sticky in fullscreen 
                                       mode)
     -fonts                            Disable smooth fonts (ClearType)
     +force-console-callbacks          Enable Use default callbacks (console) 
                                       for certificate/credential/...
     /frame-ack: <number>              Number of frame acknowledgement
     /from-stdin[: force]              Read credentials from stdin. With <force> 
                                       the prompt is done before connection, 
                                       otherwise on server request.
     /gateway: g:<gateway>[:<port>],u:<user>,d:<domain>,p:<password>,
               usage-method:[direct|detect],access-token:<token>,type:[rpc|http[,
               no-websockets][,extauth-sspi-ntlm]|auto[,no-websockets][,
               extauth-sspi-ntlm]]|arm,url:<wss://url>,
               bearer:<oauth2-bearer-token>
                                       Gateway Hostname
     /gdi: sw|hw                       GDI rendering
     +geometry                         Geometry tracking channel
     +gestures                         Enable Consume multitouch input locally
     /gfx[: [[progressive[:on|off]|RFX[:on|off]|AVC420[:on|off]AVC444[:on|off]],
            mask:<value>,small-cache[:on|off],thin-client[:on|off],progressive[
            :on|off],frame-ack[:on|off]]]
                                       RDP8 graphics pipeline
     -grab-keyboard                    Disable Grab keyboard focus, forward all 
                                       keys to remote
     -grab-mouse                       Disable Grab mouse focus, forward all 
                                       events to remote
     /h: <height>                      Height
     -heartbeat                        Disable Support heartbeat PDUs
     +help                             Print help
     +home-drive                       Enable Redirect user home as share
     /ipv4[: [:force]]                 Prefer IPv4 A record over IPv6 AAAA 
                                       record
     /ipv6[: [:force]]                 Prefer IPv6 AAAA record over IPv4 A 
                                       record
     +jpeg                             JPEG codec support
     /jpeg-quality: <percentage>       JPEG quality
     /kbd: [layout:[0x<id>|<name>],lang:<0x<id>>,fn-key:<value>,type:<value>,
           subtype:<value>,unicode[:on|off],remap:<key1>=<value1>,
           remap:<key2>=<value2>,pipe:<filename>]
                                       Keyboard related options:
                                       
                                        * layout: set the keybouard layout 
                                       announced to the server
                                       
                                        * lang: set the keyboard language 
                                       identifier sent to the server
                                       
                                        * fn-key: Function key value
                                       
                                        * pipe: Name of a named pipe that can be 
                                       used to type text into the RDP session
                                       
                                       
     /kerberos: [kdc-url:<url>,lifetime:<time>,start-time:<time>,
                renewable-lifetime:<time>,cache:<path>,armor:<path>,
                pkinit-anchors:<path>,pkcs11-module:<name>]
                                       Kerberos options
     /list: [kbd|kbd-scancode|kbd-lang[:<value>]|smartcard[:[
            pkinit-anchors:<path>][,pkcs11-module:<name>]]
            |monitor|tune|timezones]   List available options for subcommand
     /load-balance-info: <info-string> Load balance info
     /log-filters: <tag>:<level>[,<tag>:<level>[,...]]
                                       Set logger filters, see wLog(7) for 
                                       details
     /log-level: [OFF|FATAL|ERROR|WARN|INFO|DEBUG|TRACE]
                                       Set the default log level, see wLog(7) 
                                       for details
     /max-fast-path-size: <size>       Specify maximum fast-path update size
     /max-loop-time: <time>            Specify maximum time in milliseconds 
                                       spend treating packets
     +menu-anims                       Enable menu animations
     /microphone[: [sys:<sys>,][dev:<dev>,][format:<format>,][rate:<rate>,][
                   channel:<channel>]] Audio input (microphone)
     /monitors: <id>[,<id>[,...]]      Select monitors to use (only effective in 
                                       fullscreen or multimonitor mode)
     /mouse: [relative:[on|off],grab:[on|off]]
                                       Mouse related options:
                                       
                                        * relative:   send relative mouse 
                                       movements if supported by server
                                       
                                        * grab:       grab the mouse if within 
                                       the window
     -mouse-motion                     Disable Send mouse motion
     +mouse-relative                   Enable Send mouse motion with relative 
                                       addressing
     /multimon[: force]                Use multiple monitors
     +multitouch                       Enable Redirect multitouch input
     -multitransport                   Disable Support multitransport protocol
     -nego                             Disable protocol security negotiation
     /network: [invalid|modem|broadband|broadband-low|broadband-high|wan|lan|auto]
                                       Network connection type
     +nsc                              NSCodec support
     +old-license                      Enable Use the old license workflow (no 
                                       CAL and hwId set to 0)
     /orientation: [0|90|180|270]      Orientation of display in degrees
     /p: <password>                    Password
     /parallel[: <name>[,<path>]]      Redirect parallel device
     /parent-window: <window-id>       Parent window id
     /pcb: <blob>                      Preconnection Blob
     /pcid: <id>                       Preconnection Id
     /pheight: <height>                Physical height of display (in 
                                       millimeters)
     /play-rfx: <pcap-file>            Replay rfx pcap file
     /port: <number>                   Server port
     /prevent-session-lock[: <time in sec>]
                                       Prevent session locking by injecting fake 
                                       mouse motion events to the server when 
                                       the connection is idle (default interval: 
                                       180 seconds)
     +print-reconnect-cookie           Enable Print base64 reconnect cookie 
                                       after connecting
     /printer[: <name>[,<driver>[,default]]]
                                       Redirect printer device
     /proxy: [<proto>://][<user>:<password>@]<host>[:<port>]
                                       Proxy settings: override env. var (see 
                                       also environment variable below). 
                                       Protocol "socks5" should be given 
                                       explicitly where "http" is default.
     /pth: <password-hash>             Pass the hash (restricted admin mode)
     /pwidth: <width>                  Physical width of display (in 
                                       millimeters)
     /rdp2tcp: <executable path[:arg...]>
                                       TCP redirection
     /reconnect-cookie: <base64-cookie> Pass base64 reconnect cookie to the 
                                       connection
     /redirect-prefer: <FQDN|IP|NETBIOS>,[...]
                                       Override the preferred redirection order
     +relax-order-checks               Do not check if a RDP order was announced 
                                       during capability exchange, only use when 
                                       connecting to a buggy server
     +remoteGuard                      Remote guard credentials
     +restricted-admin                 Restricted admin mode
     +rfx                              RemoteFX
     /rfx-mode: [image|video]          RemoteFX mode
     /scale: [100|140|180]             Scaling factor of the display
     /scale-desktop: <percentage>      Scaling factor for desktop applications 
                                       (value between 100 and 500)
     /scale-device: 100|140|180        Scaling factor for app store applications
     /sec: [rdp[:[on|off]]|tls[:[on|off]]|nla[:[on|off]]|ext[:[on|off]]|aad[:[
           on|off]]]                   Force specific protocol security. e.g. 
                                       /sec:nla enables NLA and disables all 
                                       others, while /sec:nla:[on|off] just 
                                       toggles NLA
     /serial[: <name>[,<path>[,<driver>[,permissive]]]]
                                       Redirect serial device
     /server-name: <name>              User-specified server name to use for 
                                       validation (TLS, Kerberos)
     /shell: <shell>                   Alternate shell
     /shell-dir: <dir>                 Shell working directory
     /size: <width>x<height> or <percent>%[wh]
                                       Screen size
     /smart-sizing[: <width>x<height>] Scale remote desktop to window size
     /smartcard[: <str>[,<str>...]]    Redirect the smartcard devices containing 
                                       any of the <str> in their names.
     /smartcard-logon[: [cert:<path>,key:<key>,pin:<pin>,csp:<csp name>,
                        reader:<reader>,card:<card>]]
                                       Activates Smartcard (optional 
                                       certificate) Logon authentication.
     /sound[: [sys:<sys>,][dev:<dev>,][format:<format>,][rate:<rate>,][
              channel:<channel>,][latency:<latency>,][quality:<quality>]]
                                       Audio output (sound)
     +span                             Span screen over multiple monitors
     /spn-class: <service-class>       SPN authentication service class
     +ssh-agent                        SSH Agent forwarding channel
     /sspi-module: <SSPI module path>  SSPI shared library module file path
     -suppress-output                  Disable suppress output when minimized
     /t: <title>                       Window title
     -themes                           Disable themes
     /timeout: <time in ms>            Advanced setting for high latency links: 
                                       Adjust connection timeout, use if you 
                                       encounter timeout failures with your 
                                       connection
     /timezone: <windows timezone>     Use supplied windows timezone for 
                                       connection (requires server support), see 
                                       /list:timezones for allowed values
     /tls: [ciphers|seclevel|secrets-file|enforce]
                                       TLS configuration options: * 
                                       ciphers:[netmon|ma|<cipher names>]
                                       
                                        * seclevel:<level>, default: 1, range: 
                                       [0-5] Override the default TLS security 
                                       level, might be required for older target 
                                       servers
                                       
                                        * secrets-file:<filename>
                                       
                                        * enforce[:[ssl3|1.0|1.1|1.2|1.3]] Force 
                                       use of SSL/TLS version for a connection. 
                                       Some servers have a buggy TLS version 
                                       negotiation and might fail without this. 
                                       Defaults to TLS 1.2 if no argument is 
                                       supplied. Use 1.0 for windows 7
     -toggle-fullscreen                Disable Alt+Ctrl+Enter to toggle 
                                       fullscreen
     /tune: <setting:value>,<setting:value>
                                       [experimental] directly manipulate 
                                       freerdp settings, use with extreme 
                                       caution!
     /u: [[<domain>\]<user>|<user>[@<domain>]]
                                       Username
     +unmap-buttons                    Enable Let server see real physical 
                                       pointer button
     /usb: [dbg,][id:<vid>:<pid>#...,][addr:<bus>:<addr>#...,][auto]
                                       Redirect USB device
     /v: <server>[:port]               Server hostname
     /vc: <channel>[,<options>]        Static virtual channel
     +version                          Print version
     +video                            Video optimized remoting channel
     /vmconnect[: <vmid>]              Hyper-V console (use port 2179, disable 
                                       negotiation)
     /w: <width>                       Width
     -wallpaper                        Disable wallpaper
     +window-drag                      Enable full window drag
     /window-position: <xpos>x<ypos>   window position
     /winscard-module: <WinSCard module path>
                                       WinSCard shared library module file path
     /wm-class: <class-name>           Set the WM_CLASS hint for the window 
                                       instance
     +workarea                         Use available work area
 
 Examples:
     xfreerdp3 connection.rdp /p:Pwd123! /f
     xfreerdp3 /u:CONTOSO\JohnDoe /p:Pwd123! /v:rdp.contoso.com
     xfreerdp3 /u:JohnDoe /p:Pwd123! /w:1366 /h:768 /v:192.168.1.100:4489
     xfreerdp3 /u:JohnDoe /p:Pwd123! /vmconnect:C824F53E-95D2-46C6-9A18-23A5BB403532 /v:192.168.1.100
     xfreerdp3 /u:\AzureAD\user@corp.example /p:pwd /v:host
 
 Clipboard Redirection: +clipboard
 
 Drive Redirection: /drive:home,/home/user
 Smartcard Redirection: /smartcard:<device>
 Smartcard logon with Kerberos authentication: /smartcard-logon /sec:nla
 Serial Port Redirection: /serial:<name>,<device>,[SerCx2|SerCx|Serial],[permissive]
 Serial Port Redirection: /serial:COM1,/dev/ttyS0
 Parallel Port Redirection: /parallel:<name>,<device>
 Printer Redirection: /printer:<device>,<driver>,[default]
 TCP redirection: /rdp2tcp:/usr/bin/rdp2tcp
 
 Audio Output Redirection: /sound:sys:oss,dev:1,format:1
 Audio Output Redirection: /sound:sys:alsa
 Audio Input Redirection: /microphone:sys:oss,dev:1,format:1
 Audio Input Redirection: /microphone:sys:alsa
 
 Multimedia Redirection: /video
 USB Device Redirection: /usb:id:054c:0268#4669:6e6b,addr:04:0c
 
 For Gateways, the https_proxy environment variable is respected:
     export https_proxy=http://proxy.contoso.com:3128/
     xfreerdp3 /g:rdp.contoso.com ...
 
 More documentation is coming, in the meantime consult source files
 
 Keyboard Shortcuts:
 	<Right CTRL>
 		releases keyboard and mouse grab
 	<CTRL>+<ALT>+<Return>
 		toggles fullscreen state of the application
 	<CTRL>+<ALT>+c
 		toggles remote control in a remote assistance session
 	<CTRL>+<ALT>+m
 		minimizes the application
 	Action Script
 		Executes a predefined script on key press.
 		Should the script not exist it is ignored.
 		Scripts can be provided at the default location ~/.config/freerdp/action.sh or as command line argument /action:script:<path>
 		The script will receive the current key combination as argument.
 		The output of the script is parsed for 'key-local' which tells that the script used the key combination, otherwise the combination is forwarded to the remote.
 ```
 
 - - -
 
 ### libfreerdp-client3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shared library for common client functionality.
 
 **Installed size:** `889 KB`  
 **How to install:** `sudo apt install libfreerdp-client3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libasound2t64 
 * libavcodec61 
 * libavutil59 
 * libc6 
 * libcups2t64 
 * libfreerdp3-3 
 * libfuse3-3 
 * libpulse0 
 * libswscale8 
 * libusb-1.0-0 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libfreerdp-server-proxy3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shared library for proxying RDP connections.
 
 **Installed size:** `279 KB`  
 **How to install:** `sudo apt install libfreerdp-server-proxy3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-client3-3 
 * libfreerdp3-3 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libfreerdp-server3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shared library with common server functionality.
 
 **Installed size:** `444 KB`  
 **How to install:** `sudo apt install libfreerdp-server3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp3-3 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libfreerdp-shadow-subsystem3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shadow subsystem libraries.
 
 **Installed size:** `190 KB`  
 **How to install:** `sudo apt install libfreerdp-shadow-subsystem3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-shadow3-3 
 * libfreerdp3-3 
 * libpam0g 
 * libwinpr3-3 
 * libx11-6
 * libxdamage1 
 * libxext6
 * libxfixes3
 * libxinerama1 
 * libxtst6 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libfreerdp-shadow3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shadow libraries.
 
 **Installed size:** `303 KB`  
 **How to install:** `sudo apt install libfreerdp-shadow3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libfreerdp-server3-3 
 * libfreerdp3-3 
 * libwinpr-tools3-3 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libfreerdp3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shared library with all core functionality.
 
 **Installed size:** `2.20 MB`  
 **How to install:** `sudo apt install libfreerdp3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libavcodec61 
 * libavutil59 
 * libc6 
 * libssl3t64 
 * libswresample5 
 * libswscale8 
 * libwinpr3-3 
 * libx11-6
 * libxkbfile1 
 * zlib1g 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libwinpr-tools3-3
 
  FreeRDP is a libre client/server implementation of the Remote
  Desktop Protocol (RDP).
   
  This package contains the shared library for Windows Portable Runtime
  utilities and tools.
 
 **Installed size:** `185 KB`  
 **How to install:** `sudo apt install libwinpr-tools3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libssl3t64 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libwinpr3-3
 
  WinPR is a spin-off project of FreeRDP which aims at providing a portable
  implementation of important portions of the Windows API. Just like FreeRDP,
  WinPR is released under the Apache license. Unlike Wine, WinPR does not provide
  binary compatibility, and does not require applications to be built for
  Windows. Instead, WinPR provides API compatibility for applications targeting
  non-Windows environments. When on Windows, the original native API is being
  used instead of the equivalent WinPR implementation, without having to modify
  the code using it.
   
  This package contains the WinPR shared library.
 
 **Installed size:** `1.40 MB`  
 **How to install:** `sudo apt install libwinpr3-3`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libcjson1 
 * libgcc-s1 
 * libicu72 
 * libk5crypto3 
 * libkrb5-3 
 * libssl3t64 
 * liburiparser1 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libwinpr3-dev
 
  WinPR is a spin-off project of FreeRDP which aims at providing a portable
  implementation of important portions of the Windows API. Just like FreeRDP,
  WinPR is released under the Apache license. Unlike Wine, WinPR does not provide
  binary compatibility, and does not require applications to be built for
  Windows. Instead, WinPR provides API compatibility for applications targeting
  non-Windows environments. When on Windows, the original native API is being
  used instead of the equivalent WinPR implementation, without having to modify
  the code using it.
   
  This package contains the WinPR development files.
 
 **Installed size:** `1.15 MB`  
 **How to install:** `sudo apt install libwinpr3-dev`  
 
 {{< spoiler "Dependencies:" >}}
 * libssl-dev
 * libwinpr-tools3-3 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 
 - - -
 
 ### winpr3-utils
 
  WinPR is a spin-off project of FreeRDP which aims at providing a portable
  implementation of important portions of the Windows API. Just like FreeRDP,
  WinPR is released under the Apache license. Unlike Wine, WinPR does not provide
  binary compatibility, and does not require applications to be built for
  Windows. Instead, WinPR provides API compatibility for applications targeting
  non-Windows environments. When on Windows, the original native API is being
  used instead of the equivalent WinPR implementation, without having to modify
  the code using it.
   
  This package contains WinPR command line utils (winpr-hash, winpr-makecert).
 
 **Installed size:** `182 KB`  
 **How to install:** `sudo apt install winpr3-utils`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libwinpr-tools3-3 
 * libwinpr3-3 
 {{< /spoiler >}}
 
 ##### winpr-hash3
 
 (unknown subject)
 
 ```
 root@kali:~# winpr-hash3 --help
 missing username or password
 
 winpr-hash: NTLM hashing tool
 Usage: winpr-hash -u <username> -p <password> [-d <domain>] [-f <_default_,sam>] [-v <_1_,2>]
 ```
 
 - - -
 
 ##### winpr-makecert3
 
 (unknown subject)
 
 ```
 root@kali:~# winpr-makecert3 -h
 Usage: winpr-makecert3 [options] [output file]
 
     -rdp                 	Unsupported - Generate certificate with required options for RDP usage.
     -silent              	Silently generate certificate without verbose output.
     -live                	Generate certificate live in memory when used as a library.
     -format <crt|pem|pfx>	Specify certificate file format
     -path <path>         	Specify certificate file output path
     -p <password>        	Specify certificate export password
     -n <name>            	Specifies the subject's certificate name. This name must conform to the X.500 standard. The simplest method is to specify the name in double quotes, preceded by CN=; for example, -n "CN=myName".
     -pe                  	Unsupported - Marks the generated private key as exportable. This allows the private key to be included in the certificate.
     -sk <keyname>        	Unsupported - Specifies the subject's key container location, which contains the private key. If a key container does not exist, it will be created.
     -sr <location>       	Unsupported - Specifies the subject's certificate store location. location can be either currentuser (the default) or localmachine.
     -ss <store>          	Unsupported - Specifies the subject's certificate store name that stores the output certificate.
     -# <number>          	Specifies a serial number from 1 to 2,147,483,647. The default is a unique value generated by Makecert.exe.
     -$ <authority>       	Unsupported - Specifies the signing authority of the certificate, which must be set to either commercial (for certificates used by commercial software publishers) or individual (for certificates used by individual software publishers).
     -a <algorithm>       	Specifies the signature algorithm. algorithm must be md5, sha1, sha256 (the default), sha384, or sha512.
     -b <mm/dd/yyyy>      	Unsupported - Specifies the start of the validity period. Defaults to the current date.
     -crl                 	Unsupported - Generates a certificate relocation list (CRL) instead of a certificate.
     -cy <certType>       	Unsupported - Specifies the certificate type. Valid values are end for end-entity and authority for certification authority.
     -e <mm/dd/yyyy>      	Unsupported - Specifies the end of the validity period. Defaults to 12/31/2039 11:59:59 GMT.
     -eku <oid[,oid…]>  	Unsupported - Inserts a list of comma-separated, enhanced key usage object identifiers (OIDs) into the certificate.
     -h <number>          	Unsupported - Specifies the maximum height of the tree below this certificate.
     -ic <file>           	Unsupported - Specifies the issuer's certificate file.
     -ik <keyName>        	Unsupported - Specifies the issuer's key container name.
     -iky <keyType>       	Unsupported - Specifies the issuer's key type, which must be one of the following: signature (which indicates that the key is used for a digital signature), exchange (which indicates that the key is used for key encryption and key exchange), or an integer that represents a provider type. By default, you can pass 1 for an exchange key or 2 for a signature key.
     -in <name>           	Unsupported - Specifies the issuer's certificate common name.
     -ip <provider>       	Unsupported - Specifies the issuer's CryptoAPI provider name. For information about the CryptoAPI provider name, see the –sp option.
     -ir <location>       	Unsupported - Specifies the location of the issuer's certificate store. location can be either currentuser (the default) or localmachine.
     -is <store>          	Unsupported - Specifies the issuer's certificate store name.
     -iv <pvkFile>        	Unsupported - Specifies the issuer's .pvk private key file.
     -iy <type>           	Unsupported - Specifies the issuer's CryptoAPI provider type. For information about the CryptoAPI provider type, see the –sy option.
     -l <link>            	Unsupported - Links to policy information (for example, to a URL).
     -len <number>        	Specifies the generated key length, in bits.
     -m <number>          	Specifies the duration, in months, of the certificate validity period.
     -y <number>          	Specifies the duration, in years, of the certificate validity period.
     -nscp                	Unsupported - Includes the Netscape client-authorization extension.
     -r                   	Unsupported - Creates a self-signed certificate.
     -sc <file>           	Unsupported - Specifies the subject's certificate file.
     -sky <keyType>       	Unsupported - Specifies the subject's key type, which must be one of the following: signature (which indicates that the key is used for a digital signature), exchange (which indicates that the key is used for key encryption and key exchange), or an integer that represents a provider type. By default, you can pass 1 for an exchange key or 2 for a signature key.
     -sp <provider>       	Unsupported - Specifies the subject's CryptoAPI provider name, which must be defined in the registry subkeys of HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography\Defaults\Provider. If both –sp and –sy are present, the type of the CryptoAPI provider must correspond to the Type value of the provider's subkey.
     -sv <pvkFile>        	Unsupported - Specifies the subject's .pvk private key file. The file is created if none exists.
     -sy <type>           	Unsupported - Specifies the subject's CryptoAPI provider type, which must be defined in the registry subkeys of HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography\Defaults\Provider Types. If both –sy and –sp are present, the name of the CryptoAPI provider must correspond to the Name value of the provider type subkey.
     -tbs <file>          	Unsupported - Specifies the certificate or CRL file to be signed.
     -?                   	print help
     -!                   	print extended help
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
