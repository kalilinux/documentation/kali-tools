---
Title: sickle-tool
Homepage: https://github.com/wetw0rk/Sickle
Repository: https://gitlab.com/kalilinux/packages/sickle-tool
Architectures: all
Version: 3.1.0-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sickle-tool
 
  Sickle is a payload development tool originally created to aid in crafting
  shellcode, however it can be used in crafting payloads for other exploit
  types as well (non-binary). Although the current modules are mostly aimed
  towards assembly this tool is not limited to shellcode.
 
 **Installed size:** `223 KB`  
 **How to install:** `sudo apt install sickle-tool`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-capstone
 * python3-keystone-engine
 {{< /spoiler >}}
 
 ##### sickle-tool
 
 
 ```
 root@kali:~# sickle-tool -h
 usage: sickle-tool [-h] [-r READ] [-p PAYLOAD] [-f FORMAT] [-m MODULE]
                    [-a ARCH] [-b BADCHARS] [-v VARNAME] [-i] [-l]
 
 Sickle - Payload development framework
 
 options:
   -h, --help               Show this help message and exit
   -r, --read READ          Read bytes from binary file (use - for stdin)
   -p, --payload PAYLOAD    Shellcode to use
   -f, --format FORMAT      Output format (--list for more info)
   -m, --module MODULE      Development module
   -a, --arch ARCH          Select architecture for disassembly
   -b, --badchars BADCHARS  Bad characters to avoid in shellcode
   -v, --varname VARNAME    Alternative variable name
   -i, --info               Print detailed info for module or payload
   -l, --list               List available formats, payloads, or modules
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
