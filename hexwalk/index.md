---
Title: hexwalk
Homepage: https://www.hexwalk.com
Repository: https://salsa.debian.org/gcarmix/hexwalk
Architectures: any
Version: 1.7.1-1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### hexwalk
 
  HexWalk is an Hex editor, viewer, analyzer based on opensource
  projects like qhexedit2, binwalk and QT
  It is cross platform and has plenty of features:
   - Advanced find patterns in binary files based on HEX, UTF8, UTF16 and regex
   - Binwalk integration
   - Entropy Analysis
   - Byte Map
   - Hash Calculator
   - Bin/Dec/Hex Converter
   - Hex file editing
   - Diff file analysis
   - Byte Patterns to parse headers
 
 **Installed size:** `936 KB`  
 **How to install:** `sudo apt install hexwalk`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libgcc-s1 
 * libqt5charts5 
 * libqt5core5t64 
 * libqt5gui5t64  | libqt5gui5-gles 
 * libqt5widgets5t64 
 * libstdc++6 
 {{< /spoiler >}}
 
 ##### hexwalk
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
