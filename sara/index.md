---
Title: sara
Homepage: https://github.com/casterbyte/sara
Repository: https://gitlab.com/kalilinux/packages/sara
Architectures: all
Version: 1.0-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sara
 
  This package contains an autonomous RouterOS configuration analyzer for
  finding security issues on MikroTik hardware.
 
 **Installed size:** `52 KB`  
 **How to install:** `sudo apt install sara`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-colorama
 {{< /spoiler >}}
 
 ##### sara
 
 
 ```
 root@kali:~# sara -h
 
     _____                 
    / ____|                
   | (___   __ _ _ __ __ _ 
    \___ \ / _` | '__/ _` |
    ____) | (_| | | | (_| |
   |_____/ \__,_|_|  \__,_|  v1.0
 
     RouterOS Security Inspector. Designed for security professionals
 
     Author: Magama Bazarov, <caster@exploit.org>
 
     It's recommended to provide a configuration file exported using the 'export verbose' command
 
 usage: sara [-h] --config-file CONFIG_FILE
 
 options:
   -h, --help            show this help message and exit
   --config-file CONFIG_FILE
                         Path to the RouterOS configuration file (or .rsc)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
