---
Title: naabu
Homepage: https://github.com/projectdiscovery/naabu
Repository: https://gitlab.com/kalilinux/packages/naabu
Architectures: amd64 arm64 i386
Version: 2.3.1-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### naabu
 
  This package contains a port scanning tool written in Go that allows you to
  enumerate valid ports for hosts in a fast and reliable manner. It is a really
  simple tool that does fast SYN/CONNECT scans on the host/list of hosts and
  lists all ports that return a reply.
  Main features are:
    * Fast And Simple SYN/CONNECT probe based scanning.
    * Optimized for ease of use and lightweight on resources
    * Automatic handling of duplicate hosts between multiple subdomains
    * NMAP Integration for service discovery
    * Piped input / output support for integrating in workflows
    * Multiple Output formats supported (JSON, File, Stdout)
    * Multiple input support including HOST/IP/CIDR notation
 
 **Installed size:** `60.32 MB`  
 **How to install:** `sudo apt install naabu`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libpcap0.8t64 
 {{< /spoiler >}}
 
 ##### functional-test
 
 
 ```
 root@kali:~# functional-test -h
 Usage of functional-test:
   -dev string
     	Dev Branch Naabu Binary
   -main string
     	Main Branch Naabu Binary
   -testcases string
     	Test cases file for Naabu functional tests
 ```
 
 - - -
 
 ##### integration-test
 
 
 ```
 root@kali:~# integration-test -h
 Running test cases for "library"
 scanme.sh:80
 [✓] Test "sdk - one passive execution" passed!
 scanme.sh:80
 [✓] Test "sdk - one execution - connect" passed!
 ```
 
 - - -
 
 ##### naabu
 
 
 ```
 root@kali:~# naabu -h
 Naabu is a port scanning tool written in Go that allows you to enumerate open ports for hosts in a fast and reliable manner.
 
 Usage:
   naabu [flags]
 
 Flags:
 INPUT:
    -host string[]              hosts to scan ports for (comma-separated)
    -list, -l string            list of hosts to scan ports (file)
    -exclude-hosts, -eh string  hosts to exclude from the scan (comma-separated)
    -exclude-file, -ef string   list of hosts to exclude from scan (file)
 
 PORT:
    -port, -p string            ports to scan (80,443, 100-200)
    -top-ports, -tp string      top ports to scan (default 100) [full,100,1000]
    -exclude-ports, -ep string  ports to exclude from scan (comma-separated)
    -ports-file, -pf string     list of ports to scan (file)
    -port-threshold, -pts int   port threshold to skip port scan for the host
    -exclude-cdn, -ec           skip full port scans for CDN/WAF (only scan for port 80,443)
    -display-cdn, -cdn          display cdn in use
 
 RATE-LIMIT:
    -c int     general internal worker threads (default 25)
    -rate int  packets to send per second (default 1000)
 
 UPDATE:
    -up, -update                 update naabu to latest version
    -duc, -disable-update-check  disable automatic naabu update check
 
 OUTPUT:
    -o, -output string  file to write output to (optional)
    -j, -json           write output in JSON lines format
    -csv                write output in csv format
 
 CONFIGURATION:
    -config string                   path to the naabu configuration file (default $HOME/.config/naabu/config.yaml)
    -scan-all-ips, -sa               scan all the IP's associated with DNS record
    -ip-version, -iv string[]        ip version to scan of hostname (4,6) - (default 4) (default ["4"])
    -scan-type, -s string            type of port scan (SYN/CONNECT) (default "s")
    -source-ip string                source ip and port (x.x.x.x:yyy - might not work on OSX) 
    -interface-list, -il             list available interfaces and public ip
    -interface, -i string            network Interface to use for port scan
    -nmap                            invoke nmap scan on targets (nmap must be installed) - Deprecated
    -nmap-cli string                 nmap command to run on found results (example: -nmap-cli 'nmap -sV')
    -r string                        list of custom resolver dns resolution (comma separated or from file)
    -proxy string                    socks5 proxy (ip[:port] / fqdn[:port]
    -proxy-auth string               socks5 proxy authentication (username:password)
    -resume                          resume scan using resume.cfg
    -stream                          stream mode (disables resume, nmap, verify, retries, shuffling, etc)
    -passive                         display passive open ports using shodan internetdb api
    -irt, -input-read-timeout value  timeout on input read (default 3m0s)
    -no-stdin                        Disable Stdin processing
 
 HOST-DISCOVERY:
    -sn, -host-discovery           Perform Only Host Discovery
    -Pn, -skip-host-discovery      Skip Host discovery
    -ps, -probe-tcp-syn string[]   TCP SYN Ping (host discovery needs to be enabled)
    -pa, -probe-tcp-ack string[]   TCP ACK Ping (host discovery needs to be enabled)
    -pe, -probe-icmp-echo          ICMP echo request Ping (host discovery needs to be enabled)
    -pp, -probe-icmp-timestamp     ICMP timestamp request Ping (host discovery needs to be enabled)
    -pm, -probe-icmp-address-mask  ICMP address mask request Ping (host discovery needs to be enabled)
    -arp, -arp-ping                ARP ping (host discovery needs to be enabled)
    -nd, -nd-ping                  IPv6 Neighbor Discovery (host discovery needs to be enabled)
    -rev-ptr                       Reverse PTR lookup for input ips
 
 SERVICES-DISCOVERY:
    -sD, -service-discovery  Service Discovery
    -sV, -service-version    Service Version
 
 OPTIMIZATION:
    -retries int       number of retries for the port scan (default 3)
    -timeout int       millisecond to wait before timing out (default 1000)
    -warm-up-time int  time in seconds between scan phases (default 2)
    -ping              ping probes for verification of host
    -verify            validate the ports again with TCP verification
 
 DEBUG:
    -health-check, -hc        run diagnostic check up
    -debug                    display debugging information
    -verbose, -v              display verbose output
    -no-color, -nc            disable colors in CLI output
    -silent                   display only results in output
    -version                  display version of naabu
    -stats                    display stats of the running scan (deprecated)
    -si, -stats-interval int  number of seconds to wait between showing a statistics update (deprecated) (default 5)
    -mp, -metrics-port int    port to expose naabu metrics on (default 63636)
 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
