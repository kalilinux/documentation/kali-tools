---
Title: sslh
Homepage: http://www.rutschle.net/tech/sslh/README.html
Repository: https://salsa.debian.org/debian/sslh.git
Architectures: any
Version: 2.1.4-1
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-information-gathering kali-tools-post-exploitation kali-tools-web 
Icon: images/sslh-logo.svg
PackagesInfo: |
 ### sslh
 
  sslh lets one accept HTTPS, SSH, OpenVPN, tinc and XMPP connections on the
  same port. This makes it possible to connect to any of these servers on
  port 443 (e.g. from inside a corporate firewall, which almost never block
  port 443) while still serving HTTPS on that port.
 
 **Installed size:** `513 KB`  
 **How to install:** `sudo apt install sslh`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * debconf 
 * init-system-helpers 
 * libc6 
 * libcap2 
 * libconfig11 
 * libev4t64 
 * libpcre2-8-0 
 * libsystemd0
 * libwrap0 
 * update-inetd
 {{< /spoiler >}}
 
 ##### sslh
 
 Protocol demultiplexer
 
 ```
 root@kali:~# sslh -h
 sslhcfg: invalid option "-h"
  [-Vfin] [-F <file>] [--verbose-config=<n>] [--verbose-config-error=<n>] [--verbose-connections=<n>] [--verbose-connections-try=<n>] [--verbose-connections-error=<n>] [--verbose-fd=<n>] [--verbose-packets=<n>] [--verbose-probe-info=<n>] [--verbose-probe-error=<n>] [--verbose-system-error=<n>] [--verbose-int-error=<n>] [--transparent] [-t <n>] [--udp-max-connections=<n>] [-u <str>] [-P <file>] [-C <path>] [--syslog-facility=<str>] [--logfile=<str>] [--on-timeout=<str>] [--prefix=<str>] [-p <host:port>]... [--ssh=<host:port>]... [--tls=<host:port>]... [--ssl=<host:port>]... [--openvpn=<host:port>]... [--tinc=<host:port>]... [--wireguard=<host:port>]... [--xmpp=<host:port>]... [--http=<host:port>]... [--adb=<host:port>]... [--socks5=<host:port>]... [--syslog=<host:port>]... [--msrdp=<host:port>]... [--anyprot=<host:port>]...
   -F, --config=<file>      	Specify configuration file
   --verbose-config=<n>     	Print configuration at startup
   --verbose-config-error=<n>	Print configuration errors
   --verbose-connections=<n>	Trace established incoming address to forward address
   --verbose-connections-try=<n>	Connection errors
   --verbose-connections-error=<n>	Connection attempts towards targets
   --verbose-fd=<n>         	File descriptor activity, open/close/whatnot
   --verbose-packets=<n>    	Hexdump packets on which probing is done
   --verbose-probe-info=<n> 	Trace the probe process
   --verbose-probe-error=<n>	Failures and problems during probing
   --verbose-system-error=<n>	System call failures
   --verbose-int-error=<n>  	Internal errors that should never happen
   -V, --version            	Print version information and exit
   -f, --foreground         	Run in foreground instead of as a daemon
   -i, --inetd              	Run in inetd mode: use stdin/stdout instead of network listen
   -n, --numeric            	Print IP addresses and ports as numbers
   --transparent            	Set up as a transparent proxy
   -t, --timeout=<n>        	Set up timeout before connecting to default target
   --udp-max-connections=<n>	Number of concurrent UDP connections
   -u, --user=<str>         	Username to change to after set-up
   -P, --pidfile=<file>     	Path to file to store PID of current instance
   -C, --chroot=<path>      	Root to change to after set-up
   --syslog-facility=<str>  	Facility to syslog to
   --logfile=<str>          	Log messages to a file
   --on-timeout=<str>       	Target to connect to when timing out
   --prefix=<str>           	Reserved for testing
   -p, --listen=<host:port> 	Listen on host:port
   --ssh=<host:port>        	Set up ssh target
   --tls=<host:port>        	Set up TLS/SSL target
   --ssl=<host:port>        	Set up TLS/SSL target
   --openvpn=<host:port>    	Set up OpenVPN target
   --tinc=<host:port>       	Set up tinc target
   --wireguard=<host:port>  	Set up WireGuard target
   --xmpp=<host:port>       	Set up XMPP target
   --http=<host:port>       	Set up HTTP (plain) target
   --adb=<host:port>        	Set up ADB (Android Debug) target
   --socks5=<host:port>     	Set up socks5 target
   --syslog=<host:port>     	Set up syslog target
   --msrdp=<host:port>      	Set up msrdp target
   --anyprot=<host:port>    	Set up default target
 ```
 
 - - -
 
 ##### sslh-ev
 
 Protocol demultiplexer
 
 ```
 root@kali:~# sslh-ev -h
 sslhcfg: invalid option "-h"
  [-Vfin] [-F <file>] [--verbose-config=<n>] [--verbose-config-error=<n>] [--verbose-connections=<n>] [--verbose-connections-try=<n>] [--verbose-connections-error=<n>] [--verbose-fd=<n>] [--verbose-packets=<n>] [--verbose-probe-info=<n>] [--verbose-probe-error=<n>] [--verbose-system-error=<n>] [--verbose-int-error=<n>] [--transparent] [-t <n>] [--udp-max-connections=<n>] [-u <str>] [-P <file>] [-C <path>] [--syslog-facility=<str>] [--logfile=<str>] [--on-timeout=<str>] [--prefix=<str>] [-p <host:port>]... [--ssh=<host:port>]... [--tls=<host:port>]... [--ssl=<host:port>]... [--openvpn=<host:port>]... [--tinc=<host:port>]... [--wireguard=<host:port>]... [--xmpp=<host:port>]... [--http=<host:port>]... [--adb=<host:port>]... [--socks5=<host:port>]... [--syslog=<host:port>]... [--msrdp=<host:port>]... [--anyprot=<host:port>]...
   -F, --config=<file>      	Specify configuration file
   --verbose-config=<n>     	Print configuration at startup
   --verbose-config-error=<n>	Print configuration errors
   --verbose-connections=<n>	Trace established incoming address to forward address
   --verbose-connections-try=<n>	Connection errors
   --verbose-connections-error=<n>	Connection attempts towards targets
   --verbose-fd=<n>         	File descriptor activity, open/close/whatnot
   --verbose-packets=<n>    	Hexdump packets on which probing is done
   --verbose-probe-info=<n> 	Trace the probe process
   --verbose-probe-error=<n>	Failures and problems during probing
   --verbose-system-error=<n>	System call failures
   --verbose-int-error=<n>  	Internal errors that should never happen
   -V, --version            	Print version information and exit
   -f, --foreground         	Run in foreground instead of as a daemon
   -i, --inetd              	Run in inetd mode: use stdin/stdout instead of network listen
   -n, --numeric            	Print IP addresses and ports as numbers
   --transparent            	Set up as a transparent proxy
   -t, --timeout=<n>        	Set up timeout before connecting to default target
   --udp-max-connections=<n>	Number of concurrent UDP connections
   -u, --user=<str>         	Username to change to after set-up
   -P, --pidfile=<file>     	Path to file to store PID of current instance
   -C, --chroot=<path>      	Root to change to after set-up
   --syslog-facility=<str>  	Facility to syslog to
   --logfile=<str>          	Log messages to a file
   --on-timeout=<str>       	Target to connect to when timing out
   --prefix=<str>           	Reserved for testing
   -p, --listen=<host:port> 	Listen on host:port
   --ssh=<host:port>        	Set up ssh target
   --tls=<host:port>        	Set up TLS/SSL target
   --ssl=<host:port>        	Set up TLS/SSL target
   --openvpn=<host:port>    	Set up OpenVPN target
   --tinc=<host:port>       	Set up tinc target
   --wireguard=<host:port>  	Set up WireGuard target
   --xmpp=<host:port>       	Set up XMPP target
   --http=<host:port>       	Set up HTTP (plain) target
   --adb=<host:port>        	Set up ADB (Android Debug) target
   --socks5=<host:port>     	Set up socks5 target
   --syslog=<host:port>     	Set up syslog target
   --msrdp=<host:port>      	Set up msrdp target
   --anyprot=<host:port>    	Set up default target
 ```
 
 - - -
 
 ##### sslh-select
 
 Protocol demultiplexer
 
 ```
 root@kali:~# sslh-select -h
 sslhcfg: invalid option "-h"
  [-Vfin] [-F <file>] [--verbose-config=<n>] [--verbose-config-error=<n>] [--verbose-connections=<n>] [--verbose-connections-try=<n>] [--verbose-connections-error=<n>] [--verbose-fd=<n>] [--verbose-packets=<n>] [--verbose-probe-info=<n>] [--verbose-probe-error=<n>] [--verbose-system-error=<n>] [--verbose-int-error=<n>] [--transparent] [-t <n>] [--udp-max-connections=<n>] [-u <str>] [-P <file>] [-C <path>] [--syslog-facility=<str>] [--logfile=<str>] [--on-timeout=<str>] [--prefix=<str>] [-p <host:port>]... [--ssh=<host:port>]... [--tls=<host:port>]... [--ssl=<host:port>]... [--openvpn=<host:port>]... [--tinc=<host:port>]... [--wireguard=<host:port>]... [--xmpp=<host:port>]... [--http=<host:port>]... [--adb=<host:port>]... [--socks5=<host:port>]... [--syslog=<host:port>]... [--msrdp=<host:port>]... [--anyprot=<host:port>]...
   -F, --config=<file>      	Specify configuration file
   --verbose-config=<n>     	Print configuration at startup
   --verbose-config-error=<n>	Print configuration errors
   --verbose-connections=<n>	Trace established incoming address to forward address
   --verbose-connections-try=<n>	Connection errors
   --verbose-connections-error=<n>	Connection attempts towards targets
   --verbose-fd=<n>         	File descriptor activity, open/close/whatnot
   --verbose-packets=<n>    	Hexdump packets on which probing is done
   --verbose-probe-info=<n> 	Trace the probe process
   --verbose-probe-error=<n>	Failures and problems during probing
   --verbose-system-error=<n>	System call failures
   --verbose-int-error=<n>  	Internal errors that should never happen
   -V, --version            	Print version information and exit
   -f, --foreground         	Run in foreground instead of as a daemon
   -i, --inetd              	Run in inetd mode: use stdin/stdout instead of network listen
   -n, --numeric            	Print IP addresses and ports as numbers
   --transparent            	Set up as a transparent proxy
   -t, --timeout=<n>        	Set up timeout before connecting to default target
   --udp-max-connections=<n>	Number of concurrent UDP connections
   -u, --user=<str>         	Username to change to after set-up
   -P, --pidfile=<file>     	Path to file to store PID of current instance
   -C, --chroot=<path>      	Root to change to after set-up
   --syslog-facility=<str>  	Facility to syslog to
   --logfile=<str>          	Log messages to a file
   --on-timeout=<str>       	Target to connect to when timing out
   --prefix=<str>           	Reserved for testing
   -p, --listen=<host:port> 	Listen on host:port
   --ssh=<host:port>        	Set up ssh target
   --tls=<host:port>        	Set up TLS/SSL target
   --ssl=<host:port>        	Set up TLS/SSL target
   --openvpn=<host:port>    	Set up OpenVPN target
   --tinc=<host:port>       	Set up tinc target
   --wireguard=<host:port>  	Set up WireGuard target
   --xmpp=<host:port>       	Set up XMPP target
   --http=<host:port>       	Set up HTTP (plain) target
   --adb=<host:port>        	Set up ADB (Android Debug) target
   --socks5=<host:port>     	Set up socks5 target
   --syslog=<host:port>     	Set up syslog target
   --msrdp=<host:port>      	Set up msrdp target
   --anyprot=<host:port>    	Set up default target
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
