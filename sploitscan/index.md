---
Title: sploitscan
Homepage: https://github.com/xaitax/SploitScan
Repository: https://salsa.debian.org/pkg-security-team/sploitscan
Architectures: all
Version: 0.10.5-1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sploitscan
 
  SploitScan is an efficient and easy-to-use command-line tool designed to
  consult CVE (Common Vulnerabilities and Exposures).
  Extremely important for professionals, as it allows them to implement
  measures that prevent the exploitation of discovered vulnerabilities.
   
  Tool is capable of exporting in a single run results for JSON and CSV
  formats, from the main databases of entities and organizations linked
  to cybersecurity:
     - MITER Corporation. non-profit organization that provides technical
       solutions to critical security issue
     - EPSS (Exploit Prediction Scoring System) is a system that aims to
       predict the probability of a specific vulnerability, helping organizations
       prioritize their mitigation activities.
     - Gather PoCs (Proof of Concepts) Collecting PoCs is a common activity
       among vulnerability researchers and security professionals, who reproduce
       the severity of vulnerabilities in a controlled environment, aiming to
       take preventive measures.
     - CISA Infrastructure Security Agency - responsible for keeping Known
       Exploited Vulnerabilities Catalog (KEV) updated.
     - Capable of interacting with the Patch Priority System, responsible for
       evaluating and assigning a patch priority rating based on several factors,
       including the availability of public exploits.
 
 **Installed size:** `160 KB`  
 **How to install:** `sudo apt install sploitscan`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-jinja2
 * python3-openai
 * python3-requests
 * python3-tabulate
 {{< /spoiler >}}
 
 ##### sploitscan
 
 A tool to fetch and display vulnerability information and public exploits for given CVE IDs.
 
 ```
 root@kali:~# sploitscan -h
 
 ███████╗██████╗ ██╗      ██████╗ ██╗████████╗███████╗ ██████╗ █████╗ ███╗   ██╗
 ██╔════╝██╔══██╗██║     ██╔═══██╗██║╚══██╔══╝██╔════╝██╔════╝██╔══██╗████╗  ██║
 ███████╗██████╔╝██║     ██║   ██║██║   ██║   ███████╗██║     ███████║██╔██╗ ██║
 ╚════██║██╔═══╝ ██║     ██║   ██║██║   ██║   ╚════██║██║     ██╔══██║██║╚██╗██║
 ███████║██║     ███████╗╚██████╔╝██║   ██║   ███████║╚██████╗██║  ██║██║ ╚████║
 ╚══════╝╚═╝     ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═══╝
 v0.10.5 / Alexander Hagenah / @xaitax / ah@primepage.de
 
 usage: sploitscan [-h] [-e {json,JSON,csv,CSV,html,HTML}]
                   [-t {nessus,nexpose,openvas,docker}] [-i IMPORT_FILE]
                   [-c CONFIG] [-d]
                   [cve_ids ...]
 
 SploitScan: Retrieve and display vulnerability data as well as public exploits
 for given CVE ID(s).
 
 positional arguments:
   cve_ids               Enter one or more CVE IDs to fetch data. Separate
                         multiple CVE IDs with spaces. Format for each ID: CVE-
                         YYYY-NNNNN. This argument is optional if an import
                         file is provided using the -i option.
 
 options:
   -h, --help            show this help message and exit
   -e, --export {json,JSON,csv,CSV,html,HTML}
                         Optional: Export the results to a JSON, CSV, or HTML
                         file. Specify the format: 'json', 'csv', or 'html'.
   -t, --type {nessus,nexpose,openvas,docker}
                         Specify the type of the import file: 'nessus',
                         'nexpose', 'openvas' or 'docker'.
   -i, --import-file IMPORT_FILE
                         Path to an import file from a vulnerability scanner.
                         If used, CVE IDs can be omitted from the command line
                         arguments.
   -c, --config CONFIG   Path to a custom config file.
   -d, --debug           Enable debug output.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
