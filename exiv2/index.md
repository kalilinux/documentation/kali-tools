---
Title: exiv2
Homepage: https://www.exiv2.org/
Repository: https://salsa.debian.org/qt-kde-team/3rdparty/exiv2
Architectures: any all
Version: 0.28.4+dfsg-2
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-forensics kali-tools-respond 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### exiv2
 
  Exiv2 is a C++ library and a command line utility to manage image metadata.
  It provides fast and easy read and write access to the Exif, IPTC and XMP
  metadata of images in various formats
   
  Exiv2 command line utility to:
   
   * print Exif, IPTC and XMP image metadata in different formats:
     - Exif summary info, interpreted values, or the plain data for each tag
   * set, add and delete Exif, IPTC and XMP image metadata from command line
     modify commands or command scripts
   * adjust the Exif timestamp (that's how it all started...)
   * rename Exif image files according to the Exif timestamp
   * extract, insert and delete Exif, IPTC and XMP metadata and JPEG comments
   * extract previews from RAW images and thumbnails from the Exif metadata
   * insert and delete the thumbnail image embedded in the Exif metadata
   * print, set and delete the JPEG comment of JPEG images
   * fix the Exif ISO setting of picture taken with Canon and Nikon cameras
 
 **Installed size:** `400 KB`  
 **How to install:** `sudo apt install exiv2`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libexiv2-28 
 * libgcc-s1 
 * libstdc++6 
 {{< /spoiler >}}
 
 ##### exiv2
 
 
 ```
 root@kali:~# exiv2 -h
 Usage: exiv2 [ option [ arg ] ]+ [ action ] file ...
 
 Image metadata manipulation tool.
 
 Where file is one or more files, optionally containing a URL
 (http, https, ftp, sftp, data or file) or wildcard
 
 Actions:
   pr | print    Print image metadata (default is a summary). This is the default
                 action
   ad | adjust   Adjust Exif timestamps by the given time. Requires
                 at least one of -a, -Y, -O or -D
   rm | delete   Deletes image metadata, use -d to choose type to delete
                 (default is all)
   in | insert   Insert metadata from .exv, .xmp, thumbnail or .icc file.
                 Use option -S to change the suffix of the input files and
                 -l to change the location
   ex | extract  Extract metadata to .exv, .xmp, preview image, thumbnail,
                 or ICC profile. Use option -S to change the suffix of the input
                 files and -l to change the location
   mv | rename   Rename files and/or set file timestamps according to the
                 Exif timestamps. The filename format can be set with
                 -r format, timestamp options are controlled with -t and -T
   mo | modify   Apply commands to modify the Exif, IPTC and XMP metadata.
                 Requires option -m or -M
   fi | fixiso   Copy ISO setting from Canon and Nikon makernotes, to the
                 standard Exif tag
   fc | fixcom   Convert the Unicode Exif user comment to UCS-2. The current
                 character encoding can be specified with the -n option
 
 Options:
    -h      Display this help and exit
    -V      Show the program version and exit
    -v      Be verbose during the program run
    -q      Silence warnings and error messages (quiet)
    -Q lvl  Set log-level to d(ebug), i(nfo), w(arning), e(rror) or m(ute)
    -b      Obsolete, reserved for use with the test suit
    -u      Show unknown tags (e.g., Exif.SonyMisc3c.0x022b)
    -g str  Only output where 'str' matches in output text (grep)
            Append /i to 'str' for case insensitive
    -K key  Only output where 'key' exactly matches tag's key
    -n enc  Character set to decode Exif Unicode user comments
    -k      Preserve file timestamps when updating files (keep)
    -t      Set the file timestamp from Exif metadata when renaming (overrides -k)
    -T      Only set the file timestamp from Exif metadata ('rename' action)
    -f      Do not prompt before overwriting existing files (force)
    -F      Do not prompt before renaming files (Force)
    -a time Time adjustment in the format [+|-]HH[:MM[:SS]]. For 'adjust' action
    -Y yrs  Year adjustment with the 'adjust' action
    -O mon  Month adjustment with the 'adjust' action
    -D day  Day adjustment with the 'adjust' action
    -p mode Print mode for the 'print' action. Possible modes are:
              s : A summary of the Exif metadata (the default)
              a : Exif, IPTC and XMP tags (shortcut for -Pkyct)
              e : Exif tags (shortcut for -PEkycv)
              t : Interpreted (translated) Exif tags (-PEkyct)
              v : Plain (untranslated) Exif tags values (-PExgnycv)
              h : Hex dump of the Exif tags (-PExgnycsh)
              i : IPTC tags (-PIkyct)
              x : XMP tags (-PXkyct)
              c : JPEG comment
              p : List available image preview, sorted by size
              C : Print ICC profile
              R : Recursive print structure of image (debug build only)
              S : Print structure of image (limited file types)
              X : Extract "raw" XMP
    -P flgs Print flags for fine control of tag lists ('print' action):
              E : Exif tags
              I : IPTC tags
              X : XMP tags
              x : Tag number for Exif or IPTC tags (in hexadecimal)
              g : Group name (e.g. Exif.Photo.UserComment, Photo)
              k : Key (e.g. Exif.Photo.UserComment)
              l : Tag label (e.g. Exif.Photo.UserComment, 'User comment')
              d : Tag description
              n : Tag name (e.g. Exif.Photo.UserComment, UserComment)
              y : Type
              y : Type
              c : Number of components (count)
              s : Size in bytes of vanilla value (may include NULL)
              v : Plain data value of untranslated (vanilla)
              V : Plain data value, data type and the word 'set'
              t : Interpreted (translated) human readable values
              h : Hex dump of the data
    -d tgt1  Delete target(s) for the 'delete' action. Possible targets are:
              a : All supported metadata (the default)
              e : Exif tags
              t : Exif thumbnail only
              i : IPTC tags
              x : XMP tags
              c : JPEG comment
              C : ICC Profile
              c : All IPTC data (any broken multiple IPTC blocks)
              - : Input from stdin
    -i tgt2 Insert target(s) for the 'insert' action. Possible targets are
              a : All supported metadata (the default)
              e : Exif tags
              t : Exif thumbnail only (JPEGs only from <file>-thumb.jpg)
              i : IPTC tags
              x : XMP tags
              c : JPEG comment
              C : ICC Profile, from <file>.icc
              X : XMP sidecar from file <file>.xmp
              XX: "raw" metadata from <file>.exv. XMP default, optional Exif and IPTC
              - : Input from stdin
    -e tgt3 Extract target(s) for the 'extract' action. Possible targets
              a : All supported metadata (the default)
              e : Exif tags
              t : Exif thumbnail only (to <file>-thumb.jpg)
              i : IPTC tags
              x : XMP tags
              c : JPEG comment
              pN: Extract N'th preview image to <file>-preview<N>.<ext>
              C : ICC Profile, to <file>.icc
              X : XMP sidecar to <file>.xmp
              XX: "raw" metadata to <file>.exv. XMP default, optional Exif and IPTC
              - : Output to stdin
    -r fmt  Filename format for the 'rename' action. The format string
            follows strftime(3). The following keywords are also supported:
              :basename:   - original filename without extension
              :dirname:    - name of the directory holding the original file
              :parentname: - name of parent directory
            Default 'fmt' is %Y%m%d_%H%M%S
    -c txt  JPEG comment string to set in the image.
    -m cmdf Applies commands in 'cmdf' file, for the modify action (see -M for format).
    -M cmd  Command line for the modify action. The format is:
            ( (set | add) <key> [[<type>] <value>] |
              del <key> [<type>] |
              reg prefix namespace )
    -l dir  Location (directory) for files to be inserted from or extracted to.
    -S suf Use suffix 'suf' for source files for insert action.
 
 Examples:
    exiv2 -pe image.dng *.jp2
            Print all Exif tags in image.dng and all .jp2 files
    exiv2 -g date/i https://clanmills.com/Stonehenge.jpg
            Print all tags in file, where key contains 'date' (case insensitive)
    exiv2 -M"set Xmp.dc.subject XmpBag Sky" image.tiff
            Set (or add if missing) value to tag in file
 
 ```
 
 - - -
 
 ### libexiv2-28
 
  Exiv2 is a C++ library and a command line utility to manage image metadata.
  It provides fast and easy read and write access to the Exif, IPTC and XMP
  metadata of images in various formats
   
  Exiv2 library provides:
   * fast read and write access to the Exif, IPTC, and XMP metadata of an image
   * an easy to use and extensively documented API
   * conversions of Exif and IPTC metadata to XMP and vice versa
   * a smart IPTC implementation that does not affect data that programs like
     Photoshop store in the same image segment
   * Exif Makernote support:
     - Makernote tags can be read and written just like any other metadata
     - a sophisticated write algorithm avoids corrupting the Makernote
   * a simple interface to extract previews embedded in RAW images and Exif
     thumbnails
   * set and delete methods for Exif thumbnails
 
 **Installed size:** `3.58 MB`  
 **How to install:** `sudo apt install libexiv2-28`  
 
 {{< spoiler "Dependencies:" >}}
 * libbrotli1 
 * libc6 
 * libexiv2-data 
 * libexpat1 
 * libgcc-s1 
 * libinireader0 
 * libstdc++6 
 * zlib1g 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libexiv2-data
 
  Exiv2 is a C++ library and a command line utility to manage image metadata.
  It provides fast and easy read and write access to the Exif, IPTC and XMP
  metadata of images in various formats
   
  This package provides the data files of the exiv2 library.
 
 **Installed size:** `11.77 MB`  
 **How to install:** `sudo apt install libexiv2-data`  
 
 
 - - -
 
 ### libexiv2-dev
 
  Exiv2 is a C++ library and a command line utility to manage image metadata.
  It provides fast and easy read and write access to the Exif, IPTC and XMP
  metadata of images in various formats
   
  This package provides the development files for using exiv2.
 
 **Installed size:** `474 KB`  
 **How to install:** `sudo apt install libexiv2-dev`  
 
 {{< spoiler "Dependencies:" >}}
 * libexiv2-28 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libexiv2-doc
 
  Exiv2 is a C++ library and a command line utility to manage image metadata.
  It provides fast and easy read and write access to the Exif, IPTC and XMP
  metadata of images in various formats
   
  This package provides the Exiv2 HTML documentation.
 
 **Installed size:** `21.22 MB`  
 **How to install:** `sudo apt install libexiv2-doc`  
 
 {{< spoiler "Dependencies:" >}}
 * libjs-jquery
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
